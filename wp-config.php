<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'ns_db');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'nelam');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'passer');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jhUH6P|w!C,w1=*UP+%LzBu_aOWIIw!+&:0@N4|3/&iU|fnF(BYb:%HrJ>)r[d~j');
define('SECURE_AUTH_KEY',  'FYMn0MBTob/}WA  RE~-PL1Gtr3FjT7+iG-teSc=H,@--kP)g`72bnJ_o1[9p6lV');
define('LOGGED_IN_KEY',    's+) 2{+^M?U2+NTW>Hs+bs4Js|_-RQP<6>k^4x2owEpi3yc()LuV|M.=Av{^KoKM');
define('NONCE_KEY',        '5},?`U&h<DY@@^+=zidauI!>-Zv.ugB%g[S^T!Y>4)I%E^mi/W|{i5F5*goWxoe_');
define('AUTH_SALT',        'rg7|eBtB2E+ `|Facn;Un|Qk:l:(t{4bF^G&$nv7LVa|E7i_5aq.C(x,+&9Q/^Sc');
define('SECURE_AUTH_SALT', 'S78-0C#jyG=N$:_}u`ylt<v_48hhx@ h`K{E+D) :97zFP+&ElTK]zQOxW>|,Ty>');
define('LOGGED_IN_SALT',   '|)XM Xc+AWTn1Mv+ZSN!,qt[37.QxSzgc}/v;SP[>!qr~NvM`K3#U7JsuFycTDD`');
define('NONCE_SALT',       'Ghq%A@@<@Nr=$t ,-=.t<$XpIH2o=&S,NW2yv*l7#PR#=^E<U/{E&[N2@|g+q,48');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
define('FS_METHOD', 'direct');
