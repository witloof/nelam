<?php

/**
 * Textdomain
 *
 * @since 1.0
 */
function ef_cpt_load_textdomain() {
	$locale = get_locale();
	$dir    = trailingslashit( EF_CPT_DIR . 'lang' );
	$mofile = "{$dir}{$locale}.mo";

	// In themes/plugins/mu-plugins directory
	load_textdomain( 'ef-cpt', $mofile );
}
add_action('plugins_loaded', 'ef_cpt_load_textdomain');


/**
 * Show featured image column
 *
 * @since Brama 1.0
 */

function ef_get_featured_image_08596( $post_ID ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post_ID );
    if ( $post_thumbnail_id ) {
        $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );
        return $post_thumbnail_img[0];
    } else {
        return false;
    }
}

add_filter('manage_posts_columns', 'ef_columns_head_08596');
function ef_columns_head_08596( $defaults ) {
    $pt = get_post_type();
    if ( $pt === EF_CPT || $pt === EF_CPT_TEAM ) {
        $defaults['featured_image'] = 'Featured Image';
    }

    return $defaults;
}

add_action( 'manage_posts_custom_column', 'ef_columns_content_08596', 10, 2 );
function ef_columns_content_08596( $column_name, $post_ID ) {
    $pt = get_post_type( $post_ID );
    if ( ( $pt === EF_CPT || $pt === EF_CPT_TEAM ) && $column_name == 'featured_image' ) {
        $post_featured_image = ef_get_featured_image_08596( $post_ID );
        if ( $post_featured_image ) {
            echo '<a title="' . __( 'Edit post', 'ef-cpt' ) . '" href="' . get_edit_post_link( $post_ID ) . '"><img src="' . $post_featured_image . '" /></a>';
        }
    }
}


/**
 * Customizing widget areas UI
 *
 * @since Brama 1.0
 */

function ef_is_sidebars_post() {
	global $pagenow;
	global $post;
	return ( 'post.php' == $pagenow || 'post-new.php' == $pagenow ) && ( isset( $post ) && $post->post_type == EF_CPT_SIDEBARS ) && is_admin();
}

add_filter( 'post_row_actions', 'ef_remove_row_actions_08596', 10, 2 );
add_filter( 'edit_custom-sidebars_per_page', 'ef_remove_row_actions_08596' );
function ef_remove_row_actions_08596( $actions ) {
    if ( is_admin() && get_post_type() === EF_CPT_SIDEBARS ) {
        unset( $actions['edit'] );
        unset( $actions['view'] );
        unset( $actions['inline hide-if-no-js'] );
    }
    return $actions;
}

add_filter( 'get_sample_permalink_html', 'ef_remove_preview_permalink_08596' );
function ef_remove_preview_permalink_08596( $return ) {
    if ( ef_is_sidebars_post() ){
    	$return = '';
    }

    return $return;
}

add_action('admin_head-post.php', 'ef_hide_publishing_actions_08596');
add_action('admin_head-post-new.php', 'ef_hide_publishing_actions_08596');
function ef_hide_publishing_actions_08596() {
    if ( ef_is_sidebars_post() ){
        $style = '';
        $style .= '<style type="text/css">';
        $style .= '.postbox-container #submitdiv > .handlediv, .postbox-container #submitdiv > .hndle, #edit-slug-box, #misc-publishing-actions, #minor-publishing-actions, #visibility, .num-revisions, .curtime';
        $style .= '{display: none; }';
        $style .= '</style>';

        echo $style;
    } else {
    	return;
    }
}

add_filter( 'gettext', 'ef_change_publish_button_08596', 10, 2 );
function ef_change_publish_button_08596( $translation, $text ) {
	if ( ef_is_sidebars_post() && $text == 'Publish' ) {
		return __( 'Save widget area', 'ef-cpt' );
	}

	return $translation;
}

add_filter( 'post_updated_messages', 'ef_remove_all_messages_08596' );
function ef_remove_all_messages_08596( $messages ) {
    if ( ef_is_sidebars_post() ){
        return array();
    }
}

add_filter( 'bulk_post_updated_messages', 'ef_bulk_post_updated_messages_08596', 10, 2 );
function ef_bulk_post_updated_messages_08596( $bulk_messages, $bulk_counts ) {
    $bulk_messages[EF_CPT_SIDEBARS] = array(
        'updated'   => _n( '%s widget area updated.', '%s widget areas updated.', $bulk_counts['updated'], 'ef-cpt' ),
        'locked'    => _n( '%s widget area not updated, somebody is editing it.', '%s widget areas not updated, somebody is editing them.', $bulk_counts['locked'], 'ef-cpt' ),
        'deleted'   => _n( '%s widget area permanently deleted.', '%s widget areas permanently deleted.', $bulk_counts['deleted'], 'ef-cpt' ),
        'trashed'   => _n( '%s widget area moved to the Trash.', '%s widget areas moved to the Trash.', $bulk_counts['trashed'], 'ef-cpt' ),
        'untrashed' => _n( '%s widget area restored from the Trash.', '%s widget areas restored from the Trash.', $bulk_counts['untrashed'], 'ef-cpt' ),
    );
    $bulk_messages[EF_CPT_TEAM] = array(
        'updated'   => _n( '%s Member updated.', '%s Members updated.', $bulk_counts['updated'], 'ef-cpt' ),
        'locked'    => _n( '%s Member not updated, somebody is editing it.', '%s Members not updated, somebody is editing them.', $bulk_counts['locked'], 'ef-cpt' ),
        'deleted'   => _n( '%s Member permanently deleted.', '%s Members permanently deleted.', $bulk_counts['deleted'], 'ef-cpt' ),
        'trashed'   => _n( '%s Member moved to the Trash.', '%s Members moved to the Trash.', $bulk_counts['trashed'], 'ef-cpt' ),
        'untrashed' => _n( '%s Member restored from the Trash.', '%s Members restored from the Trash.', $bulk_counts['untrashed'], 'ef-cpt' ),
    );

    return $bulk_messages;
}

add_filter('bulk_actions-edit-custom-sidebars','ef_custom_bulk_actions_08596');
function ef_custom_bulk_actions_08596( $actions ){
    unset( $actions['edit'] );
    return $actions;
}