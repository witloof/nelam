<?php
/*
Plugin Name: Fireform Meta Box - Brama
Plugin URI: http://themeforest.net/users/fireform
Description: This is a required plugin for "Brama" theme. This plugin helps to adjust the theme by providing metaboxes with usefull options for customization.
Author: Evgeny Fireform
Author URI: http://themeforest.net/users/fireform
Version: 1.0
License: Regular License
License URI: http://themeforest.net/licenses
*/

/*  Copyright 2014 Evgeny Fireform. Feel free to contact me via my profile page contact form.

    The Regular License allows use of the item in one single end product which end users are not charged to access or use. You can do this directly or, if you're a freelancer, you can create the end product for one client to distribute free to its end users. You can charge your client to produce the single end product. Distribution of source files is not permitted.

	Important information !!!

	"Fireform Meta Box" plugin is based on "Meta box" by Rilwis (Tran Ngoc Tuan Anh). Direct link to "Meta box" documentation: http://www.deluxeblogtips.com/meta-box/
	Do not use "Fireform Meta Box" plugin in your themes (to Themeforest authors). Create your own mods using source code from Tran Ngoc Tuan Anh. Get more information about copyrights and using at http://www.deluxeblogtips.com.
*/

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Add theme support
add_theme_support( 'ef-meta-boxes' );

// Script version, used to add version for scripts and styles
define( 'efmb_VER', '1.0' );

// Define plugin URLs, for fast enqueuing scripts and styles
if ( ! defined( 'efmb_URL' ) ) {
	define( 'efmb_URL', plugin_dir_url( __FILE__ ) );
	define( 'efmb_JS_URL', trailingslashit( efmb_URL . 'js' ) );
	define( 'efmb_CSS_URL', trailingslashit( efmb_URL . 'css' ) );
	define( 'efmb_IMG_URL', trailingslashit( efmb_URL . 'img' ) );
	define( 'efmb_INC_URL', trailingslashit( efmb_URL . 'inc' ) );
}

// Plugin paths, for including files
if ( ! defined( 'efmb_DIR' ) ) {
	define( 'efmb_DIR', plugin_dir_path( __FILE__ ) );
	define( 'efmb_INC_DIR', trailingslashit( efmb_DIR . 'inc' ) );
	define( 'efmb_FIELDS_DIR', trailingslashit( efmb_INC_DIR . 'fields' ) );
	define( 'efmb_CLASSES_DIR', trailingslashit( efmb_INC_DIR . 'classes' ) );
}

// Helper function to retrieve meta value
require_once efmb_INC_DIR . 'helpers.php';

if ( is_admin() ) {
	require_once efmb_INC_DIR . 'common.php';

	// Field classes
	foreach ( glob( efmb_FIELDS_DIR . '*.php' ) as $file ) {
		require_once $file;
	}

	// Main file
	require_once efmb_CLASSES_DIR . 'meta-box.php';
}

require_once efmb_INC_DIR . 'functions.php';