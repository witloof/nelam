<?php

add_action( 'admin_enqueue_scripts', 'ef_mb_enqueue_scripts' );

function ef_mb_enqueue_scripts() {
	wp_enqueue_script( 'ef-mb-common', efmb_JS_URL . '/common.js', array( 'jquery' ), array(), true );
}

add_theme_support( 'post-formats', array( 'video' ) );

if ( current_theme_supports( 'ef-custom-post-types' ) ) {
	add_post_type_support( EF_CPT, 'post-formats' );
}


/* Custom helpers */

/* Check if is page template in wp admin */
function ef_mb_if_is_page_template_wpadmin( $page_tpl = '', $if_woo = false ) {
	if ( isset ( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	} elseif ( isset ( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}

	if ( $if_woo && isset( $post_id ) ) {
		return $post_id == get_option( 'woocommerce_shop_page_id' );
	}

	if ( !isset ( $post_id ) || empty ( $post_id ) ) return;
	$template_file = get_post_meta( $post_id, '_wp_page_template', true );
	return $page_tpl == $template_file;
}


/* Check if is post type in wp admin */
function ef_mb_if_current_post_type_wpadmin( $post_type = '' ) {
	global $post, $typenow, $current_screen;
	$pt = false;

	if ( $post && $post->post_type ) {
		$pt = $post->post_type;
	} elseif ( $typenow ) {
		$pt = $typenow;
	} elseif ( $current_screen && $current_screen->post_type ) {
		$pt = $current_screen->post_type;
	} elseif ( isset( $_REQUEST['post_type'] ) ) {
		$pt = sanitize_key( $_REQUEST['post_type'] );
	}

	return $post_type == $pt;
}

/* Get global page layout */
function ef_mb_get_global_layouts( $ef_data = null ) {
	$layouts = array();
	if ( !empty( $ef_data ) ) {
		if ( ef_mb_if_is_page_template_wpadmin( 'templates/portfolio-template.php' ) ) {
			$layouts['menu'] 	= $ef_data['ef_menu_layout_portf'];
			$layouts['sidebar'] = $ef_data['ef_layout_portf'];
			$layouts['footer'] 	= $ef_data['ef_layout_footer_portf'];
		} elseif ( ef_mb_if_is_page_template_wpadmin( 'templates/blog-template.php' ) ) {
			$layouts['menu'] 	= $ef_data['ef_menu_layout_blog'];
			$layouts['sidebar'] = $ef_data['ef_layout_blog'];
			$layouts['footer'] 	= $ef_data['ef_layout_footer_blog'];
		} elseif ( ef_mb_if_current_post_type_wpadmin( 'portfolios' ) ) {
			$layouts['menu'] 	= $ef_data['ef_menu_layout_portf_single'];
			$layouts['sidebar'] = $ef_data['ef_layout_portf_single'];
			$layouts['footer'] 	= $ef_data['ef_layout_footer_portf_single'];
		} elseif ( ef_mb_if_is_page_template_wpadmin( '', true ) ) {
			$layouts['menu'] 	= $ef_data['ef_menu_layout_archive_product'];
			$layouts['sidebar'] = $ef_data['ef_layout_archive_product'];
			$layouts['footer'] 	= $ef_data['ef_layout_footer_archive_product'];
		} elseif ( ef_mb_if_current_post_type_wpadmin( 'extras' ) || ef_mb_if_current_post_type_wpadmin( 'post' ) ) {
			$layouts['menu'] 	= $ef_data['ef_menu_layout_blog_single'];
			$layouts['sidebar'] = $ef_data['ef_layout_blog_single'];
			$layouts['footer'] 	= $ef_data['ef_layout_footer_blog_single'];
		} else {
			$layouts['menu'] 	= $ef_data['ef_menu_layout'];
			$layouts['sidebar'] = $ef_data['ef_layout'];
			$layouts['footer'] 	= $ef_data['ef_layout_footer'];
		}
	}
	return $layouts;
}