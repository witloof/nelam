<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Make sure "text" field is loaded
require_once efmb_FIELDS_DIR . 'url.php';

if ( ! class_exists( 'efmb_OEmbed_Field' ) )
{
	class efmb_OEmbed_Field extends efmb_URL_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_script( 'efmb-oembed', efmb_JS_URL . 'oembed.js', array(  ), efmb_VER, true );
			//wp_enqueue_style( 'efmb-oembed', efmb_CSS_URL . 'oembed.css', array(  ), efmb_VER );
			wp_localize_script( 'efmb-oembed', 'efmb_OEmbed', array( 'url' => efmb_URL ) );
		}

		/**
		 * Add actions
		 *
		 * @return void
		 */
		static function add_actions()
		{
			// Attach images via Ajax
			add_action( 'wp_ajax_efmb_get_embed', array( __CLASS__, 'wp_ajax_get_embed' ) );
		}

		/**
		 * Ajax callback for returning oEmbed HTML
		 *
		 * @return void
		 */
		static function wp_ajax_get_embed()
		{
			global $post;
			$url = isset( $_POST['oembed_url'] ) ? $_POST['oembed_url'] : 0;
			$post_id = is_numeric( $_REQUEST['post_id'] ) ? (int) $_REQUEST['post_id'] : 0;
			if ( isset( $_REQUEST['post_id'] ) )
				$post = get_post( $_REQUEST['post_id'] );
			$embed = self::get_embed( $url );
			EF_Meta_Box::ajax_response( $embed, 'success' );
			exit;
		}

		/***
		* Get embed html from url
		* @param 	string $url
		* $return 	string
		*/

		static function get_embed( $url )
		{
				
			$embed = wp_oembed_get( esc_url( $url ) );

			if( $embed )
			{
				return $embed;
			}
			else
			{
				return  'Embed not available.';
			}

		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			return sprintf(
				'<input type="url" class="efmb-oembed" name="%s" id="%s" value="%s" size="%s" />
				<span class="spinner" style="display: none;"></span>
				<a href="#" class="show-embed button-secondary">Show embed</a>
				<div class="embed-code"> %s </div>',
				$field['field_name'],
				$field['id'],
				$meta,
				$field['size'],
				self::get_embed( $meta )
			);
		}
	}
}
