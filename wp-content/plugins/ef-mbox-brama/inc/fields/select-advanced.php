<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Make sure "select" field is loaded
require_once efmb_FIELDS_DIR . 'select.php';

if ( !class_exists( 'efmb_Select_Advanced_Field' ) )
{
	class efmb_Select_Advanced_Field extends efmb_Select_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style( 'select2', efmb_CSS_URL . 'select2/select2.css', array(), '3.2' );
			wp_enqueue_style( 'efmb-select-advanced', efmb_CSS_URL . 'select-advanced.css', array(), efmb_VER );

			wp_register_script( 'select2', efmb_JS_URL . 'select2/select2.min.js', array(), '3.2', true );
			wp_enqueue_script( 'efmb-select-advanced', efmb_JS_URL . 'select-advanced.js', array( 'select2' ), efmb_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			$html = sprintf(
				'<select class="efmb-select-advanced" name="%s" id="%s" %s data-options="%s">',
				$field['field_name'],
				$field['id'],
				$field['multiple'] ? ' multiple="multiple"' : '',
				esc_attr( json_encode( $field['js_options'] ) )
			);

			$html .= self::options_html( $field, $meta );

			$html .= '</select>';

			return $html;
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = parent::normalize_field( $field );
			
			$field = wp_parse_args( $field, array(
				'js_options' => array(),
			) );

			$field['js_options'] = wp_parse_args( $field['js_options'], array(
				'allowClear'  => true,
				'width'       => 'resolve',
				'placeholder' => empty( $field['std'] ) ? __( 'Select a value', 'efmb' ) : $field['std']
			) );
			
			$field['std'] = '';

			return $field;
		}
	}
}
