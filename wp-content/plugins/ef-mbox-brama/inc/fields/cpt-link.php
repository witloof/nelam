<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'efmb_cpt_link_Field' ) )
{
	class efmb_cpt_link_Field {
		/**
		 * Get field HTML
		 *
		 * @param $html
		 * @param $field
		 * @param $meta
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field ) {	
			
			$field['options'] = self::get_options();
			
			$field['std'] = count( self::get_options() ) == 0 ? __( 'No Portfolio pages available', 'efmb' ) : __( '- Hide link -', 'efmb' );
			
			$html = efmb_Select_Field::html( $html, $meta, $field );
			
			return $html;
		}

		/**
		 * Get options for select
		 *
		 * @param $options array
		 */
		static function get_options()
		{		
			$pages = array(); $list = array();
			
			$pages = get_pages( array(
				'meta_key' => '_wp_page_template',
			    'meta_value' => 'templates/portfolio-template.php'
			));
							  
		  	foreach ( $pages as $page ) {
				$list[get_permalink( $page->ID )] = $page->post_title;
		  	}
			
			return $list;
		}
	}
}
