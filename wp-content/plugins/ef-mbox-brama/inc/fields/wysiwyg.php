<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( !class_exists( 'efmb_Wysiwyg_Field' ) )
{
	class efmb_Wysiwyg_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style( 'efmb-meta-box-wysiwyg', efmb_CSS_URL . 'wysiwyg.css', array(), efmb_VER );
		}

		/**
		 * Add field actions
		 *
		 * @return void
		 */
		static function add_actions()
		{
			// Add TinyMCE script for WP version < 3.3
			global $wp_version;

			if ( version_compare( $wp_version, '3.9' ) < 1 )
			{
				add_action( 'admin_print_footer-post.php', 'wp_tiny_mce', 25 );
				add_action( 'admin_print_footer-post-new.php', 'wp_tiny_mce', 25 );
			}
		}

		/**
		 * Change field value on save
		 *
		 * @param mixed $new
		 * @param mixed $old
		 * @param int   $post_id
		 * @param array $field
		 *
		 * @return string
		 */
		static function value( $new, $old, $post_id, $field )
		{
			return ( $field['raw'] ? $new : wpautop( $new ) );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			global $wp_version;
			$name = "name='{$field['field_name']}'";
			$rows = isset( $field['rows'] ) ? $field['rows'] : get_option( 'default_post_edit_rows', 10 );
			$media = isset( $field['media'] ) ? $field['media'] : true;
			$tinymce = isset( $field['tinymce'] ) ? $field['tinymce'] : true;
			$quicktags = isset( $field['quicktags'] ) ? $field['quicktags'] : true;
			$dragupload = isset( $field['dragupload'] ) ? $field['dragupload'] : true;

			if ( version_compare( $wp_version, '3.9' ) < 1 )
			{
				return "<textarea class='efmb-wysiwyg theEditor large-text' {$name} id='{$field['id']}' cols='60' rows='{$rows}'>$meta</textarea>";
			}
			else
			{
				// Use new wp_editor() since WP 3.9
				// Using output buffering because wp_editor() echos directly
				ob_start();
				wp_editor( $meta, $field['id'], array(
					'editor_class' => 'efmb-wysiwyg',
					'textarea_rows' => $rows,
					'quicktags'     => $quicktags,
					'drag_drop_upload' => $dragupload,
					'media_buttons' => $media,
					'tinymce' => $tinymce,
				) );
				return ob_get_clean();
			}
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = wp_parse_args( $field, array(
				'raw'     => false,
				'options' => array(),
			) );

			$field['options'] = wp_parse_args( $field['options'], array(
				'editor_class' => 'efmb-wysiwyg',
				'dfw'          => true, // Use default WordPress full screen UI
			) );

			// Keep the filter to be compatible with previous versions
			$field['options'] = apply_filters( 'efmb_wysiwyg_settings', $field['options'] );

			return $field;
		}
	}
}