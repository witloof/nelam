(function($) {
	"use strict";

	var isFormat = (function(formatInput) {
		$("#post_format_video_mb, #post_mb").hide();
		if (formatInput.val() == 'video'){
			$("#post_format_video_mb").show();
		} else {
			$("#post_mb").show();
		}
	});

	var formatInput = $("#post-formats-select").find('input.post-format').filter(':checked');

	isFormat(formatInput);

	$("#post-formats-select").find('.post-format').change(function() {
		isFormat($(this));
	});

	var isExtrTeam = (function(ExtrInput) {
		$("#extra_mb, #team_mb").hide();
		if (ExtrInput.val() == '0'){
			$("#team_mb").show();
		} else {
			$("#extra_mb").show();
		}
	});

	var ExtrInput = $("#page_mb").find('input.efmb-radio').filter(':checked');

	isExtrTeam(ExtrInput);

	$("#page_mb").find('input.efmb-radio').change(function() {
		isExtrTeam($(this));
	});
})(jQuery);