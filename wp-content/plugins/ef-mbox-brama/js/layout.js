/**
 * Masked Inputs (images as radio buttons)
 */
jQuery(document).ready(function($) {
	"use strict";

	$('.efmb-layout-wrapper .efmb-radio, .efmb-extras-wrapper .efmb-radio').hide();

	$('.efmb-layout-wrapper').each(function() {
		var checkedInput = $(this).find('.efmb-input').find('input:checked');
		checkedInput.parent('.efmb-layout').addClass('efmb-layout-img-selected');
	});

	$('.efmb-extras-wrapper .efmb-input').find('input:checked').parent('.efmb-layout').addClass('efmb-layout-img-selected');

	$('.efmb-layout-wrapper .efmb-layout, .efmb-extras-wrapper .efmb-layout').click(function() {
		$(this).parent().find('.efmb-layout').removeClass('efmb-layout-img-selected');
		$(this).parent().find('input').attr('checked', false);
		$(this).addClass('efmb-layout-img-selected');
		$(this).find('input').attr('checked', true);
	});
});