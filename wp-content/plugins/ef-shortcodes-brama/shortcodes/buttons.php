<?php

add_filter( 'mce_external_languages', 'ef_shtd_shortcodes_langs');
function ef_shtd_shortcodes_langs( $locales ) {
    $locales['ef_shortcodes_button'] = SHTD_ADM_DIR . 'translations.php';
    return $locales;
}


add_action( 'admin_head', 'ef_shtd_shortcode_button' );
function ef_shtd_shortcode_button() {
	if ( ( current_user_can('edit_posts') || current_user_can('edit_pages') ) && get_user_option('rich_editing') ) {
		add_filter( 'mce_external_plugins', 'ef_shtd_shortcode_add_button' );
	    add_filter( 'mce_buttons', 'ef_shtd_shortcode_register_button' );
	}
}


function ef_shtd_shortcode_add_button($plugin_args) {
	$plugin_args['ef_shortcodes_button'] = SHTD_ADM_URL . 'scripts/buttons.js';
	return $plugin_args;
}


function ef_shtd_shortcode_register_button($buttons) {
	array_push( $buttons, 'ef_shortcodes_button' );
	return $buttons;
}


add_action( 'admin_enqueue_scripts', 'ef_shtd_shortcodes_interface_css' );
function ef_shtd_shortcodes_interface_css() {
	wp_enqueue_style( 'ef_shortcodes_interface', SHTD_ADM_URL . 'css/interface.css' );
}