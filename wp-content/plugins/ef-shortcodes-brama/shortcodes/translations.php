<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( '_WP_Editors' ) ) require( ABSPATH . WPINC . '/class-wp-editor.php' );

function ef_shtd_shortcodes_translation() {

	// Blog pages
	$pages = $blog_pages = array();
	$pages = get_pages( array(
		'meta_key'		=> '_wp_page_template',
		'meta_value'	=> 'templates/blog-template.php'
	) );

	$blog_pages[] = array(
		'text' => __( 'Hide link', EF_SHTD ),
		'value' => ''
	);
	foreach ( $pages as $page ) {
		$blog_pages[] = array(
			'text'		=> $page->post_title,
			'value'		=> get_permalink( $page->ID )
		);
	}

	// Blog categories
	$categories = $blog_cats = array();
	$categories = get_categories();
	$blog_cats[] = array(
		'text' => __( 'Show all categories', EF_SHTD ),
		'value' => ''
	);
	foreach ( $categories as $category ) {
		$blog_cats[] = array(
			'text'		=> $category->name,
			'value'		=> $category->cat_ID
		);
	}

    $strings = array(
        'shtd_title'			=> __( 'Shortcodes', EF_SHTD ),
        'shtd_section1'			=> __( 'Formatting', EF_SHTD ),
        'shtd_section2'			=> __( 'Elements', EF_SHTD ),
        'shtd_section3'			=> __( 'Javascript', EF_SHTD ),
        'shtd_headings'			=> __( 'Headings', EF_SHTD ),

        'shtd_dividers'			=> __( 'Dividers', EF_SHTD ),
        'divider_type1'			=> __( 'Default', EF_SHTD ),
        'divider_type2'         => __( 'Blank', EF_SHTD ),
        'divider_style1'        => __( 'Basic', EF_SHTD ),
        'divider_style2'        => __( 'Double', EF_SHTD ),
        'divider_style3'        => __( 'Dotted', EF_SHTD ),
        'divider_style4'        => __( 'Decorative #1', EF_SHTD ),
        'divider_style5'        => __( 'Decorative #2', EF_SHTD ),
        'divider_style6'        => __( 'Decorative #3', EF_SHTD ),
        'divider_style7'        => __( 'Decorative #4', EF_SHTD ),
        'divider_style8'		=> __( 'Decorative #5', EF_SHTD ),
        'divider_bottom'		=> __( 'Indent', EF_SHTD ),

        'shtd_columns'			=> __( 'Columns', EF_SHTD ),

        'shtd_buttons'			=> __( 'Buttons', EF_SHTD ),
        'btn_type_val1'			=> __( 'Primary', EF_SHTD ),
        'btn_size1'				=> __( 'Tiny', EF_SHTD ),
        'btn_size2'				=> __( 'Small', EF_SHTD ),
        'btn_size3'				=> __( 'Large', EF_SHTD ),
        'btn_target'			=> __( 'Target', EF_SHTD ),

        'shtd_panels'			=> __( 'Panels', EF_SHTD ),

        'shtd_alerts'			=> __( 'Alerts', EF_SHTD ),
        'shtd_alerts_close'		=> __( 'Close button', EF_SHTD ),

        'shtd_progress'			=> __( 'Progressbars', EF_SHTD ),
        'shtd_progress_skill'	=> __( 'Skill name', EF_SHTD ),
        'shtd_progress_percent' => __( 'Percent', EF_SHTD ),

        'shtd_recent' 			=> __( 'Recent posts', EF_SHTD ),
        'recent_img' 			=> __( 'Featured image', EF_SHTD ),
        'recent_num' 			=> __( 'Latest', EF_SHTD ),
        'recent_per' 			=> __( 'Per slide', EF_SHTD ),
        'recent_lnk' 			=> __( 'Blog link', EF_SHTD ),
        'blog_pages_array'		=> $blog_pages,
        'recent_link_title'		=> __( 'Link title', EF_SHTD ),
        'recent_exclude_ids'	=> __( 'Exclude category', EF_SHTD ),
        'recent_exclude_ids_array'=> $blog_cats,
        'recent_auto'			=> __( 'Autoplay', EF_SHTD ),
        'recent_inf'			=> __( 'Infinite', EF_SHTD ),
        'recent_dur'			=> __( 'Duration', EF_SHTD ),
        'recent_trans'			=> __( 'Transition', EF_SHTD ),

        'shtd_ui'				=> __( 'jQuery UI', EF_SHTD ),

        'shtd_slider'			=> __( 'Slideshow', EF_SHTD ),
        'slideshow_type1'		=> __( 'Carousel', EF_SHTD ),
        'slideshow_slides'		=> __( 'Slides', EF_SHTD ),
        'slideshow_img'			=> SHTD_ADM_URL,

        'field_title'			=> __( 'Title', EF_SHTD ),
        'field_content'			=> __( 'Content', EF_SHTD ),
        'field_text'			=> __( 'Text', EF_SHTD ),
        'field_size'            => __( 'Size', EF_SHTD ),
        'field_style'			=> __( 'Style', EF_SHTD ),
        'field_type'			=> __( 'Type', EF_SHTD ),
        'field_url'				=> __( 'Url', EF_SHTD ),

        'field_danger'			=> __( 'Danger', EF_SHTD ),
        'field_warning'			=> __( 'Warning', EF_SHTD ),
        'field_info'			=> __( 'Info', EF_SHTD ),
        'field_success'			=> __( 'Success', EF_SHTD ),
    );

    $locale = _WP_Editors::$mce_locale;
    $translated = 'tinyMCE.addI18n("' . $locale . '.ef_shtd_lang", ' . json_encode( $strings ) . ");\n";

    return $translated;
}

$strings = ef_shtd_shortcodes_translation();