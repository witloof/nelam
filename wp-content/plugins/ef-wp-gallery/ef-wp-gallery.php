<?php
/*
Plugin Name: Fireform WP Gallery
Plugin URI: http://themeforest.net/users/fireform
Description: Masonry mode for the native WordPress Gallery.
Author: Evgeny Fireform
Author URI: http://themeforest.net/users/fireform
Version: 1.0
License: Regular License
License URI: http://themeforest.net/licenses
*/

/*  Copyright 2014 Evgeny Fireform. Feel free to contact me via my profile page contact form.

    The Regular License allows use of the item in one single end product which end users are not charged to access or use. You can do this directly or, if you're a freelancer, you can create the end product for one client to distribute free to its end users. You can charge your client to produce the single end product. Distribution of source files is not permitted.
*/

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

/**
 * Definitions
 *
 * @since 1.0
 */
define( 'efwpg_VERSION', '1.0' );

/**
 * Add theme support
 *
 * @since 1.0
 */
add_theme_support( 'ef-wp-gallery' );

if( !defined( 'efwpg_DIR' ) ) define( 'efwpg_DIR', plugin_dir_path( __FILE__ ) );
if( ! defined( 'efwpg_URL' ) ) define( 'efwpg_URL', plugin_dir_url( __FILE__ ) );
if( ! defined( 'efwpg_ASSETS' ) ) define( 'efwpg_ASSETS', trailingslashit( efwpg_URL . 'assets' ) );

/**
 * Required Files
 *
 * @since 1.0
 */
require_once efwpg_DIR . 'functions.php';