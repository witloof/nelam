<?php

/**
 * Required scripts/css
 *
 * @since 1.0.0
 */
function ef_efwpg_enqueue_style() {
    wp_enqueue_style( 'efwpg', efwpg_ASSETS . 'css/style.min.css', array(), efwpg_VERSION, 'screen' );
}

function ef_efwpg_enqueue_script() {
    if ( !wp_script_is( 'jquery-masonry', 'done' ) ) wp_enqueue_script( 'jquery-masonry' );
    wp_enqueue_script( 'efwpg-init', efwpg_ASSETS . 'js/init.js', array( 'jquery' ), efwpg_VERSION, true );
}

add_action( 'wp_enqueue_scripts', 'ef_efwpg_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'ef_efwpg_enqueue_script' );


function ef_modified_gallery_atts_68163( $atts ) {
	$atts['itemtag'] = 'div';
	$atts['icontag'] = 'div';
	$atts['captiontag'] = 'h5';
	if ( defined( 'EF_TDM' ) ) {
		$atts['size'] = is_single() || is_page() ? 'medium_thumb' : 'thumbnail';
	} else {
		$atts['size'] = 'large';
	}

	return $atts;
}
function ef_modified_gallery_68163( $atts ) {
    return '<div class="ef-gallery-outer">' . strip_tags( gallery_shortcode( $atts ), '<a><span><div><img><h5>' ) . '</div>';
}
add_filter( 'use_default_gallery_style', '__return_false' );
add_filter( 'shortcode_atts_gallery', 'ef_modified_gallery_atts_68163', 10, 3 );
remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'ef_modified_gallery_68163' );