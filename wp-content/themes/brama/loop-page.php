<?php
/**
 * The loop that displays a page.
 *
 * @package Brama
 * @since Brama 1.0
 */
?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php if ( have_posts() ) { ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php if ( $post->post_content != "" ) { ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class( 'entry-content' ); ?>>
				<?php the_content(); ?>
			</div><!-- #post-## -->

			<?php // If comments are open or we have at least one comment, load up the comment template. ?>
			<?php if ( !post_password_required() && ( comments_open() || get_comments_number() ) ) { ?>
				<?php comments_template( '', true ); ?>
			<?php } ?>
		<?php } ?>

		<?php /* Page links */ ?>
		<?php wp_link_pages( array(
			'before'		=> '<div class="ef-nextpage"><span class="ef-page-links-title">' . __( 'Pages:', EF_TDM ) . '</span>',
			'after'	=> '</div>',
			'before_link'	=> '<span>',
			'after_link'	=> '</span>'
		) ); ?>

	<?php endwhile; // end of the loop. ?>
<?php } ?>