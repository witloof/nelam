<?php
/**
 * The main template file.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

	<?php get_template_part( 'loop', 'blog' ); ?>

	<?php /* Pagination */ ?>
	<?php ef_theme_pagination( $wp_query->max_num_pages, $ef_data ); ?>

<?php get_footer(); ?>