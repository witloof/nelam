<?php
/**
 * The template for displaying Archive pages.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

		<?php $ef_data = EfGetOptions::TplConditions(); ?>
		<?php $archive = $ef_data['cpt_support'] && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ? 'portfolio' : 'archive'; ?>

		<?php get_template_part( 'loop', $archive ); ?>

		<?php /* Pagination */ ?>
		<?php ef_theme_pagination( $wp_query->max_num_pages, $ef_data ); ?>

<?php get_footer(); ?>