<?php

/* Initial WooCommerse setup */
add_theme_support( 'woocommerce' );

if ( !defined( 'EF_WOO_URL' ) ) {
	define( 'EF_WOO_URL', get_template_directory_uri() . trailingslashit( '/woocommerce' ) );
}

// CSS
add_action( 'wp_enqueue_scripts', 'ef_theme_woo_enqueue_css' );
function ef_theme_woo_enqueue_css() {
	wp_enqueue_style( 'ef-woocommerce-css', EF_WOO_URL . 'css/wocommerce.min.css', array(), '', 'all' );
}

/**
	WooCommerce filter hooks

	@since	Brama 1.0
* */


// Default featured image
add_action( 'init', 'ef_woo_fix_default_thumbnail' );
function ef_woo_fix_default_thumbnail() {
	add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
	function custom_woocommerce_placeholder_img_src( $src ) {
		return EF_WOO_URL . 'img/placeholder.png';
	}
}


// Disabling lightbox, coming with WooCommerce
add_action( 'wp_enqueue_scripts', 'ef_woo_deregister_lbox', 100 );
function ef_woo_deregister_lbox() {
	wp_dequeue_style( 'woocommerce_frontend_styles' );
	wp_dequeue_style( 'woocommerce_fancybox_styles' );
	wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
	wp_dequeue_script( 'prettyPhoto' );
	wp_dequeue_script( 'prettyPhoto-init' );
	wp_dequeue_script( 'fancybox' );
}

// Custom pagination for WooCommerce instead the default woocommerce_pagination()
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
add_action( 'woocommerce_after_shop_loop', 'ef_woo_pagination', 10);
function ef_woo_pagination() {
	ef_theme_pagination();
}

// Move sale flash
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 8 );

// Hide page title
add_filter( 'woocommerce_show_page_title', '__return_false' );

// Wrap shop loop
add_action('woocommerce_before_shop_loop', 'ef_woo_before_loop');
function ef_woo_before_loop() {
	echo '<div class="ef-woo-shop-wrapper ef-woo-shop-medium">';
}

add_action('woocommerce_after_shop_loop', 'ef_woo_after_loop');
function ef_woo_after_loop() {
	echo '</div>';
}

// Moving stars :)
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_rating', 5 );

// Wrap shop item loop
add_action('woocommerce_before_shop_loop_item', 'ef_woo_before_item_loop');
function ef_woo_before_item_loop() {
	echo '<div class="ef-woo-item-wrapper">';
}
add_action('woocommerce_after_shop_loop_item', 'ef_woo_after_item_loop');
function ef_woo_after_item_loop() {
	echo '</div>';
}

// Wrap product title in the loop
add_filter('woocommerce_before_shop_loop_item_title', 'ef_woo_before_shop_title');
function ef_woo_before_shop_title() {
	echo '<span class="ef-woo-loop-title clearfix">';
}
add_filter('woocommerce_after_shop_loop_item_title', 'ef_woo_after_shop_title');
function ef_woo_after_shop_title() {
	echo '</span>';
}

// Move single product title
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 9);

// Move ratings
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_rating', 10 );

//Wrap sale flash, product title and rating on single product page
add_filter( 'woocommerce_before_single_product_summary', 'ef_before_single_sale_flash', 4 );
function ef_before_single_sale_flash() {
	echo '<div class="row"><div class="col-md-8 col-md-offset-2 text-center">';
}

// Wrap Summary
add_filter('woocommerce_before_single_product_summary', 'ef_woo_before_summary' );
function ef_woo_before_summary() {
	echo '</div><!-- .col-md-8.col-md-offset-2.text-center --></div><!-- .row --><div class="row"><div class="col-md-8 col-md-offset-2">';
}
add_filter('woocommerce_after_single_product_summary', 'ef_woo_after_summary' );
function ef_woo_after_summary() {
	echo '</div></div>';
}

// Move price on single product page
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 27 );

// Move except
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 25 );

// Remove product description heading (in tabs)
add_filter( 'woocommerce_product_description_heading', '__return_false', 10 );

// Review form
add_filter( 'woocommerce_product_review_comment_form_args', 'ef_woo_comment_form_modifier' );
function ef_woo_comment_form_modifier( $comment_form ) {
	$commenter = wp_get_current_commenter();

	$comment_form['fields']['author'] = '<div class="form-group"><input placeholder="' . __( 'Name *', EF_TDM ) . '" class="form-control" name="author" type="text" size="30" tabindex="1" aria-required="true" value="' . esc_attr( $commenter['comment_author'] ) . '" /></div>';

	$comment_form['fields']['email'] = '<div class="form-group ef-block-merger"><input placeholder="' . __( 'E-mail *', EF_TDM ) . '" class="form-control" name="email" type="text" size="30" tabindex="2" aria-required="true" value="' . esc_attr( $commenter['comment_author_email'] ) . '" /></div>';

	$textarea = '<div class="ef-textarea row"><div class="col-lg-12"><div class="form-group"><textarea placeholder="' . __( 'Your review *', EF_TDM ) . '" class="form-control" rows="5" name="comment" type="text" tabindex="3" aria-required="true"></textarea></div></div></div>';

	if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
		$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . __( 'Your Rating', 'woocommerce' ) .'</label><select name="rating" id="rating">
			<option value="">' . __( 'Rate&hellip;', 'woocommerce' ) . '</option>
			<option value="5">' . __( 'Perfect', 'woocommerce' ) . '</option>
			<option value="4">' . __( 'Good', 'woocommerce' ) . '</option>
			<option value="3">' . __( 'Average', 'woocommerce' ) . '</option>
			<option value="2">' . __( 'Not that bad', 'woocommerce' ) . '</option>
			<option value="1">' . __( 'Very Poor', 'woocommerce' ) . '</option>
		</select></div>';

		$comment_form['comment_field'] .= $textarea;
	} else {
		$comment_form['comment_field'] = $textarea;
	}

	return $comment_form;
}

// Wrap cart totals + shipping calculator
add_filter( 'woocommerce_cart_collaterals', 'ef_woo_before_cart_totals' );
function ef_woo_before_cart_totals() {
	echo '<div class="row"><div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-6">';
}
add_filter( 'woocommerce_after_shipping_calculator', 'ef_woo_after_cart_totals' );
function ef_woo_after_cart_totals() {
	echo '</div></div>';
}

// Thank you
add_filter( 'woocommerce_thankyou_order_received_text', 'ef_woo_thankyou' );
function ef_woo_thankyou() {
	return sprintf( '<strong>%1s</strong> %2s', __( 'Thank you!', EF_TDM ), __( 'Your order has been received.', EF_TDM ) );
}

// Default image sizes
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'ef_woo_image_dimensions', 1 );
function ef_woo_image_dimensions() {
	$catalog = array(
		'width' => '800',	// px
		'height'=> '1000',	// px
		'crop'	=> 1 // true
	);

	$single = array(
		'width' => '800',	// px
		'height'=> '1000',	// px
		'crop'	=> 1 // true
	);

	$thumbnail = array(
		'width' => '300',	// px
		'height'=> '300',	// px
		'crop'	=> 1 // true
	);

	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); // Product category thumbs
	update_option( 'shop_single_image_size', $single ); // Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); // Image gallery thumbs
}

// Related products args
add_filter( 'woocommerce_output_related_products_args', 'ef_related_products_args' );
function ef_related_products_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}


// Cart
function ef_get_woo_cart() {

	global $woocommerce;

	$cart_html = '<a class="cart-contents" href="' . esc_url( $woocommerce->cart->get_cart_url() ) . '" title="' .__( 'View your shopping cart', EF_TDM ) . '"><span>' . $woocommerce->cart->cart_contents_count . '</span></a>';

	echo '<div id="ef-woo-shopping-cart" class="cart_contents">' . $cart_html . '<div class="ef-mini-cart"><div><div class="widget_shopping_cart_content">';

	woocommerce_mini_cart();

	echo '</div></div></div></div>';
}

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {

	global $woocommerce;
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', EF_TDM ); ?>"><span><?php echo $woocommerce->cart->cart_contents_count; ?></span></a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}