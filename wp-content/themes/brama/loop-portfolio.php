<?php
/**
 * The loop that displays posts.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

	<?php if ( ! have_posts() || ! $ef_data['cpt_support'] ) { ?>
		<?php ef_get_msg_data( $wp_query->query_vars['post_type'], $ef_data ); ?>
	<?php } elseif ( ef_if_has_portfolios() ) { ?>
		<section id="ef-portfolio" class="clearfix">
			<ul>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $pass = post_password_required() ? '<span class="ef-pass ts-icon-lock"></span>' : ''; ?>

					<?php if ( $ef_data['mb_support'] ) { ?>
						<?php $thumb = efmb_meta( 'ef_img_orientation', 'type=radio' ); ?>
						<?php $thumb = $thumb === '1' ? 'portfolio_portrate' : 'portfolio_landscape'; ?>
					<?php } else { ?>
						<?php $thumb = 'portfolio_landscape'; ?>
					<?php } ?>
					<?php if ( has_post_thumbnail() ) { ?>
						<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="ef-proj-img">
								<?php echo get_the_post_thumbnail( $post->ID, $thumb ); ?>

								<h1 class="ef-proj-desc entry-title">
									<?php echo $pass; ?>

									<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
	                                	<span class="ef-details-holder">
		                                	<?php the_title(); ?>

		                                	<?php if ( $ef_data['mb_support'] ) { ?>
												<?php $creds = efmb_meta( 'ef_img_credits', 'type=text', $post->ID ); ?>
												<?php echo !empty( $creds ) ? '<span class="ef-additional-info">' . wp_kses( $creds, array( 'img' => array( 'src' => array(), 'alt' => array() ), 'br' => array(), 'strong' => array() ) ) . '</span>' : ''; ?>
											<?php } ?>
										</span>
	                                </a>
								</h1><!-- .ef-proj-desc -->
							</div><!-- .ef-proj-img  -->
						</li><!-- .ef-post  -->
					<?php } ?>
				<?php endwhile; // End the loop. ?>
			</ul>
		</section><!-- #ef-portfolio -->
	<?php } ?>