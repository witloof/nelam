<?php
/**
 * The template for displaying all pages.
 *
 * @package Fidelity
 * @since Fidelity 1.0
 */

?>

<?php get_header(); ?>

	<?php woocommerce_content(); ?>
		
<?php get_footer(); ?>