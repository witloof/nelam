<?php

/* STD fix */
add_action( 'save_post', 'ef_mb_set_default_meta_new_post' );
function ef_mb_set_default_meta_new_post( $post_ID ) {

	if ( get_post_type( $post_ID ) == 'page' ) {

		$prefix = "ef_";

		$stds = array(
			"templates/home-template.php" => array(
				"{$prefix}home_style"		=> "ef-home-default",
				"{$prefix}slider_autoplay"	=> "0",
			),
			"templates/portfolio-template.php" => array(
				"{$prefix}portf_style"		=> "ef-medium-portfolio",
				"{$prefix}portf_type"  		=> "filtered",
				"{$prefix}range"   			=> 9,
			),
			"templates/regular-template.php" => array(
				"{$prefix}team_extr"  		=> "1",
				"{$prefix}extr_content"  	=> "ef-excerpt",
				"{$prefix}team_content"  	=> "ef-content",
			),
			"templates/blog-template.php" 	=> array(
				"{$prefix}portf_type"  		=> "regular",
				"{$prefix}feed_layout"  	=> "ef-grid-blog",
				"{$prefix}show_gallery"		=> 0,
				"{$prefix}show_cats"		=> 1,
				"{$prefix}range"   			=> 10,
				"{$prefix}extr_content"  	=> "ef-excerpt",
			),
			"templates/contact-template.php" 	=> array(
				"{$prefix}map_zoom"  		=> 15,
				"{$prefix}map_lat"  		=> "53.350193",
				"{$prefix}map_long"  		=> "-6.267704",
			)
		);

		foreach ( $stds as $tpl => $param ) {

			$template = get_post_meta( $post_ID, '_wp_page_template' );
			if ( in_array( $tpl, $template ) ) {
				foreach ( $param as $std => $default_meta ) {
					$current_field_value = get_post_meta( $post_ID, $std );
					if ( isset( $current_field_value ) && !wp_is_post_revision( $post_ID ) ) {
						update_post_meta( $post_ID, $std, $default_meta );
					}
				}
			}
		}
	}
}

$template_url = get_template_directory_uri();

/* Layouts */
function ef_mb_get_widget_layouts( $efto_data = null, $area = 'sidebar', $template_url = '' ) {
	$ef_options = array(
		'1' => array(
			$template_url . '/assets/img/admin/nosb.gif',
			__( 'Regular', EF_TDM ),
			),
		'2'	=> array(
			$area !== 'sidebar' ? $template_url . '/assets/img/admin/sb-menu.gif' : $template_url . '/assets/img/admin/sb.gif',
			__( '+ widgets', EF_TDM ),
			),
	);

	if ( !empty( $efto_data ) ) {
		$layout = ef_mb_get_global_layouts( $efto_data );

		$first = array(
			$ef_options[$layout[$area]][0],
			sprintf( __( 'Global %s', EF_TDM ), '(' . $ef_options[$layout[$area]][1] . ')' ),
		);
	} else {
		$first = array(
			$ef_options['1'][0],
			__( 'Global (isn\'t set)', EF_TDM ),
		);
	}
	$layouts = array(
		'1'	=> array(
			$ef_options['1'][0],
			$ef_options['1'][1],
		),
		'2'	=> array(
			$ef_options['2'][0],
			$ef_options['2'][1],
		),
	);

	array_unshift( $layouts, $first );

	return $layouts;
}


$prefix = 'ef_';

global $meta_boxes, $efto_data;
$meta_boxes = array();

$efto = current_theme_supports( 'ef-theme-options' );
$ef_cpt = current_theme_supports( 'ef-custom-post-types' );

if ( $ef_cpt ) {
	$cpt = EF_CPT;
	$cpt_tax_cat = EF_CPT_TAX_CAT;
	$cpt_extr = EF_CPT_EXTR;
	$cpt_tax_cat_extr = EF_CPT_EXTR_TAX;
	$cpt_team = EF_CPT_TEAM;
	$cpt_tax_cat_team = EF_CPT_TEAM_TAX;
} else {
	$cpt = $cpt_tax_cat = $cpt_extr = $cpt_tax_cat_extr = $cpt_team = $cpt_tax_cat_team = '';
}


/**
 * Registering meta boxes
 */

// Page Description
$meta_boxes[] = array(
	'id' 			=> 'header_mb',
	'title' 		=> __( 'Page Description', EF_TDM ),
	'pages' 		=> array( 'page' ),
	'context' 		=> 'normal',
	'priority' 		=> 'high',
	'autosave' 		=> true,
	'fields' 		=> array(
		// TEXT
		array(
			'id'    => "{$prefix}text",
			'desc'  => __( 'Short description of the entire page.', EF_TDM ),
			'quicktags' => false,
			'media'	=> false,
			'dragupload' => false,
			'type'	=> 'wysiwyg',
			'rows'	=> "3",
			'tinymce'	=> array(
				'keep_styles' => false,
				'content_css' => get_template_directory_uri() . '/editor-style.css',
				'forced_root_block' => false,
			    'force_br_newlines' => true,
			    'force_p_newlines' => false,
			    'convert_newlines_to_brs' => true,
				'toolbar1' => 'italic,link,unlink,underline',
				'toolbar2' => '',
				'toolbar3' => '',
			)
		),
	)
);

// Homepage style
$meta_boxes[] = array(
	'id' 			=> 'home_mb',
	'title' 		=> __( 'Home', EF_TDM ),
	'pages' 		=> array( 'page' ),
	'context' 		=> 'normal',
	'priority' 		=> 'high',
	'autosave' 		=> true,
	'fields' 		=> array(
		array(
			'name'    => __( 'Style', EF_TDM ),
			'desc'    => __( 'Choose a style of the page.', EF_TDM ),
			'id'      => "{$prefix}home_style",
			'options' => array(
				'ef-home-default'	=> __( 'Default', EF_TDM ),
				'ef-home-clean'		=> __( 'Minimal', EF_TDM ),
			),
			'type'    => 'radio',
		),
		array(
			'name'    => __( 'Social profiles', EF_TDM ),
			'desc'    => __( 'Show/hide social profiles.', EF_TDM ),
			'id'      => "{$prefix}home_icons",
			'type'    => 'checkbox',
			'std'	  => 1,
		),
		array(
			'name'    => __( '"Scroll To" tip', EF_TDM ),
			'desc'    => __( 'This option is actual for "Minimal" page style (arrow at the bottom left corner).', EF_TDM ),
			'id'      => "{$prefix}home_recent_txt",
			'type'    => 'text',
		),
	),
	'only_on'    => array(
		'template'	=> array( 'templates/home-template.php' )
	)
);

if ( $efto ) {

	$ef_options = array(
		'1' => __( 'Hide', EF_TDM ),
		'2'	=> __( 'Blog Posts', EF_TDM ),
		'3'	=> __( 'Portfolio Posts', EF_TDM ),
		'4'	=> __( 'Custom Set', EF_TDM ),
		'5'	=> __( 'Video', EF_TDM ),
	);

	$ef_options1 = array(
		'1' => __( 'On', EF_TDM ),
		'2' => __( 'Off', EF_TDM ),
	);

	// Homepage

	$meta_boxes[] = array(
		'id' 			=> 'home_slider_mb',
		'title' 		=> __( 'Content', EF_TDM ),
		'pages' 		=> array( 'page' ),
		'context' 		=> 'normal',
		'priority' 		=> 'high',
		'autosave' 		=> true,
		'fields' 		=> array(
			array(
				'name'    => __( 'Content', EF_TDM ),
				'desc'    => __( 'Choose a content type (configure "Theme Options > Home Settings > Content" settings first).', EF_TDM ),
				'id'      => "{$prefix}home_slider",
				'type'    => 'select',
				'options' => $ef_options,
				'std'	  => sprintf( __( 'Global (%s)', EF_TDM ), isset( $efto_data['ef_home_slideshow'] ) ? $ef_options[$efto_data['ef_home_slideshow']] : '2' ),
			),
			array(
				'name' => __( 'Autoplay', EF_TDM ),
				'id'   => "{$prefix}slider_autoplay",
				'desc' => __( 'Enable/disable autoplay (slideshow only).', EF_TDM ),
				'type' => 'radio',
				'options' => array(
					'0' => sprintf( __( 'Global (%s)', EF_TDM ), !empty( $efto_data['ef_flex_auto'] ) ? $ef_options1['1'] : $ef_options1['2'] ),
					'1' => $ef_options1['1'],
					'2' => $ef_options1['2'],
				),
			),
		),
		'only_on'    => array(
			'template'	=> array( 'templates/home-template.php' )
		)
	);
}

// Extra icons
$extra_icn = array(
	'ts-icon-alarm'			=> 'ts-icon-alarm',
	'ts-icon-audio'			=> 'ts-icon-audio',
	'ts-icon-award'			=> 'ts-icon-award',
	'ts-icon-backward'		=> 'ts-icon-backward',
	'ts-icon-bag'			=> 'ts-icon-bag',
	'ts-icon-ball'			=> 'ts-icon-ball',
	'ts-icon-battery'		=> 'ts-icon-battery',
	'ts-icon-bell'			=> 'ts-icon-bell',
	'ts-icon-bigone'		=> 'ts-icon-bigone',
	'ts-icon-blocks'		=> 'ts-icon-blocks',
	'ts-icon-bluetooth'		=> 'ts-icon-bluetooth',
	'ts-icon-browser'		=> 'ts-icon-browser',
	'ts-icon-cup'			=> 'ts-icon-cup',
	'ts-icon-creditcard'	=> 'ts-icon-creditcard',
	'ts-icon-conus'			=> 'ts-icon-conus',
	'ts-icon-contacts'		=> 'ts-icon-contacts',
	'ts-icon-confirm'		=> 'ts-icon-confirm',
	'ts-icon-compass'		=> 'ts-icon-compass',
	'ts-icon-coffee'		=> 'ts-icon-coffee',
	'ts-icon-coctail'		=> 'ts-icon-coctail',
	'ts-icon-cd'			=> 'ts-icon-cd',
	'ts-icon-cloud'			=> 'ts-icon-cloud',
	'ts-icon-cartridge'		=> 'ts-icon-cartridge',
	'ts-icon-camera'		=> 'ts-icon-camera',
	'ts-icon-calendar'		=> 'ts-icon-calendar',
	'ts-icon-cursoure'		=> 'ts-icon-cursoure',
	'ts-icon-delete'		=> 'ts-icon-delete',
	'ts-icon-desctop'		=> 'ts-icon-desctop',
	'ts-icon-diagram'		=> 'ts-icon-diagram',
	'ts-icon-diamond'		=> 'ts-icon-diamond',
	'ts-icon-document'		=> 'ts-icon-document',
	'ts-icon-doodle'		=> 'ts-icon-doodle',
	'ts-icon-download'		=> 'ts-icon-download',
	'ts-icon-drop'			=> 'ts-icon-drop',
	'ts-icon-edit'			=> 'ts-icon-edit',
	'ts-icon-equalizer'		=> 'ts-icon-equalizer',
	'ts-icon-export'		=> 'ts-icon-export',
	'ts-icon-eye'			=> 'ts-icon-eye',
	'ts-icon-icecream'		=> 'ts-icon-icecream',
	'ts-icon-home'			=> 'ts-icon-home',
	'ts-icon-heart'			=> 'ts-icon-heart',
	'ts-icon-grid'			=> 'ts-icon-grid',
	'ts-icon-gamepad'		=> 'ts-icon-gamepad',
	'ts-icon-flash'			=> 'ts-icon-flash',
	'ts-icon-flashdrive'	=> 'ts-icon-flashdrive',
	'ts-icon-flashlight'	=> 'ts-icon-flashlight',
	'ts-icon-folder'		=> 'ts-icon-folder',
	'ts-icon-forward'		=> 'ts-icon-forward',
	'ts-icon-fork'			=> 'ts-icon-fork',
	'ts-icon-import'		=> 'ts-icon-import',
	'ts-icon-key'			=> 'ts-icon-key',
	'ts-icon-keyboard'		=> 'ts-icon-keyboard',
	'ts-icon-lab'			=> 'ts-icon-lab',
	'ts-icon-lamp'			=> 'ts-icon-lamp',
	'ts-icon-letter'		=> 'ts-icon-letter',
	'ts-icon-like'			=> 'ts-icon-like',
	'ts-icon-link'			=> 'ts-icon-link',
	'ts-icon-list'			=> 'ts-icon-list',
	'ts-icon-location'		=> 'ts-icon-location',
	'ts-icon-lock'			=> 'ts-icon-lock',
	'ts-icon-medecine'		=> 'ts-icon-medecine',
	'ts-icon-menuclassic'	=> 'ts-icon-menuclassic',
	'ts-icon-picture'		=> 'ts-icon-picture',
	'ts-icon-pause'			=> 'ts-icon-pause',
	'ts-icon-outlet'		=> 'ts-icon-outlet',
	'ts-icon-notepad'		=> 'ts-icon-notepad',
	'ts-icon-nature'		=> 'ts-icon-nature',
	'ts-icon-mouse'			=> 'ts-icon-mouse',
	'ts-icon-moon'			=> 'ts-icon-moon',
	'ts-icon-more'			=> 'ts-icon-more',
	'ts-icon-mobile'		=> 'ts-icon-mobile',
	'ts-icon-mic'			=> 'ts-icon-mic',
	'ts-icon-messages'		=> 'ts-icon-messages',
	'ts-icon-menushort'		=> 'ts-icon-menushort',
	'ts-icon-menudots'		=> 'ts-icon-menudots',
	'ts-icon-pin'			=> 'ts-icon-pin',
	'ts-icon-planet'		=> 'ts-icon-planet',
	'ts-icon-play'			=> 'ts-icon-play',
	'ts-icon-plus'			=> 'ts-icon-plus',
	'ts-icon-portfolio'		=> 'ts-icon-portfolio',
	'ts-icon-printer'		=> 'ts-icon-printer',
	'ts-icon-processor'		=> 'ts-icon-processor',
	'ts-icon-protect'		=> 'ts-icon-protect',
	'ts-icon-pulse'			=> 'ts-icon-pulse',
	'ts-icon-random'		=> 'ts-icon-random',
	'ts-icon-rating'		=> 'ts-icon-rating',
	'ts-icon-refresh'		=> 'ts-icon-refresh',
	'ts-icon-sale'			=> 'ts-icon-sale',
	'ts-icon-timer'			=> 'ts-icon-timer',
	'ts-icon-temperature'	=> 'ts-icon-temperature',
	'ts-icon-target'		=> 'ts-icon-target',
	'ts-icon-switchers'		=> 'ts-icon-switchers',
	'ts-icon-sun'			=> 'ts-icon-sun',
	'ts-icon-star'			=> 'ts-icon-star',
	'ts-icon-smalllist'		=> 'ts-icon-smalllist',
	'ts-icon-slider'		=> 'ts-icon-slider',
	'ts-icon-share'			=> 'ts-icon-share',
	'ts-icon-settings'		=> 'ts-icon-settings',
	'ts-icon-send'			=> 'ts-icon-send',
	'ts-icon-search'		=> 'ts-icon-search',
	'ts-icon-save'			=> 'ts-icon-save',
	'ts-icon-trash'			=> 'ts-icon-trash',
	'ts-icon-twoblocks'		=> 'ts-icon-twoblocks',
	'ts-icon-unlike'		=> 'ts-icon-unlike',
	'ts-icon-upload'		=> 'ts-icon-upload',
	'ts-icon-video'			=> 'ts-icon-video',
	'ts-icon-user'			=> 'ts-icon-user',
	'ts-icon-volume'		=> 'ts-icon-volume',
	'ts-icon-wallet'		=> 'ts-icon-wallet',
	'ts-icon-washer'		=> 'ts-icon-washer',
	'ts-icon-wi-fi'			=> 'ts-icon-wi-fi',
	'ts-icon-help'			=> 'ts-icon-help',
);

$tax_desc = __( "Select categories you want to include. Note, empty categories aren't available.", EF_TDM );
$excerpt_dsc = __( 'Choose "content" if you want to display the whole post. Also you are able to use WordPress <a target="_blank" href="http://en.support.wordpress.com/splitting-content/more-tag/">"More Tag"</a> to split content manually no matter whatever type you choose.', EF_TDM );


// Meta boxes for the Extras post
$meta_boxes[] = array(
	'id'		=> 'extras_mb',
	'title'		=> __( 'Icon', EF_TDM ),
	'pages'		=> array( $cpt_extr ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(
		array(
			'name'		=> ' ',
			'id'		=> "{$prefix}extra_icons",
			'class'		=> "{$prefix}extra-icons",
			'type'		=> 'extras',
			'options'	=> $extra_icn,
			'std'   	=> "ts-icon-alarm",
		),
	)
);

if ( $ef_cpt ) {

	// Meta boxes for portfolio
	$meta_boxes[] = array(
		'id' 		=> 'portfolio_mb',
		'title' 	=> __( 'Content', EF_TDM ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(

			array(
				'name'    => __( 'Type', EF_TDM ),
				'desc'    => __( '"Paginated" type means that portfolio will be splitted into separate pages with simple pagination. "Infinite" mode provides an ajax loading without a refreshing the whole page.', EF_TDM ),
				'id'      => "{$prefix}portf_type",
				'options' => array(
					"regular"   => "Paginated",
					"filtered"  => "Infinite",
				),
				'type'    => 'radio',
			),

			array(
				'name'    => __( 'Thumbnails size', EF_TDM ),
				'desc'    => __( 'Choose "Small", "Medium" or "Large" size.', EF_TDM ),
				'id'      => "{$prefix}portf_style",
				'options' => array(
					"ef-small-portfolio"		=> __( 'Small', EF_TDM ),
					"ef-medium-portfolio"		=> __( 'Medium', EF_TDM ),
					"ef-large-portfolio"		=> __( 'Large', EF_TDM ),
				),
				'type'    => 'radio',
			),

			array(
				'name' 	=> __( 'Posts Per Page', EF_TDM ),
				'id'   	=> "{$prefix}range",
				'type' 	=> 'slider',
				'js_options' 	=> array(
					'min'   	=> 9,
					'max'   	=> 100,
					'step'  	=> 1,
				),
			),

			array(
				'name'    	=> __( 'Categories', EF_TDM ),
				'desc'    	=> $tax_desc,
				'id'      	=> "{$prefix}cat",
				'type'    	=> 'taxonomy',
				'options' 	=> array(
					'taxonomy' 	=> $cpt_tax_cat,
					'type' 		=> 'select_tree',
					'args' 		=> array( 'hide_empty' => true )
				),
				'multiple' 	=> true,
			),

		),

		'only_on'    => array(
			'template' => array( 'templates/portfolio-template.php' )
		)
	);

	// Portfolio post details
	$meta_boxes[] = array(
		'id'		=> 'post_details_mb',
		'title'		=> __( 'Details', EF_TDM ),
		'pages'		=> array( $cpt ),
		'context'	=> 'normal',
		'priority'	=> 'high',
		'autosave'	=> true,
		'fields'	=> array(
			array(
				'name'    	=> __( 'Client', EF_TDM ),
				'id'		=> "{$prefix}client",
				'type'		=> 'text',
			),
			array(
				'name'    	=> __( 'Services', EF_TDM ),
				'id'		=> "{$prefix}services",
				'type'		=> 'textarea',
				'cols' 		=> 20,
				'rows' 		=> 3,
			),
			array(
				'name'    	=> __( 'Website', EF_TDM ),
				'id'		=> "{$prefix}website",
				'type'		=> 'text',
			),
		)
	);

	// Thumbnail settings
	$meta_boxes[] = array(
		'id'		=> 'img_orientation_mb',
		'title'		=> __( 'Thumbnail settings', EF_TDM ),
		'pages'		=> array( $cpt ),
		'context'	=> 'side',
		'priority'	=> 'low',
		'autosave'	=> true,
		'fields'	=> array(

			array(
				'name'    	=> __( 'Orientation', EF_TDM ),
				'id'		=> "{$prefix}img_orientation",
				'type'		=> 'radio',
				'std'		=> '0',
				'class'		=> 'ef-radio-check',
				'options'	=> array(
					'0' 		=> __( 'Landscape', EF_TDM ),
					'1'			=> __( 'Portrait', EF_TDM )
				),
			),

			array(
				'name'    => __( 'Description', EF_TDM ),
				'desc'	  => __( 'Short description.', EF_TDM ),
				'id'      => "{$prefix}img_credits",
				'type'    => 'text'
			),
		)
	);

	// Meta boxes for regular page
	$meta_boxes[] = array(
		'id' 		=> 'page_mb',
		'title' 	=> __( 'Content', EF_TDM ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(
			array(
				'name'    => __( 'Show', EF_TDM ),
				'id'      => "{$prefix}team_extr",
				'type'    => 'radio',
				'desc'    => __( 'Display "Team" or "Extras" on this page.', EF_TDM ),
				'class'   => 'ef-radio-check',
				'options' => array(
					'0'		=> __( 'Team', EF_TDM ),
					'1'		=> __( 'Extras', EF_TDM ),
				),
			),
		),
		'only_on'    => array(
			'template' => array( 'templates/regular-template.php' )
		)
	);

	$meta_boxes[] = array(
		'id' 		=> 'extra_mb',
		'title' 	=> __( 'Extras', EF_TDM ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(
			array(
				'name'    => __( 'Categories', EF_TDM ),
				'desc'    => $tax_desc,
				'id'      => "{$prefix}cat_extras",
				'type'    => 'taxonomy',
				'options' => array(
					'taxonomy'	=> $cpt_tax_cat_extr,
					'type' 		=> 'select_tree',
					'args' 		=> array( 'hide_empty' => true )
				),
				'multiple' => true,
			),

			array(
				'name'    => __( 'Content', EF_TDM ),
				'id'      => "{$prefix}extr_content",
				'type'    => 'radio',
				'desc'    => $excerpt_dsc,
				'class'   => 'ef-radio-check',
				'options' => array(
					'ef-content' => __( 'Content', EF_TDM ),
					'ef-excerpt' => __( 'Excerpt', EF_TDM ),
				),
			),
		),
		'only_on'    => array(
			'template' => array( 'templates/regular-template.php' )
		)
	);

	$meta_boxes[] = array(
		'id' 		=> 'team_mb',
		'title' 	=> __( 'Team', EF_TDM ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(
			array(
				'name'    => __( 'Categories', EF_TDM ),
				'desc'    => $tax_desc,
				'id'      => "{$prefix}cat_team",
				'type'    => 'taxonomy',
				'options' => array(
					'taxonomy'	=> $cpt_tax_cat_team,
					'type' 		=> 'select_tree',
					'args' 		=> array( 'hide_empty' => true )
				),
				'multiple' => true,
			),

			array(
				'name'    => __( 'Content', EF_TDM ),
				'id'      => "{$prefix}team_content",
				'type'    => 'radio',
				'desc'    => $excerpt_dsc,
				'class'   => 'ef-radio-check',
				'options' => array(
					'ef-content' => __( 'Content', EF_TDM ),
					'ef-excerpt' => __( 'Excerpt', EF_TDM ),
				),
			),
		),
		'only_on'    => array(
			'template' => array( 'templates/regular-template.php' )
		)
	);

	$meta_boxes[] = array(
		'id'		=> 'team_info_mb',
		'title'		=> __( 'Member Settings', EF_TDM ),
		'pages'		=> array( $cpt_team ),
		'context'	=> 'normal',
		'priority'	=> 'high',
		'autosave'	=> true,
		'fields'	=> array(
			array(
				'name'  => __( 'Position/Job title', EF_TDM ),
				'id'    => "{$prefix}member_pos",
				'desc'  => __( 'Specify a member\'s position.', EF_TDM ),
				'type'  => 'text',
			),

			array(
				'name'	=> __( 'Socialize', EF_TDM ),
				'type'	=> "heading",
				'id'	=> "fake_id",
			),

			// Social icons
			array(
				'name'	=> ' ',
				'desc'	=> "Twitter",
				'id'	=> "ef_icon-ef-twitter",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> "Facebook",
				'id'	=> "ef_icon-ef-facebook",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> "LinkedIn",
				'id'	=> "ef_icon-ef-linkedin",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> "Google +",
				'id'	=> "ef_icon-ef-gplus",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> __( 'Send Mail', EF_TDM ),
				'id'	=> "ef_icon-ef-mail",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Instagramm',
				'id'	=> "ef_icon-ef-instagramm",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Behance',
				'id'	=> "ef_icon-ef-behance",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'StumbleUpon',
				'id'	=> "ef_icon-ef-stumbleupon",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Skype',
				'id'	=> "ef_icon-ef-skype",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Pinterest',
				'id'	=> "ef_icon-ef-pinterest",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Youtube',
				'id'	=> "ef_icon-ef-youtube",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Vimeo',
				'id'	=> "ef_icon-ef-vimeo",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Soundcloud',
				'id'	=> "ef_icon-ef-soundcloud",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Tumblr',
				'id'	=> "ef_icon-ef-tumblr",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Vkontakte',
				'id'	=> "ef_icon-ef-vkontakte",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Last.fm',
				'id'	=> "ef_icon-ef-lastfm",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Dribbble',
				'id'	=> "ef_icon-ef-dribbble",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Flickr',
				'id'	=> "ef_icon-ef-flickr",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> 'Digg',
				'id'	=> "ef_icon-ef-digg",
				'type'	=> "text",
				'std'	=> "",
			),

			array(
				'name'	=> ' ',
				'desc'	=> '500px',
				'id'	=> "ef_icon-ef-fivehundredpx",
				'type'	=> "text",
				'std'	=> "",
			),
		)
	);
}

// Video post format
$meta_boxes[] = array(
	'id'		=> 'post_format_video_mb',
	'title'		=> __( 'Post Format Settings', EF_TDM ),
	'pages'		=> array( 'post', $cpt ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(
		array(
			'name'				=> __( 'Mp4 Video', EF_TDM ),
			'id'				=> "{$prefix}mp4_url",
			'desc'				=> __( 'Upload *.mp4 video. This is required field.', EF_TDM ),
			'type'				=> 'file_advanced',
			'max_file_uploads'	=> 1,
			'mime_type'			=> 'video',
		),

		array(
			'name'				=> __( 'Webm Video', EF_TDM ),
			'id'				=> "{$prefix}webm_url",
			'desc'				=> __( 'Upload *.webm video. This is required field.', EF_TDM ),
			'type'				=> 'file_advanced',
			'max_file_uploads'	=> 1,
			'mime_type'			=> 'video',
		),

		array(
			'name'				=> __( 'External link', EF_TDM ),
			'id'				=> "{$prefix}video_url",
			'desc'				=> __( 'Insert a link to Vimeo/Youtube video.', EF_TDM ),
			'type'				=> 'text',
		),
	)
);

// Meta boxes for the blog
$meta_boxes[] = array(
	'id'		=> 'blog_layout_mb',
	'title'		=> __( 'Blog', EF_TDM ),
	'pages'		=> array( 'page' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(
		array(
			'name'    => __( 'Style', EF_TDM ),
			'desc'	  => __( 'Choose between 3 predefined blog styles.', EF_TDM ),
			'id'      => "{$prefix}feed_layout",
			'type'    => 'layout',
			'class'   => 'ef-layout-check ef-blog-feed-layout',
			'options' => array(
				'ef-grid-blog'	  => array(
					0 => $template_url . '/assets/img/admin/grid-blog.gif',
					1 => __( 'Grid', EF_TDM ),
				),
				'ef-classic-blog' => array(
					0 => $template_url . '/assets/img/admin/classic-blog.gif',
					1 => __( 'Classic', EF_TDM ),
				),
				'ef-min-blog'	  => array(
					0 => $template_url . '/assets/img/admin/min-blog.gif',
					1 => __( 'Minimal', EF_TDM ),
				),
			),
		),

		array(
			'name' => __( 'Gallery overview', EF_TDM ),
			'id'   => "{$prefix}show_gallery",
			'desc' => __( 'Show/hide galleries on the main blog page. Note that this option works with "Grid" and "Classic" blog styles only.', EF_TDM ),
			'type' => 'checkbox',
		),

		array(
			'name' => __( 'Categories', EF_TDM ),
			'id'   => "{$prefix}show_cats",
			'desc' => __( 'Show/hide categories menu on the main blog page.', EF_TDM ),
			'type' => 'checkbox',
		),
	),

	'only_on'    => array(
		'template' => array( 'templates/blog-template.php' )
	),
);

// Meta boxes for Blog
$meta_boxes[] = array(
	'id'		=> 'blog_mb',
	'title'		=> __( 'Content', EF_TDM ),
	'pages'		=> array( 'page' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name'    => __( 'Type', EF_TDM ),
			'desc'    => __( '"Paginated" type means that portfolio will be splitted into separate pages with simple pagination. "Infinite" mode provides an ajax loading without a refreshing the whole page.', EF_TDM ),
			'id'      => "{$prefix}portf_type",
			'options' => array(
				"regular"   => __( "Paginated", EF_TDM ),
				"filtered"  => __( "Infinite", EF_TDM ),
			),
			'type'    => 'radio',
		),

		array(
			'name' => __( 'Posts Per Page', EF_TDM ),
			'id'   => "{$prefix}range",
			'type' => 'slider',
			'js_options' => array(
				'min'		=> 5,
				'max'		=> 100,
				'step'		=> 1,
			),
		),

		array(
			'name'    => __( 'Categories', EF_TDM ),
			'desc'    => $tax_desc,
			'id'      => "{$prefix}cat",
			'type'    => 'taxonomy',
			'options' => array(
				'taxonomy' => 'category',
				'type' => 'select',
				'args' => array()
			),
			'multiple' => true,
		),

		array(
			'name'    => __( 'Post content', EF_TDM ),
			'id'      => "{$prefix}extr_content",
			'type'    => 'radio',
			'desc'     => $excerpt_dsc,
			'class'   => 'ef-radio-check',
			'options' => array(
				'ef-content' => __( 'Content', EF_TDM ),
				'ef-excerpt' => __( 'Excerpt', EF_TDM ),
			),
		),

	),

	'only_on'    => array(
		'template' => array( 'templates/blog-template.php' )
	),
);

// Slider
$meta_boxes[] = array(
	'id'		=> 'post_mb',
	'title'		=> __( 'Slideshow', EF_TDM ),
	'pages'		=> array( 'page', $cpt, $cpt_extr, 'post' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name' => __( 'Autoplay', EF_TDM ),
			'id'   => "{$prefix}slider_autoplay",
			'desc' => __( 'Enable/disable autoplay.', EF_TDM ),
			'type' => 'checkbox',
			'std'  => 1,
		),

		array(
			'name' => __( 'Slide Interval', EF_TDM ),
			'id'   => "{$prefix}slider_duration",
			'desc' => __( 'Slide interval time (milliseconds).', EF_TDM ),
			'type' => 'slider',
			'js_options'	=> array(
				'min'		=> 3000,
				'max'		=> 15000,
				'step'		=> 500,
			),
			'std' => 6000,
		),

		array(
			'name'    => __( 'Transition', EF_TDM ),
			'id'      => "{$prefix}slider_fx",
			'desc'    => __( 'Slideshow transition (animation).', EF_TDM ),
			'type'    => 'radio',
			'options' => array(
				'fade'		=> __( 'Fade', EF_TDM ),
				'slide'  	=> __( 'Slide', EF_TDM ),
			),
			'std'	  => 'fade',
		),

		array(
			'name' => __( 'Transition speed', EF_TDM ),
			'id'   => "{$prefix}transition_speed",
			'desc' => __( 'Slideshow transition duration (milliseconds).', EF_TDM ),
			'type' => 'slider',
			'js_options'	=> array(
				'min'		=> 500,
				'max'		=> 2000,
				'step'		=> 100,
			),
			'std' => 800,
		),

		array(
			'name' => __( 'Cover screen', EF_TDM ),
			'id'   => "{$prefix}slider_cover",
			'desc' => __( 'Images will cover the whole screen. Turn it off to show full image (fills screen height).', EF_TDM ),
			'type' => 'checkbox',
			'std'  => 1,
		),

		array(
			'name'          => __( 'Slider images', EF_TDM ),
			'id'            => "{$prefix}upload_img",
			'desc'          => __( "Select images from the WordPress Gallery or upload new ones (width should be not less than 1800 pixels for the 'pixel perfect' appearance). Use drag'n'drop to reorder images.", EF_TDM ),
			'type'             => 'image_advanced',
			'max_file_uploads' => 10,
		),

		array(
			'name' => __( 'Include featured image', EF_TDM ),
			'id'   => "{$prefix}inc_featured_img",
			'desc' => __( 'Include featured image into slideshow.', EF_TDM ),
			'type' => 'checkbox',
			'std'  => 1,
		),
	),
	'exclude_on'    => array(
		'template'	=> array( 'templates/home-template.php', 'templates/contact-template.php' )
	)
);

// Meta boxes for contact
$meta_boxes[] = array(
	'id'		=> 'contact_mb',
	'title'		=> __( 'Map Settings', EF_TDM ),
	'pages'		=> array( 'page' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name'  => __( 'Latitude', EF_TDM ),
			'id'    => "{$prefix}map_lat",
			'desc'  => __( 'Use Google Maps to get the latitude of your location.', EF_TDM ),
			'type'  => 'text',
			'std'   => '',
		),

		array(
			'name'  => __( 'Longitude', EF_TDM ),
			'id'    => "{$prefix}map_long",
			'desc'  => __( 'Use Google Maps to get the longitude of your location.', EF_TDM ),
			'type'  => 'text',
			'std'   => '',
		),

		array(
			'name' => __( 'Zoom level', EF_TDM ),
			'id'   => "{$prefix}map_zoom",
			'type' => 'slider',
			'js_options' => array(
				'min'		=> 5,
				'max'		=> 20,
				'step'		=> 1,
			),
		),

		array(
			'name'          => __( 'Map marker', EF_TDM ),
			'id'            => "{$prefix}upload_marker",
			'desc'          => __( "Upload your custom marker. Note that it must be 180px x 190px.", EF_TDM ),
			'type'			=> 'image_advanced',
			'max_file_uploads' => 1,
		),
	),

	'validation' => array(
		'rules' => array(
			"{$prefix}password" => array(
				'required' => true,
				'minlength' => 7,
			),
		)
	),

	'only_on'    => array(
		'template' => array( 'templates/contact-template.php' )
	)
);


// Sidebars
$meta_boxes[] = array(
	'id'		=> 'menu_mb',
	'title'		=> __( 'Menu', EF_TDM ),
	'pages'		=> array( 'page', 'post', $cpt, $cpt_extr ),
	'context'	=> 'normal',
	'priority'	=> 'default',
	'autosave'	=> true,
	'fields'	=> array(
		array(
			'name'    => 'Layout',
			'desc'	  => __( 'Select a page layout.', EF_TDM ),
			'id'      => "{$prefix}menu_layout",
			'type'    => 'layout',
			'class'   => 'ef-layout-check',
			'options' => ef_mb_get_widget_layouts( isset( $efto_data ) ? $efto_data : null, 'menu', $template_url ),
			'std'     => '0',
		),
	),
);

if ( current_theme_supports( 'ef-custom-post-types' ) ) {
	array_push( $meta_boxes[count($meta_boxes)-1]['fields'], array(
			'name'    => __( 'Widget area', EF_TDM ),
			'desc'    => __( 'Select a widget area. This option overrides settings set up in "Theme Options > Widgets".', EF_TDM ),
			'id'      => "{$prefix}menu_widgets",
			'type'    => 'sidebars',
			'std'	  => isset( $efto_data ) ? __( 'Global', EF_TDM ) : __( 'Default widget area', EF_TDM )
		)
    );
}


$meta_boxes[] = array(
	'id'		=> 'sidebars_mb',
	'title'		=> __( 'Sidebar', EF_TDM ),
	'pages'		=> array( 'page', 'post', $cpt, $cpt_extr ),
	'context'	=> 'normal',
	'priority'	=> 'default',
	'autosave'	=> true,
	'fields'	=> array(
		array(
			'name'    => 'Layout',
			'desc'	  => __( 'Select a page layout.', EF_TDM ),
			'id'      => "{$prefix}sidebar_layout",
			'type'    => 'layout',
			'class'   => 'ef-layout-check',
			'options' => ef_mb_get_widget_layouts( isset( $efto_data ) ? $efto_data : null, 'sidebar', $template_url ),
			'std'     => '0',
		),
	),
);

if ( current_theme_supports( 'ef-custom-post-types' ) ) {
	array_push( $meta_boxes[count($meta_boxes)-1]['fields'], array(
			'name'    => __( 'Widget area', EF_TDM ),
			'desc'    => __( 'Select a widget area. This option overrides settings set up in "Theme Options > Widgets".', EF_TDM ),
			'id'      => "{$prefix}sidebar_widgets",
			'type'    => 'sidebars',
			'std'	  => isset( $efto_data ) ? __( 'Global', EF_TDM ) : __( 'Default widget area', EF_TDM )
		)
    );
}

$meta_boxes[] = array(
	'id'		=> 'footer_mb',
	'title'		=> __( 'Footer', EF_TDM ),
	'pages'		=> array( 'page', 'post', $cpt, $cpt_extr ),
	'context'	=> 'normal',
	'priority'	=> 'default',
	'autosave'	=> true,
	'fields'	=> array(
		array(
			'name'    => 'Layout',
			'desc'	  => __( 'Select a page layout.', EF_TDM ),
			'id'      => "{$prefix}footer_layout",
			'type'    => 'layout',
			'class'   => 'ef-layout-check',
			'options' => ef_mb_get_widget_layouts( isset( $efto_data ) ? $efto_data : null, 'footer', $template_url ),
			'std'     => '0',
		),
	),
	'exclude_on'    => array(
		'template' => array( 'templates/home-template.php' )
	),
);

if ( current_theme_supports( 'ef-custom-post-types' ) ) {
	array_push( $meta_boxes[count($meta_boxes)-1]['fields'], array(
			'name'    => __( 'Widget area', EF_TDM ),
			'desc'    => __( 'Select a widget area. This option overrides settings set up in "Theme Options > Widgets".', EF_TDM ),
			'id'      => "{$prefix}footer_widgets",
			'type'    => 'sidebars',
			'std'	  => isset( $efto_data ) ? __( 'Global', EF_TDM ) : __( 'Default widget area', EF_TDM )
		)
    );
}

/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function rw_register_meta_boxes() {
	global $meta_boxes;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'EF_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			if ( isset( $meta_box['only_on'] ) && ! ef_maybe_include( $meta_box['only_on'] ) ) {
				continue;
			} elseif ( isset( $meta_box['exclude_on'] ) && ef_maybe_exlude( $meta_box['exclude_on'] ) ) {
				continue;
			}

			new EF_Meta_Box( $meta_box );
		}
	}

}

add_action( 'admin_init', 'rw_register_meta_boxes' );

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function ef_maybe_include( $conditions ) {


	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
		return false;
	}

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return true;
	}

	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	}
	elseif ( isset( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}
	else {
		$post_id = false;
	}

	$post_id = (int) $post_id;
	$post    = get_post( $post_id );


	foreach ( $conditions as $cond => $v ) {
		// Catch non-arrays too
		if ( ! is_array( $v ) ) {
			$v = array( $v );
		}

		switch ( $cond ) {
		case 'id':
			if ( in_array( $post_id, $v ) ) {
				return true;
			}
			break;
		case 'parent':
			$post_parent = $post->post_parent;
			if ( in_array( $post_parent, $v ) ) {
				return true;
			}
			break;
		case 'slug':
			$post_slug = $post->post_name;
			if ( in_array( $post_slug, $v ) ) {
				return true;
			}
			break;
		case 'template':
			$template = get_post_meta( $post_id, '_wp_page_template', true );
			if ( in_array( $template, $v ) ) {
				return true;
			}
			break;
		}
	}

	// If no condition matched
	return false;

}

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function ef_maybe_exlude( $conditions ) {
	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
		return true;
	}

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return false;
	}

	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	}
	elseif ( isset( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}
	else {
		$post_id = false;
	}

	$post_id = (int) $post_id;
	$post    = get_post( $post_id );

	foreach ( $conditions as $cond => $v ) {
		// Catch non-arrays too
		if ( ! is_array( $v ) ) {
			$v = array( $v );
		}

		switch ( $cond ) {
		case 'id':
			if ( in_array( $post_id, $v ) ) {
				return true;
			}
			break;
		case 'parent':
			$post_parent = $post->post_parent;
			if ( in_array( $post_parent, $v ) ) {
				return true;
			}
			break;
		case 'slug':
			$post_slug = $post->post_name;
			if ( in_array( $post_slug, $v ) ) {
				return true;
			}
			break;
		case 'template':
			$template = get_post_meta( $post_id, '_wp_page_template', true );
			if ( in_array( $template, $v ) ) {
				return true;
			}
			break;
		}
	}

	// If no condition matched
	return false;
}
