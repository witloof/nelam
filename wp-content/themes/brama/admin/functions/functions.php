<?php

$tpl_url = get_template_directory();

// Demo
if ( file_exists( $tpl_url . '/demo/changer.php' ) ) {
	require_once $tpl_url . '/demo/changer.php';
}
if ( !defined('EF_DEMO_CHANGER' ) ) {
	define( 'EF_DEMO_CHANGER', FALSE );
}

/**

  Performing basic setup
  @since	Brama 1.0

* */
add_action( 'after_setup_theme', 'ef_theme_setup_68163' );
function ef_theme_setup_68163() {

	$tpl_url = get_template_directory();

	// Thumbnails

	$post_types_thmb = array( 'post', 'page' );

	if ( current_theme_supports( 'ef-custom-post-types' ) ) array_push( $post_types_thmb, EF_CPT, EF_CPT_EXTR, EF_CPT_TEAM );
	if ( current_theme_supports( 'woocommerce' ) ) array_push( $post_types_thmb, 'product' );

	add_theme_support( 'post-thumbnails', $post_types_thmb );

	// Add new image sizes
	add_image_size( 'share_thumb', 600, 600, true );
	add_image_size( 'portfolio_portrate', 800, 1000, true );
	add_image_size( 'portfolio_landscape', 800, 450, true );
	add_image_size( 'medium_thumb', 1000, 9999 );
	add_image_size( 'large_thumb', 1800, 9999 );
	add_image_size( 'blog_classix', 1200, 675, true );

	load_theme_textdomain( EF_TDM, $tpl_url . '/languages' );

	add_editor_style();

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'menus' );
	register_nav_menus(	array( 'primary' => __( 'Header Navigation', EF_TDM ) ) );

	//Redux Framework options
	if ( class_exists( 'ReduxFramework' ) && ef_is_plugin_active( 'redux-framework/redux-framework.php' ) ) {
		if ( get_option( 'page_for_posts' ) != 0 ) {
			update_option( 'page_for_posts', 0 );
		}
		add_theme_support( 'ef-theme-options' );
	    require_once( get_template_directory() . '/admin/classes/class.redux.php' );
	}

	//Meta box
	if ( class_exists( 'EF_Meta_Box' ) && ef_is_plugin_active( 'ef-mbox-brama/ef-meta-box.php' ) ) {
	    require_once( get_template_directory() . '/admin/functions/metabox.php' );
	}
}

add_filter( 'image_size_names_choose', 'ef_show_image_sizes' );
function ef_show_image_sizes( $sizes ) {
	$sizes['share_thumb']			= __( 'Custom Thumb 1', EF_TDM );
	$sizes['portfolio_portrate']	= __( 'Custom Thumb 2', EF_TDM );
	$sizes['portfolio_landscape']	= __( 'Custom Thumb 3', EF_TDM );
	$sizes['medium_thumb']			= __( 'Custom Thumb 4', EF_TDM );
	$sizes['large_thumb']			= __( 'Custom Thumb 5', EF_TDM );
	$sizes['blog_classix']			= __( 'Custom Thumb 6', EF_TDM );

	return $sizes;
}


/**

  "Category thumbnails" plugin support
  @since	Brama 1.0

* */
add_theme_support( 'category-thumbnails' );



/**

  Check if plugins are active
  @since	Brama 1.0

* */
function ef_is_plugin_active( $plugin ) {
	return in_array( $plugin, (array) get_option( 'active_plugins', array() ) );
}


/**

  Max content width
  @since	Brama 1.0

* */
if ( !isset( $content_width ) ) {
	$content_width = 1201;
}


/**

  Plugins
  @since	Brama 1.0

* */
add_action( 'tgmpa_register', 'ef_register_required_plugins_68163' );
function ef_register_required_plugins_68163() {

	$plugins_dir = get_template_directory() . '/inc/plugins/';

	$plugins = array(

		// Plugins pre-packaged with a theme

		array(
			'name'				=> 'Fireform CPT - Brama',
			'slug'				=> 'ef-cpt-brama',
			'source'			=> $plugins_dir . 'ef-cpt-brama.zip',
			'required'			=> true,
			'version'			=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => true,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Fireform Meta Box - brama',
			'slug'				=> 'ef-mbox-brama',
			'source'			=> $plugins_dir . 'ef-mbox-brama.zip',
			'required'			=> true,
			'version'			=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => true,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Fireform WP Gallery',
			'slug'				=> 'ef-wp-gallery',
			'source'			=> $plugins_dir . 'ef-wp-gallery.zip',
			'required'			=> true,
			'version'			=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => false,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Fireform Shortcodes - Brama',
			'slug'				=> 'ef-shortcodes-brama',
			'source'			=> $plugins_dir . 'ef-shortcodes-brama.zip',
			'required'			=> true,
			'version'			=> '1.2',
			'force_activation'  => false,
			'force_deactivation'  => false,
			'external_url'		=> '',
		),

		// Plugins from the WordPress Plugin Repository

		array(
			'name'				=> 'Redux Framework',
			'slug'				=> 'redux-framework',
			'version'			=> '3.5.4.3',
			'required'			=> true,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'Category Thumbnails',
			'slug'				=> 'category-thumbnails',
			'version'			=> '1.0.5',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'Contact Form 7',
			'slug'				=> 'contact-form-7',
			'version'			=> '4.1.2',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'WP Instagram Widget',
			'slug'				=> 'wp-instagram-widget',
			'version'			=> '1.3.1',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'Really Simple CAPTCHA',
			'slug'				=> 'really-simple-captcha',
			'version'			=> '1.8.0.1',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'Intuitive Custom Post Order',
			'slug'				=> 'intuitive-custom-post-order',
			'version'			=> '3.0.4',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'WooCommerce',
			'slug'				=> 'woocommerce',
			'version'			=> '2.3.8',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'WooCommerce Shortcodes',
			'slug'				=> 'woocommerce-shortcodes',
			'version'			=> '1.0.0',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'Force Regenerate Thumbnails',
			'slug'				=> 'force-regenerate-thumbnails',
			'version'			=> '2.0.5',
			'required'			=> false,
			'external_url'		=> ''
		),

		array(
			'name'				=> 'YITH WooCommerce Zoom Magnifier',
			'slug'				=> 'yith-woocommerce-zoom-magnifier',
			'version'			=> '1.2.0',
			'required'			=> false,
			'external_url'		=> ''
		),
	);

	$config = array(
		'domain'				=> EF_TDM,
		'default_path'			=> '',
		'parent_menu_slug'		=> 'themes.php',
		'parent_url_slug'		=> 'themes.php',
		'menu'					=> 'tgmpa-install-plugins',
		'has_notices'			=> true,
		'is_automatic'			=> true,
		'message'				=> '',
	);

	tgmpa( $plugins, $config );

}


/**

  Advanced Pagination.
  @since Brama 1.0

* */
function ef_theme_pagination( $total = '', $ef_data = '' ) {

	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$big = 999999999;
	if ( $total == '' ) {
		global $wp_query;
		$total = $wp_query->max_num_pages;
		if ( !$total ) {
			$total = 1;
		}
	}

	if ( $total > 1 ){
		if ( !is_search() && !empty( $ef_data ) && isset( $ef_data['paging_type'] ) && $ef_data['paging_type'] !== 'regular' && ( $ef_data['is_portfolio'] || $ef_data['is_blog'] ) ) {

			echo '<div class="cbp-l-loadMore-text"><div href="' . get_pagenum_link( $paged + 1 ) . '" class="cbp-l-loadMore-text-link text-center"><i class="icon-ef-arrows-cw animate-spin"></i>' . __( 'Loading...', EF_TDM ) . '</div><!-- .cbp-l-loadMore-text-link --></div>';
		} else {
			if ( get_option( 'permalink_structure' ) ) {
				$format = 'page/%#%/';
			} else {
				$format = '&paged=%#%';
			}

			$links = paginate_links( array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, $paged ),
				'total' 		=> $total,
				'mid_size'		=> 3,
				'type' 			=> 'array',
				'prev_text'		=> '&larr;',
				'next_text'		=> '&rarr;',
			) );

			echo '<div class="row"><div class="col-lg-12 text-center"><ul class="pagination pagination-sm">';
			foreach ( $links as $link ) {
				echo '<li>' . $link . '</li>';
			}
			echo '</ul></div></div>';
		}
	}
}

/**

  Post navigation.
  @since Brama 1.0

* */
function ef_theme_post_nav( $ef_data = '' ) {
	global $post;

	$previous = is_attachment() ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( $next || $previous ) {
		printf( '<div id="ef-post-nav" class="row"><div class="col-sm-6 text-center-xs">%1$s %2$s</div><div class="col-sm-6 text-right-md text-right-sm text-right-lg text-center-xs">%3$s %4$s</div></div>', get_previous_post_link( '<h6>%link</h6>', _x( '%title', 'Previous', EF_TDM ) ), $previous ? __( '/ Previous', EF_TDM ) : '', $next ? __( 'Next /', EF_TDM ) : '', get_next_post_link( '<h6>%link</h6>', _x( '%title', 'Next', EF_TDM ) )  );
		echo '<hr>';
	}
}


/**

  Outputs custom page/post titles
  @since Brama 1.0

* */
function ef_page_title( $ef_data = '' ) {
	if ( is_search() ) {
		$title = sprintf( __( 'Search Results for: %s', EF_TDM ), '<span class="ef-drop-span">' . get_search_query() . '</span>' );
	} elseif ( is_404() ) {
		$title = __( 'Oops. The page you are looking for does not exist.', EF_TDM );
	} elseif ( is_archive() ) {
		if ( is_day() ) {
			$title = sprintf( __( 'Daily Archives: %s', EF_TDM ), '<span class="ef-drop-span">' . esc_html( get_the_date( 'F j, Y' ) ) . '</span>' );
		} elseif ( is_month() ) {
			$title = sprintf( __( 'Monthly Archives: %s', EF_TDM ), '<span class="ef-drop-span">' . esc_html( get_the_date( _x( 'F Y', 'monthly archives date format', EF_TDM ) ) ) . '</span>' );
		} elseif ( is_year() ) {
			$title = sprintf( __( 'Yearly Archives: %s', EF_TDM ), '<span class="ef-drop-span">' . esc_html( get_the_date( _x( 'Y', 'yearly archives date format', EF_TDM ) ) ) . '</span>' );
		} elseif ( is_author() ) {
			global $author; $userdata = get_userdata( $author );
			$title = sprintf( __( 'Author Archives: %s', EF_TDM ), '<span class="ef-drop-span">' . $userdata->display_name . "</span>" );
		} elseif ( current_theme_supports( 'woocommerce' ) && ( is_product_category() || is_product_tag() ) ) {
			$title = sprintf( __( 'Product %1s: %2s', EF_TDM ), is_product_category() ? __( 'category', EF_TDM ) : __( 'tag', EF_TDM ), '<span class="ef-drop-span">' . single_term_title( '', false ) . '</span>' );
		} elseif ( is_tax() || is_category() || is_tag() ) {
			$title = sprintf( __( '%1s: %2s', EF_TDM ), is_category() || ( $ef_data['cpt_support'] && is_tax( EF_CPT_TAX_CAT ) ) || ( $ef_data['cpt_support'] && is_tax( EF_CPT_EXTR_TAX ) ) ? __( 'Browsing category', EF_TDM ) : __( 'Browsing tag', EF_TDM ), '<span class="ef-drop-span">' . single_term_title( '', false ) . '</span>' );
		} else {
			if ( current_theme_supports( 'woocommerce' ) && is_post_type_archive( 'product' ) && !get_option( 'woocommerce_shop_page_id' ) ) {
				$title = __( 'Product Archives', EF_TDM );
			} elseif ( $ef_data['cpt_support'] && is_post_type_archive( EF_CPT ) ) {
				$title = __( 'Portfolio Archives', EF_TDM );
			} else if ( is_post_type_archive( 'post' ) ) {
				$title = __( 'Blog Archives', EF_TDM );
			} else {
				$title = get_the_title( $ef_data['post_id'] );
			}
		}
	} elseif ( get_option( 'page_for_posts' ) && is_home() && !( is_home() && is_front_page() ) ) {
		$title = get_the_title( get_option( 'page_for_posts' ) );
	} else {
		$title = get_the_title();
	}

	echo $title;
}


/**

  Get an author info (when it's specified)
  @since Brama 1.0

* */
function ef_theme_author_info( $ef_data = null ) {

	global $post;

	if ( null !== $post ) {
		$author_id = get_post( $post->id )->post_author;

		if ( get_the_author_meta( 'description', $author_id ) ) {

			if ( is_single() ) {
				$infotmp = sprintf( '<a title="%1$s" href="%2$s">%3$s</a>',
					esc_attr( sprintf( __( 'View all posts by %s', EF_TDM ), get_the_author_meta( 'display_name', $author_id ) ) ),
					esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
					get_avatar( get_the_author_meta( 'user_email', $author_id ), apply_filters( 'brama_author_bio_avatar_size', 160 ) )
				);
			} else {
				$infotmp = sprintf( '%1$s', get_avatar( get_the_author_meta( 'user_email', $author_id ), apply_filters( 'brama_author_bio_avatar_size', 160 ) )
				);
			}

			if ( $post->post_type == 'post' ) {
				$num_post = number_format_i18n( get_the_author_posts( 'ID' ) );
				if ( $num_post > 1 ) {
					if ( is_single() ) {
						$num_posts = '<a href="'.esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ).'" class="ef-author-posts">'.sprintf( __( '%s posts', EF_TDM ), $num_post ).'</a>';
					} else {
						$num_posts = $num_post > 1 ? '<span class="ef-author-posts">'.sprintf( __( '%s posts', EF_TDM ), $num_post ).'</span>' : '';
					}
				} else {
					$num_posts = '';
				}

				return '<div class="vcard author">
						<div class="ef-author-info">
							<div class="ef-avatar pull-left">'.$infotmp.'</div>'.$num_posts.'
							<div class="post-comm">
								<h4>'.sprintf( __( 'About %s:', EF_TDM ), get_the_author_meta( 'display_name', $author_id ) ).'</h4>
								<p class="ef-no-margin">'.get_the_author_meta( 'description', $author_id ).'</p>
							</div>
						</div>
					</div>';
			} else {
				return '';
			}
		}
	} else {
		return '';
	}
}


/**

  Prints HTML with meta information for the current post.
  @since Brama 1.0

* */
function ef_theme_posted_on( $post = false, $ef_data = '', $sngl = true ) {

	global $author;

	$postID = $post->ID;

	$output = '';

	/* Date */

	if ( $sngl ) {
		$output = '<ul class="ef-posted-on entry-meta list-inline">';

		if ( $ef_data['cpt_support'] && is_singular( EF_CPT ) ) {
			$termslist = get_the_term_list( $postID, EF_CPT_TAX_CAT, '', ', ' );

			if ( !empty( $termslist ) ) {
				$output .= sprintf( '<li>%1$s %2$s</li>', $ef_data['cpt_support'] && is_singular( EF_CPT ) ? __( 'In', EF_TDM )  : __( 'in', EF_TDM ), strip_tags( $termslist, '<a>' ) );
			}
		} else {
			$output .= sprintf( '<li>%1$s <a href="%2$s" title="%3$s"><time class="entry-date updated" datetime="%4$s">%5$s</time></a></li> ',
				is_attachment() ? __( 'Uploaded on', EF_TDM ) : __( 'On', EF_TDM ),
				esc_url( get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ) ),
				esc_attr( get_the_time( 'g:i A', $postID ) ),
				esc_attr( get_the_date( 'c', $postID ) ),
				esc_html( get_the_date( '', $postID ) )
			);
		}
	}

	if ( is_singular() && !$sngl ) {

		/* Author */

		if ( is_singular( 'post' ) ) {
			$output .= sprintf( '<li>%1$s <a href="%2$s" title="%3$s">%4$s</a></li>',
				__( 'Posted by', EF_TDM ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf( __( 'View all posts by %s', EF_TDM ), get_the_author_meta( 'display_name' ) ) ),
				get_the_author_meta( 'display_name' )
			);
		}

		/* Categories */

		if ( is_singular( 'post' ) ) {
			$termslist = get_the_term_list( $postID, 'category', '', ', ', '' );

			if ( !empty( $termslist ) ) {
				$output .= sprintf( '<li>%1$s %2$s</li>', $ef_data['cpt_support'] && is_singular( EF_CPT ) ? __( 'In', EF_TDM )  : __( 'in', EF_TDM ), strip_tags( $termslist, '<a>' ) );
			}
		}
	}

	/* Attachment info */

	if ( is_attachment() ) {
		$metadata = wp_get_attachment_metadata();
		$output .= sprintf( '<li>' . __( 'Full size', EF_TDM ) . ' <a href="%1$s" title="'.esc_attr( __( 'Link to full-size image', EF_TDM ) ).'">%2$s &times; %3$s</a></li>',
			esc_url( wp_get_attachment_url() ),
			$metadata['width'],
			$metadata['height']
		);

		$parent_ttl = get_the_title( $post->post_parent );
		$ttl = get_the_title( $postID );

		if ( $parent_ttl !== $ttl ) {
			$output .= sprintf( '<li>' . __( 'Uploaded to', EF_TDM ) . ' ' . '<a href="%1$s" title="'.esc_attr( __( 'Return to %2$s', EF_TDM ) ).'">"%3$s"</a></li>',
				esc_url( get_permalink( $post->post_parent ) ),
				esc_attr( get_the_title( $post->post_parent ) ),
				$parent_ttl
			);
		}
	}

	if ( $sngl ) {

		$output .= '</ul>';
	}

	return $output;
}


/**

  Prints HTML with meta information for the current post (tags).
  @since Brama 1.0

* */
function ef_theme_posted_in( $post = '', $ef_data = '' ) {

	$output = '<ul class="ef-posted-in list-unstyled entry-meta">';

	$output .= '<li><ul class="list-inline">';

	$output .= ef_theme_posted_on( $post, $ef_data, false );

	/* Comments */

	$num_comments = get_comments_number();
	if ( comments_open() ) {
		if ( $num_comments == 0 ) {
			$comments = '';
		} elseif ( $num_comments > 1 ) {
			$comments = sprintf( __( '&#8212; %s comments', EF_TDM ), $num_comments );
		} else {
			$comments = __( '&#8212; 1 comment', EF_TDM );
		}
		$comments_num = $comments;

		$output .= $num_comments > 0 ? sprintf( '<li>%s</li>', $comments_num ) : '';
	} else {
		$output .= $post->post_type == 'post' ? sprintf( '<li>%s</li>', __( 'Comments are off for this post', EF_TDM ) ) : '';
	}

	$output .= '</ul></li>';

	/* Tags/taxonomies */

	if ( !is_attachment() ) {
		if ( is_tax() || ( $ef_data['cpt_support'] && is_singular( EF_CPT ) ) ) {
			$tag_list = get_the_term_list( $post->ID, EF_CPT_TAX_TAG, '#', ' #', '' );
		} else {
			$tag_list = get_the_tag_list( '#', ' #', '' );
		}

		if ( !empty( $tag_list ) ) {
			$output .= sprintf( '<li class="ef-posted-tag icon-ef-tag-1">%s</li>', $tag_list );
		}
	}

	$output .= '</ul>';
	return $output;
}


/**

  Check if an array is empty.
  @since Brama 1.0

* */
function ef_array_empty( $mixed ) {
	if ( is_array( $mixed ) ) {
		foreach ( $mixed as $value ) {
			if ( !ef_array_empty( $value ) ) {
				return false;
			}
		}
	}
	elseif ( !empty( $mixed ) ) {
		return false;
	}
	return true;
}


/**

  Get admin info.
  @since Brama 1.0

* */
function ef_get_admin_info() {
	$admin = get_user_meta(1);
	if ( !empty( $admin['description'][0] ) ) {
		echo '<div class="vcard author text-left"><div class="ef-avatar pull-left">' . get_avatar( 1, apply_filters( 'brama_author_bio_avatar_size', 160 ) ) . '</div><h1 id="ef-main-description" class="ef-author-bio">' . $admin["description"][0] . '</h1></div>';
	}
}


/**

  Prints gallery in the posts list.
  @since Brama 1.0

* */
function ef_call_gallery_imgs( $post = '', $tag = '' ) {
	if ( !empty( $post ) && get_post_gallery( $post->ID ) ) {

		$gal = get_post_gallery( $post->ID, false );

		echo $tag == 'a' ? '<a class="ef-gallery-overview" href="' . esc_url( get_permalink( $post->ID ) ) . '">' : '<span class="ef-gallery-overview">';
		foreach ( $gal['src'] as $attachment_src ) {
			echo '<span>' . '<img src="' . $attachment_src . '"/>' . '</span>';
		}
		echo $tag == 'a' ? '</a>' : '</span>';
	}
}


/**

  Get attachment id by image src.
  @since Brama 1.0

* */
function ef_get_attachment_id_from_src( $image_src ) {
	global $wpdb;
	$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
	$id = $wpdb->get_var( $query );
	return $id;
}



/**

  Check if portfolio archives aren't empty.
  @since Brama 1.0

* */
function ef_if_has_portfolios() {
	if ( is_archive() ) {
		$args = array(
		    'meta_key' => '_thumbnail_id',
		    'post_status' => 'publish',
		    'post_type' => EF_CPT
		);
	    $q = new WP_Query( $args );
	  	return $q->found_posts !== 0;
	} else {
		return true;
	}
}


/**

  Social profiles.
  @since Brama 1.0

* */
function ef_add_social_profiles( $ef_data = '' ) {
	if ( $ef_data['to_support'] && count( $ef_data['socialize'] ) > 0 ) {
		echo '<ul id="ef-social-bar" class="list-inline">';
		$l = 1;
		foreach( $ef_data['socialize'] as $title => $icon ) {
			if ( $l <= 7 ) {
				if ( function_exists( 'icl_register_string' ) ) {
					icl_register_string( __( 'Profiles/Social Network Links', EF_TDM ), esc_attr( $title ), $icon['url'] );
				}
				$wpml_icon_url = function_exists( 'icl_t' ) ? icl_t( __( 'Profiles/Social Network Links', EF_TDM ), esc_attr( $title ), $icon['url'] ) : $icon['url'];
				echo '<li><a class="' . $icon['class'] . '" target="_blank" href="' . esc_url( $wpml_icon_url ) . '" title="' . esc_attr( $title ) . '"></a></li>';
				$l++;
			}
		}
		echo '</ul><!-- #ef-social-bar.list-inline -->';
	}
}


/**

  Sharing bar.
  @since Brama 1.0

* */
function ef_add_share_btns( $ef_data = '', $title = '', $permalink = '' ) {
	if ( $ef_data['to_support'] && !( count( $ef_data['ef_sharing']['enabled'] ) === 1 ) ) {
		$i = 0;
		$btns = '<ul class="ef-share-buttons">';
		foreach ( $ef_data['ef_sharing']['enabled'] as $cls => $ttl ) {
			if ( $i <= 5 ) {
				switch( $cls ) {
					case 'icon-ef-facebook':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://www.facebook.com/sharer.php?u='.$permalink.'&amp;t='.$title.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-linkedin':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.$permalink.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-twitter':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://twitter.com/share?text='.$title.'&amp;url='.$permalink.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-mail':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="mailto:?Subject='.$title.'&amp;Body='.$permalink.'"></a></li>';
					break;
					case 'icon-ef-gplus':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="https://plus.google.com/share?url='.$permalink.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-reddit':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://reddit.com/submit?url='.$permalink.'&amp;title='.$title.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-stumbleupon':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://www.stumbleupon.com/submit?url='.$permalink.'&amp;title='.$title.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-vkontakte':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://vkontakte.ru/share.php?url='.$permalink.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-digg':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="http://digg.com/submit?url='.$permalink.'&amp;title='.$title.'" target="_blank"></a></li>';
					break;
					case 'icon-ef-pinterest':
						$btns .= '<li><a title="'.$ttl.'" class="'.$cls.'" href="#" target="_blank"></a></li>';
					break;
				}
			}
			$i++;
		}
		$btns .= '</ul>';
		return $btns;
	} else {
		return false;
	}
}



/**

  WPML custom language switcher.
  @since Brama 1.0

* */
function ef_wpml_lang_switcher() {
	if ( function_exists( 'icl_get_languages' ) ) {
		$output = '';
    	$langs_array = icl_get_languages( 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
    	if ( count( $langs_array ) > 1 ) {
			$output .= '<ul id="ef-wpml-lang-switcher" class="list-inline">';
	    	foreach ( $langs_array as $lang_code => $prop ) {
	    		$active = $prop['active'] ? ' class="ef-active-lang"' : '';
	    		$output .= '<li' . $active . '><a href="' . esc_url( $prop['url'] ) . '" title="' . esc_attr( $prop['translated_name'] ) . '">' . esc_html( $lang_code ) . '</a></li>';
	    	}
	    	$output .= '</ul>';
    	}
    }

    return isset( $output ) && !empty( $output ) ? $output : false;
}


/**

  Dynamic CSS.
  @since Brama 1.12

* */
function ef_dynamic_css_68163() {
	require( get_template_directory() . '/assets/css/options.php' );
	exit;
}
add_action( 'wp_ajax_ef_dynamic_css_68163', 'ef_dynamic_css_68163' );
add_action( 'wp_ajax_nopriv_ef_dynamic_css_68163', 'ef_dynamic_css_68163' );