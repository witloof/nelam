<?php

function ef_primary_navigation() {
	wp_nav_menu( array(
			'container'			=> false,
			'container_class'	=> 'menu',
			'menu'				=> '',
			'menu_class'		=> 'top-bar-menu left',
			'theme_location'	=> 'primary',
			'before'			=> '',
			'after'				=> '',
			'link_before'		=> '',
			'link_after'		=> '',
			'depth'				=> 5,
			'fallback_cb'		=> 'ef_nav_fallback',
			'walker'			=> new top_bar_walker()
		) );
}

function ef_nav_fallback() {
	echo '<ul class="top-bar-menu left">';
	wp_list_pages( array(
			'depth'				=> 0,
			'child_of'			=> 0,
			'exclude'			=> '',
			'include'			=> '',
			'title_li'			=> '',
			'echo'				=> 1,
			'authors'			=> '',
			'sort_column'		=> 'menu_order, post_title',
			'link_before'		=> '',
			'link_after'		=> '',
			'walker'			=> new page_walker(),
			'post_type'			=> 'page',
			'post_status'		=> 'publish'
		) );
	echo '</ul>';
}

class top_bar_walker extends Walker_Nav_Menu {

	function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
		$element->has_children = !empty( $children_elements[$element->ID] );
		$element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
		$element->classes[] = ( $element->has_children ) ? 'has-dropdown' : '';
		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$item_html = '';
		parent::start_el( $item_html, $item, $depth, $args );
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$output .= $item_html;
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "\n<ul class=\"sub-menu dropdown\">\n";
	}
}

class page_walker extends Walker_Page {

	function start_el( &$output, $page, $depth = 0, $args = array(), $id = 0 ) {
		global $post;
		$item_html = '';
		$current_page = isset( $post ) ? $post->ID : '';
		parent::start_el( $item_html, $page, $depth, $args, $current_page );
		$css_class[] = 'page_item';
		$css_class[] = 'page-item-'.$page->ID;
		$css_class[] = $args['has_children'] ? 'has-dropdown' : '';

		if ( !empty( $current_page ) ) {
			$_current_page = get_page( $current_page );
			$css_class[] = isset( $_current_page->ancestors ) && in_array( $page->ID, (array) $_current_page->ancestors ) ? 'current_page_ancestor' : '';
			$css_class[] = $_current_page && $page->ID == $_current_page->post_parent ? 'current_page_parent active' : '';
			$css_class[] = $page->ID == $current_page ? 'current_page_item active' : '';
		} elseif ( $page->ID == get_option( 'page_for_posts' ) ) {
			$classes[] = 'current_page_parent';
		}

		$css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $id = 0 ) );
		$item_html = '<li class="' . $css_class . '"><a href="' . esc_url( get_permalink( $page->ID ) ) . '">' . apply_filters( 'the_title', $page->post_title, $page->ID ) . '</a>';

		$output .= $item_html;
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "\n<ul class=\"sub-menu dropdown\">\n";
	}
}