<?php

defined( 'ABSPATH' ) || exit;

if ( !class_exists( 'EfPrintImages' ) ) {
	class EfPrintImages	{

		private static $img_data = array();

		public static function output( $ef_data = '' ) {
	        return self::htmlOutput( $ef_data );
	    }

	    public static function has_category_thumb( $ef_data = '' ) {
	    	if ( is_tax() ) {
	    		$queried_object = get_queried_object();
				$term_id = $queried_object->term_id;
	    	} else {
	    		$term_id = '';
	    	}

	    	return ( is_category() || is_tax() ) && ( function_exists( 'has_category_thumbnail' ) && has_category_thumbnail( $term_id ) );
	    }

	    /**

		Thumbnail arguments.
		@since Brama 1.0

		* */
		public static function thumbArgs( $ef_data = '', $thumbID = '' ) {
			if ( !empty( $ef_data ) && self::has_category_thumb( $ef_data ) ) {
				$thumb_data = get_the_category_data();
				$lg_image_src = isset( $thumb_data->sizes->large_thumb ) ? $thumb_data->sizes->large_thumb->url : null;
				$md_image_src = isset( $thumb_data->sizes->medium_thumb ) ? $thumb_data->sizes->medium_thumb->url : null;
				$share_image_src = isset( $thumb_data->sizes->share_thumb ) ? $thumb_data->sizes->share_thumb->url : null;
				self::$img_data['large_thumb'] = null !== $lg_image_src ? $lg_image_src : $thumb_data->url;
				self::$img_data['medium_thumb'] = null !== $md_image_src ? $md_image_src : $thumb_data->url;
				self::$img_data['share_thumb'] = null !== $share_image_src ? $share_image_src : $thumb_data->url;
				self::$img_data['alt'] = $thumb_data->alt;
			} elseif ( !empty( $thumbID ) ) {
				$full_image_src = wp_get_attachment_image_src( $thumbID, 'full' );
				$lg_image_src = wp_get_attachment_image_src( $thumbID, 'large_thumb' );
				$md_image_src = wp_get_attachment_image_src( $thumbID, 'medium_thumb' );
				$share_image_src = wp_get_attachment_image_src( $thumbID, 'share_thumb' );
				self::$img_data['large_thumb'] = $lg_image_src ? $lg_image_src[0] : $full_image_src[0];
				self::$img_data['medium_thumb'] = $md_image_src ? $md_image_src[0] : $full_image_src[0];
				self::$img_data['share_thumb'] = $share_image_src ? $share_image_src[0] : $full_image_src[0];
				self::$img_data['alt'] = get_post_meta( $thumbID, '_wp_attachment_image_alt', true );
			}

			return self::$img_data;
		}

	    /**

		Printing thumbnail.
		@since Brama 1.0

		* */
		public static function getThumb( $src_lg = '', $src_mob = '', $alt = '', $isParallax = true, $postID = false ) {
			$parallaxAttr = $isParallax ? ' ef-parallax-block' : '';
			$poster = $postID && get_post_format( $postID ) ? ' ef-poster-image' : '';
			return "<img class='ef-adjust-position{$parallaxAttr}{$poster}' src='{$src_lg}' data-src='{$src_mob}' alt='{$alt}' />";
		}

		/**

		php image luminance detector.
		@since Brama 1.0

		* */
		public static function is_dark_mage( $filepath = null, $num_samples = 10 ) {
			if ( !$filepath ) return false;

		    if ( function_exists( 'curl_version' ) ) {
		    	$ch = curl_init();
			    curl_setopt( $ch, CURLOPT_URL, $filepath );
			    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			    curl_setopt( $ch, CURLOPT_BINARYTRANSFER, 1 );
			    $data = curl_exec( $ch );
			    curl_close( $ch );
			    $img = @imagecreatefromstring( $data );
		    }

		    if ( empty( $img ) ) {
		    	$image = @getimagesize( $filepath );
		    	if ( !$image ) return false;
		    	$allowedMimeTypes = array( 'image/gif', 'image/jpeg', 'image/png' );
			    if ( !in_array( $image['mime'], $allowedMimeTypes ) ) return false;
			    $img = null;
			    switch ( $image['mime'] ) {
			        case 'image/gif' :
			            $img = @imageCreateFromGif( $filepath );
			        break;
			        case 'image/jpeg' :
			            $img = @imageCreateFromJpeg( $filepath );
			        break;
			        case 'image/png' :
			            $img = @imageCreateFromPng( $filepath );
			        break;
			    }
		    }

		    if ( !( $img && function_exists('imagesx') && function_exists('imagecolorat') ) ) return false;

		    $width = imagesx( $img );
		    $height = imagesy( $img );
		    $x_step = intval( $width / $num_samples );
		    $y_step = intval( $height / $num_samples );
		    $total_lum = 0;
		    $sample_no = 1;

		    for ( $x = 0; $x < $width; $x += $x_step ) {
		        for ( $y = 0; $y < $height; $y += $y_step ) {
		            $rgb = imagecolorat( $img, $x, $y );
		            $r = ( $rgb >> 16 ) & 0xFF;
		            $g = ( $rgb >> 8 ) & 0xFF;
		            $b = $rgb & 0xFF;
		            $lum = ( $r+$r+$b+$g+$g+$g ) / 6;
		            $total_lum += $lum;
		            $sample_no++;
		        }
		    }

		    $avg_lum  = $total_lum / $sample_no;

		    imagedestroy( $img );

		    return $avg_lum < 170;
		}

		/**

		Get video.
		@since Brama 1.0

		* */
		public static function video_meta( $postID = false ) {
			$mp4_id = efmb_meta( 'ef_mp4_url', 'type=file_advanced' );
			$webm_id = efmb_meta( 'ef_webm_url', 'type=file_advanced' );
			$ext_vid = efmb_meta( 'ef_video_url', 'type=text' );
			$poster = $postID ? self::thumbArgs( false, get_post_thumbnail_id( $postID ) ) : false;

			return array(
				1 => wp_get_attachment_url( $mp4_id ),
				2 => wp_get_attachment_url( $webm_id ),
				3 => $poster ? $poster['medium_thumb'] : '',
				4 => !empty( $ext_vid ) ? $ext_vid : false,
			);
		}

		/**

		Print video.
		@since Brama 1.0

		* */
		public static function video( $mp4 = false, $webm = false, $poster = false, $ext_vid = false, $loop = false, $wrapper = true, $is_home = false, $autoplay = false ) {
			if ( $ext_vid ) {
				if ( strpos( $ext_vid, 'vimeo.com' ) ) {
					$vid = wp_oembed_get( $ext_vid );
					$vid = str_replace( '<iframe ', '<iframe id="ef-ext-video-v" ', $vid );
					$prov = 'vimeo';
				} else if ( strpos( $ext_vid, 'youtube.com' ) ) {
					$vid = '<video id="ef-video-player-' . uniqid() . '" width="640" height="360" style="width: 100%; height: 100%;">
						<source type="video/youtube" src="' . $ext_vid . '" />
					</video>';
					$prov = 'ytb';
				} else {
					return false;
				}
			} else if ( !empty( $mp4 ) && !empty( $webm ) ) {
				$autoplay = $autoplay ? ' autoplay' : '';
				$loop = $loop ? ' loop' : '';

				$vid = '<video id="ef-video-player-' . uniqid() . '" width="640" height="360"' . $loop . $autoplay . ' controls="controls" preload="metadata" style="width: 100%; height: 100%;" poster="' . $poster . '">
					<source src="' . $mp4 . '?_=1" type="video/mp4"></source>
					<source src="' . $webm . '?_=1" type="video/webm"></source>
				</video>';
				$prov = 'html5';
			}

			if ( isset( $vid ) ) {
				if ( $wrapper ) {
					if ( $is_home ) {
						echo '<div id="ef-video-header" class="ef-parallax-block ef-adjust-position ef-html5-vid">' . $vid . '</div>';
					} else {
						echo '<div class="ef-ext-vid ef-' . $prov . '-vid">' . $vid . '</div><img alt="" src="' . $poster . '" class="ef-vid-poster ef-adjust-position ef-parallax-block ef-' . $prov . '-vid"><a href="#" class="mejs-overlay-button ef-vid-play ef-' . $prov . '-vid"></a>';
					}
				} else {
					$pass = post_password_required() ? '<span class="ef-pass ts-icon-lock"></span>' : '';
					echo '<div class="ef-thumb">' . $vid . $pass . '</div>';
				}
			}
		}

		/**

		Latest posts.
		@since Brama 1.0

		* */
		public static function slides( $ef_data = '', $num = 5 ) {
			if ( $ef_data['home_slideshow'] ) {
				if ( $ef_data['home_slideshow'] === '1' ) {
					$num = 0;
				} elseif ( $ef_data['home_slideshow'] === '2' ) {
					$num = $ef_data['ef_blog_slideshow'];
					$post_type = 'post';
				} elseif ( $ef_data['home_slideshow'] === '3' && $ef_data['cpt_support'] ) {
					$num = $ef_data['ef_portf_slideshow'];
					$post_type = EF_CPT;
				} elseif ( $ef_data['home_slideshow'] === '4' && !empty( $ef_data['ef_home_slides'] ) ) {
					$num = count( $ef_data['ef_home_slides'] );
				} elseif ( $ef_data['home_slideshow'] === '5' && !empty( $ef_data['ef_mp4vid_upload']['url'] ) && !empty( $ef_data['ef_webmvid_upload']['url'] ) && !empty( $ef_data['ef_vid_image_upload']['url'] ) ) {
					$num = 0;
					$video = true;
				} else {
					$num = 0;
				}
			} else {
				$post_type = 'post';
			}

			$content_array = array();

			if ( $num > 0 ) {
				if ( isset( $post_type ) ) {
					if ( $ef_data['cpt_support'] && $post_type === EF_CPT ) {
						$inc = !empty( $ef_data['ef_include_portf'] ) ? $ef_data['ef_include_portf'] : '';
					} else {
						$inc = !empty( $ef_data['ef_include_posts'] ) ? $ef_data['ef_include_posts'] : '';
					}
					$args = array(
					    'numberposts'	=> $num,
					    'post_status'	=> 'publish',
					    'include'		=> $inc,
					    'meta_key'		=> '_thumbnail_id',
					    'post_type'		=> $post_type,
					);

		    		$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
					for ( $i = 0; $i < count( $recent_posts ); ++$i ) {
						$pt = get_post_type( $recent_posts[$i]['ID'] );
						if ( $pt === 'post' ) {
							$desc = ef_theme_posted_on( get_post( $recent_posts[$i]['ID'] ), $ef_data );
						} elseif ( $ef_data['mb_support'] && $pt === EF_CPT ) {
							$desc = '<p>' . wp_kses( efmb_meta( 'ef_img_credits', 'type=text', $recent_posts[$i]['ID'] ), array( 'img' => array( 'src' => array(), 'alt' => array() ), 'br' => array(), 'strong' => array() ) ) . '</p>';
						} else {
							$desc = '';
						}
						$lnk = get_permalink( $recent_posts[$i]['ID'] );
					    $content_array[$i]['thumbs'] = self::thumbArgs( $ef_data, get_post_thumbnail_id( $recent_posts[$i]['ID'] ) );
					    $content_array[$i]['title'] = '<h2><a href="' . $lnk . '">' . $recent_posts[$i]['post_title'] . '</a></h2>';
					    $content_array[$i]['description'] = $desc;
					    $content_array[$i]['url'] = $lnk;
					}
				} else {
					$slides = $ef_data['ef_home_slides'];

					if ( count( $slides ) > 0 ) {
						for ( $i = 0; $i < count( $slides ); ++$i ) {
							if ( !empty( $slides[$i]['attachment_id'] ) ) {
								$slide_context =  'Slide #' . $i;

								$slide_title = !empty( $slides[$i]['title'] ) ? wp_kses( $slides[$i]['title'], array( 'br' => array(), 'strong' => array() ) ): false;
								$slide_desc = !empty( $slides[$i]['description'] ) ? wp_kses( $slides[$i]['description'], array( 'br' => array(), 'strong' => array() ) ) : false;
								$slide_lnk = !empty( $slides[$i]['url'] ) ? esc_url( $slides[$i]['url'] ) : false;

								if ( function_exists( 'icl_register_string' ) ) {
									if ( $slide_lnk ) {
                                        icl_register_string( $slide_context, 'Link', $slide_lnk );
                                    }
                                    if ( $slide_desc ) {
                                        icl_register_string( $slide_context, 'Description', $slide_desc );
                                    }
                                    if ( $slide_title ) {
                                        icl_register_string( $slide_context, 'Title', $slide_title );
                                    }
                                }

                                if ( function_exists( 'icl_t' ) ) {
                                    if ( $slide_lnk ) {
                                        $slide_lnk = icl_t( $slide_context, 'Link', $slide_lnk );
                                    }
                                    if ( $slide_desc ) {
                                        $slide_desc = icl_t( $slide_context, 'Description', $slide_desc );
                                    }
                                    if ( $slide_title ) {
                                        $slide_title = icl_t( $slide_context, 'Title', $slide_title );
                                    }
                                }

							    $content_array[$i]['thumbs'] = self::thumbArgs( $ef_data, $slides[$i]['attachment_id'] );
							    if ( $slide_lnk ) {
							    	$content_array[$i]['title'] = $slide_title ? '<h2><a href="' . $slide_lnk . '">' . $slide_title . '</a></h2>' : '';
							    } else {
							    	$content_array[$i]['title'] = $slide_title ? '<h2>' . $slide_title . '</h2>' : '';
							    }
							    $content_array[$i]['description'] = $slide_desc ? '<p>' . $slide_desc . '</p>' : '';
							    $content_array[$i]['url'] = $slide_lnk ? $slide_lnk : '';
							}
						}
					}
				}
			} elseif ( isset( $video ) ) {
				$content_array['video'] = true;
				$img = self::thumbArgs( $ef_data, $ef_data['ef_vid_image_upload']['id'] );
				$content_array[0]['thumbs']['medium_thumb'] = $img['medium_thumb'];
				$content_array[0]['thumbs']['share_thumb'] = $img['share_thumb'];
				$content_array['mp4'] = $ef_data['ef_mp4vid_upload']['url'];
				$content_array['webm'] = $ef_data['ef_webmvid_upload']['url'];

				$slide_title = !empty( $ef_data['ef_vid_title'] ) ? wp_kses( $ef_data['ef_vid_title'], array( 'br' => array(), 'strong' => array() ) ): false;
				$slide_desc = !empty( $ef_data['ef_vid_desc'] ) ? wp_kses( $ef_data['ef_vid_desc'], array( 'br' => array(), 'strong' => array() ) ) : false;
				$slide_lnk = !empty( $ef_data['ef_vid_lnk'] ) ? esc_url( $ef_data['ef_vid_lnk'] ) : false;

				if ( function_exists( 'icl_register_string' ) ) {
					if ( $slide_lnk ) {
                        icl_register_string( 'Video', 'Link', $slide_lnk );
                    }
                    if ( $slide_desc ) {
                        icl_register_string( 'Video', 'Description', $slide_desc );
                    }
                    if ( $slide_title ) {
                        icl_register_string( 'Video', 'Title', $slide_title );
                    }
                }

                if ( function_exists( 'icl_t' ) ) {
                    if ( $slide_lnk ) {
                        $slide_lnk = icl_t( 'Video', 'Link', $slide_lnk );
                    }
                    if ( $slide_desc ) {
                        $slide_desc = icl_t( 'Video', 'Description', $slide_desc );
                    }
                    if ( $slide_title ) {
                        $slide_title = icl_t( 'Video', 'Title', $slide_title );
                    }
                }
                if ( $slide_lnk ) {
                	$content_array['title'] = $slide_title ? '<h2><a href="' . $slide_lnk . '">' . $slide_title . '</a></h2>' : '';
                } else {
                	$content_array['title'] = $slide_title ? '<h2>' . $slide_title . '</h2>' : '';
                }
				$content_array['description'] = $slide_desc ? '<p>' . $slide_desc . '</p>' : '';
				$content_array['url'] = $slide_lnk ? $slide_lnk : '';
				$content_array['loop'] = !empty( $ef_data['ef_vid_loop'] );
			}

			return $content_array;
		}

		/**

		If first image in the slider is dark.
		@since Brama 1.0

		* */
		public static function is_dark_first_mage( $ef_data = '', $postID = '' ) {
 			$featured = $ef_data['mb_support'] ? efmb_meta( 'ef_inc_featured_img', 'type=checkbox', $ef_data['post_id'] ) : false;
			$slides = self::slides( $ef_data );

			if ( $ef_data['img_array'] && !$featured && !( get_post_format( $postID ) === 'video' ) ) {
				$img = array_values( $ef_data['img_array'] );
				$img = $img[0];
				return self::is_dark_mage( $img['medium_url'] );
			} elseif ( self::has_category_thumb( $ef_data ) || ( has_post_thumbnail( $postID ) && !is_page_template( 'templates/home-template.php' ) ) ) {
				$img = self::thumbArgs( $ef_data, get_post_thumbnail_id( $postID ) );
				return self::is_dark_mage( $img['medium_thumb'] );
			} elseif ( is_page_template( 'templates/home-template.php' ) && !empty( $slides ) ) {
				return self::is_dark_mage( $slides[0]['thumbs']['medium_thumb'] );
			} else {
				return false;
			}
		}

		/**

		Slider output.
		@since Brama 1.0

		* */
		public static function buildSlider( $ef_data = '', $isParallax = true, $postID = '' ) {
			$parallaxAttr = $isParallax ? ' ef-parallax-block' : '';

			$html_start = '<section id="fireform-slider-wrapper"><div class="fireform-slider-inner ef-positioner' . $parallaxAttr . '"><ul class="slides">';
			$html_end = '</ul></div></section><!-- #fireform-slider-wrapper -->';

			if ( $ef_data['img_array'] ) {
				$featured = efmb_meta( 'ef_inc_featured_img', 'type=checkbox', $ef_data['post_id'] );

				echo $html_start;

				if ( $featured ) {
					$img = self::thumbArgs( $ef_data, get_post_thumbnail_id( $postID ) );
					$dark = self::is_dark_mage( $img['medium_thumb'] ) ? ' ef-dark-slide' : '';

					echo '<li class="ef-slide' . $dark . '">' . self::getThumb( $img['large_thumb'], $img['medium_thumb'], $img['alt'], false ) . '</li><!-- .ef-slide -->';
				}

				foreach ( $ef_data['img_array'] as $img ) {
					$dark = self::is_dark_mage( $img['medium_url'] ) ? ' ef-dark-slide' : '';
					echo '<li class="ef-slide' . $dark . '">' . self::getThumb( $img['large_url'], $img['medium_url'], $img['alt'], false ) . '</li><!-- .ef-slide -->';
				}

				echo $html_end;
			} else {
				$slides = self::slides( $ef_data );

				if ( !empty( $slides['video'] ) ) {
					$desc = !empty( $slides['description'] ) ? $slides['description'] : '';
					$caption = !empty( $slides['title'] ) ? '<figure class="slide_desc">' . $slides['title'] . $desc . '</figure><!-- .html-desc -->' : '';
					self::video( $slides['mp4'], $slides['webm'], $slides[0]['thumbs']['medium_thumb'], false, $slides['loop'], true, true, true );
					echo $caption;
				} elseif ( count( $slides ) > 0 ) {
					echo $html_start;
					foreach ( $slides as $slide ) {
						$caption = '<figure class="html-desc">' . $slide['title'] . $slide['description'] . '</figure><!-- .html-desc -->';
						$dark = self::is_dark_mage( $slide['thumbs']['medium_thumb'] ) ? ' ef-dark-slide' : '';
						echo '<li class="ef-slide' . $dark . '" data-url="' . $slide['url'] . '">';
						echo self::getThumb( $slide['thumbs']['large_thumb'], $slide['thumbs']['medium_thumb'], $slide['thumbs']['alt'], false );
						echo $caption . '</li><!-- .ef-slide -->';
					}
					echo $html_end;
				}
			}
		}

		/**

		Summary output.
		@since Brama 1.0

		* */
		private static function htmlOutput( $ef_data = '' ) {
			if ( post_password_required( $ef_data['post_id'] ) && has_post_thumbnail( $ef_data['post_id'] ) ) {
				echo '<div class="ef-pass ef-parallax-block ts-icon-lock"></div>';
			} elseif ( self::has_category_thumb( $ef_data ) ) {
				$img = self::thumbArgs( $ef_data, false );
				echo self::getThumb( $img['large_thumb'], $img['medium_thumb'], $img['alt'], true );
			} elseif ( $ef_data['mb_support'] && $ef_data['has_map'] ) {
				echo '<div id="ef-gmap" class="ef-parallax-block"></div>';
			} elseif ( null !== $ef_data['post_id'] && has_post_thumbnail( $ef_data['post_id'] ) ) {
				if ( $ef_data['mb_support'] && get_post_format( $ef_data['post_id'] ) === 'video' ) {
					$vid = self::video_meta( $ef_data['post_id'] );
					self::video( $vid[1], $vid[2], $vid[3], $vid[4] );
				} elseif ( $ef_data['mb_support'] && $ef_data['img_array'] ) {
					self::buildSlider( $ef_data, true, $ef_data['post_id'] );
				} else {
					$img = self::thumbArgs( false, get_post_thumbnail_id( $ef_data['post_id'] ) );
					echo self::getThumb( $img['large_thumb'], $img['medium_thumb'], $img['alt'], true );
				}
			}
		}
	}
}