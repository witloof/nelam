<?php
class efCommentsWalker extends Walker_Comment {

	protected $comment_num;

    /* CONSTRUCTOR */
    function __construct() {
    	$this->comment_num = 1; ?>
        <ul id="comment-list">
    <?php }

    /* START_LVL */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $GLOBALS['comment_depth'] = $depth + 1; ?>
        <ul class="children">
    <?php }

    /* END_LVL */
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $GLOBALS['comment_depth'] = $depth + 1; ?>
        </ul><!-- .children -->
	<?php }
		    /* START_EL */
		    function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		        $depth++;
		        $GLOBALS['comment_depth'] = $depth;
		        $GLOBALS['comment'] = $comment;
		        $parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' );

		        if ( $comment->user_id == '0' ) {
					if ( !empty ( $comment->comment_author_url ) ) {
						$auth_url = $comment->comment_author_url;
					} else {
						$auth_url = '#';
					}
				} else {
					$auth_url = get_author_posts_url( $comment->user_id );
				}

				switch ( $comment->comment_type ) :
				case '' : ?>

		        <li <?php comment_class( $parent_class ); ?> id="comment-<?php comment_ID() ?>">
		        	<div id="comment-body-<?php comment_ID() ?>" class="comment-body">
						<?php if ( $args['avatar_size'] != 0 ) { ?>
							<a href="<?php echo esc_url( $auth_url ); ?>" class="vcard author ef-avatar pull-left">
			                    <?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
			                </a><!-- .comment-author -->
						<?php } ?>

						<div class="comment post-comm">
							<div class="ef-post-author">
								<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
									<?php printf( '<strong>%1$s</strong>', get_comment_author( $comment->comment_ID ) ); ?>

									<span><?php printf( __( 'says on %1$s at %2$s', EF_TDM ), get_comment_date(),  get_comment_time() ); ?></span>
								</a>
							</div>

							<em class="ef-comment-num"><?php echo $this->comment_num++; ?></em>

			                <div id="comment-content-<?php comment_ID(); ?>" class="comment-content">
			                    <?php if ( !$comment->comment_approved ) { ?>
			                    	<p><strong><?php _e( 'Your comment is awaiting moderation.', EF_TDM ); ?></strong></p>
			                    <?php } else { ?>
			                    	<?php comment_text(); ?>
			                    <?php } ?>
			                </div><!-- .comment-content -->

			                <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		                </div>

		                <?php edit_comment_link( '', '<div class="ef-comment-edit-link icon-ef-pencil-1"> ', '</div>' ); ?>
		            </div><!-- .comment-body -->

		    	<?php break;
				case 'pingback'  :
				case 'trackback' :
					echo '<li class="post pingback"><p>'.__( 'Pingback:', EF_TDM ).' '.get_comment_author_link().' '.get_edit_comment_link( __( 'Edit Comment', EF_TDM ), ' ' ).'</p>';
				break;
				endswitch;
		    }

		    function end_el( &$output, $comment, $depth = 0, $args = array() ) { ?>
		        </li><!-- #comment-' . get_comment_ID() . ' -->
		    	<?php
		    }

		    function __destruct() { ?>
	    </ul><!-- #comment-list -->
    <?php }
}