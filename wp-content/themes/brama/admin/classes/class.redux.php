<?php

/**
  ReduxFramework Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('ef_redux_config_class')) {

    class ef_redux_config_class {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

            add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'ef_custom_styles_compiler' ), 10, 3);

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          Custom CSS compiler.

         * */
        function ef_custom_styles_compiler( $options, $css, $changed_values ) {

            $css_dir = get_template_directory() . '/assets/css/';
            $uploads = wp_upload_dir();
            $uploads_dir = trailingslashit( $uploads['basedir'] );

            /* Capture CSS output */
            ob_start();
            require( $css_dir . 'options.php' );
            $css = ob_get_clean();
            /* Capture CSS output */

            global $wp_filesystem;
            if ( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
                WP_Filesystem();
            }

            if ( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $uploads_dir . 'wp_brama_style.css',
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
            }
        }

        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            $template_url = get_template_directory_uri();

            // General settings

            $this->sections[] = array(
                'title'     => __('General Settings', EF_TDM),
                'desc'      => '',
                'icon'      => 'el-icon-cog',
                'fields'    => array(

                    array(
                        'id'        => 'ef_custom_logo',
                        'type'      => 'media',
                        'url'       => false,
                        'title'     => __('Logo', EF_TDM),
                        'mode'      => 'image',
                        'subtitle'  => __('Upload any png/jpg/gif logo. Note that default "Vector logo" should be removed if you want to apply a raster logo instead.', EF_TDM),
                        'default'   => array('url' => $template_url . '/assets/img/logo.png'),
                    ),
                    array(
                        'id'        => 'ef_custom_logo_svg',
                        'type'      => 'media',
                        'url'       => false,
                        'title'     => __('Vector logo', EF_TDM),
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'subtitle'  => __('Upload svg image (resolution-independed scallable vector graphics) that will be applied instead of your raster logo, uploaded above. Works in <a title="Check SVG support" target="_blank" href="http://caniuse.com/#feat=svg">browsers that support SVG<a>.', EF_TDM),
                        'default'   => array('url' => $template_url . '/assets/img/logo.svg'),
                    ),
                    array(
                        'id'        => 'ef_custom_logo_light',
                        'type'      => 'media',
                        'url'       => false,
                        'title'     => __('Light logo', EF_TDM),
                        'mode'      => 'image',
                        'subtitle'  => __('Upload any png/jpg/gif logo. Note that default "Light vector logo" should be removed if you want to apply a raster logo instead.', EF_TDM),
                        'default'   => array('url' => $template_url . '/assets/img/logo-white.png'),
                    ),
                    array(
                        'id'        => 'ef_custom_logo_svg_light',
                        'type'      => 'media',
                        'url'       => false,
                        'title'     => __('Light vector logo', EF_TDM),
                        'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'subtitle'  => __('Upload svg image (resolution-independed scallable vector graphics) that will be applied instead of your raster logo, uploaded above. Works in <a title="Check SVG support" target="_blank" href="http://caniuse.com/#feat=svg">browsers that support SVG<a>.', EF_TDM),
                        'default'   => array('url' => $template_url . '/assets/img/logo-white.svg'),
                    ),
                    array(
                        'id'       => 'ef_base_offset',
                        'type'     => 'dimensions',
                        'compiler' => true,
                        'units'    => array('px'),
                        'title'    => __('Base template offset', EF_TDM),
                        'subtitle' => __('Allow you to specify left and right margin for the page layout. This option is useful if your logo is larger/smaller than default logo.', EF_TDM),
                        'height'   => false,
                        'default'  => array(
                            'width'   => '140'
                        ),
                    ),
                    array(
                        'id'        => 'ef_custom_favicon',
                        'type'      => 'media',
                        'preview'   => true,
                        'title'     => __('Favicon', EF_TDM),
                        'mode'      => 'image',
                        'subtitle'  => __('Upload a 16px x 16px png/gif image (or ico file, which contains both 16x16px and 32x32px icons) that represents your website\'s favicon.', EF_TDM),
                        'default'   => array('url' => $template_url . '/assets/img/favicon.ico'),
                    ),
                    array(
                        'id'        => 'ef_tagline',
                        'type'      => 'switch',
                        'title'     => __('Tagline', EF_TDM),
                        'subtitle'      => __('Show tagline below the site name in the menu section (can be changed at "Settings > General" screen).', EF_TDM),
                        'default'   => true,
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Social profiles', EF_TDM),
                'desc'      => __('Enter URLs to your favorite profiles. Leave field blank to hide link or remove all links to hide the whole icons set. Note that only seven links can be simultaneously displayed.', EF_TDM),
                'icon'      => 'el-icon-adult',
                'subsection' => true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_icon-ef-twitter',
                        'type'      => 'text',
                        'title'     => 'Twitter',
                        'validate'  => 'url',
                        'default'   => 'https://twitter.com/',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-facebook',
                        'type'      => 'text',
                        'title'     => 'Facebook',
                        'validate'  => 'url',
                        'default'   => 'http://facebook.com/',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-linkedin',
                        'type'      => 'text',
                        'title'     => 'Linkedin',
                        'validate'  => 'url',
                        'default'   => 'http://linkedin.com/',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-gplus',
                        'type'      => 'text',
                        'title'     => 'Google +',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-mail',
                        'type'      => 'text',
                        'title'     => __('Send Mail', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_icon-ef-instagramm',
                        'type'      => 'text',
                        'title'     => 'Instagram',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-behance',
                        'type'      => 'text',
                        'title'     => 'Behance',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-stumbleupon',
                        'type'      => 'text',
                        'title'     => 'Stumbleupon',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-skype',
                        'type'      => 'text',
                        'title'     => 'Skype',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-pinterest',
                        'type'      => 'text',
                        'title'     => 'Pinterest',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-youtube',
                        'type'      => 'text',
                        'title'     => 'Youtube',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-vimeo',
                        'type'      => 'text',
                        'title'     => 'Vimeo',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-soundcloud',
                        'type'      => 'text',
                        'title'     => 'Soundcloud',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-tumblr',
                        'type'      => 'text',
                        'title'     => 'Tumblr',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-vkontakte',
                        'type'      => 'text',
                        'title'     => 'Vkontakte',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-lastfm',
                        'type'      => 'text',
                        'title'     => 'Last.fm',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-dribbble',
                        'type'      => 'text',
                        'title'     => 'Dribbble',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-flickr',
                        'type'      => 'text',
                        'title'     => 'Flickr',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-digg',
                        'type'      => 'text',
                        'title'     => 'Digg',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-rss',
                        'type'      => 'text',
                        'title'     => 'RSS',
                        'validate'  => 'url',
                    ),
                    array(
                        'id'        => 'ef_icon-ef-fivehundredpx',
                        'type'      => 'text',
                        'title'     => '500px',
                        'validate'  => 'url',
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Sharing Bar', EF_TDM),
                'desc'      => __('Drag buttons you want to add into "Enabled" area. Note that the bar holds only five icons simultaneously. Sharing bar is located in the sliding menu area and in single posts.', EF_TDM),
                'icon'      => 'el-icon-share',
                'subsection' => true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_sharing',
                        'type'      => 'sorter',
                        'options'   => array(
                            'disabled'  => array(
                                'icon-ef-reddit'     => 'Reddit',
                                'icon-ef-stumbleupon'=> 'StumbleUpon',
                                'icon-ef-vkontakte'  => 'Vkontakte',
                                'icon-ef-digg'       => 'Digg',
                            ),
                            'enabled'   => array(
                                'icon-ef-facebook'   => 'Facebook',
                                'icon-ef-twitter'    => 'Twitter',
                                'icon-ef-linkedin'   => 'Linkedin',
                                'icon-ef-pinterest'  => 'Pinterest',
                                'icon-ef-gplus'      => 'Google +',
                                'icon-ef-mail'       => __('Email', EF_TDM),
                            ),
                        ),
                        'limits' => array(
                            'enabled'   => 5,
                        ),
                    ),
                    array(
                        'id'        => 'ef_twt_username',
                        'type'      => 'text',
                        'title'     => __('Twitter Username', EF_TDM),
                        'subtitle'      => __('Your Twitter username will be used for <a href="https://dev.twitter.com/docs/cards">Twitter Card</a> Meta Tags', EF_TDM),
                        'validate' => 'no_html'
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Additional Settings', EF_TDM),
                'icon'      => 'el-icon-plus-sign',
                'subsection' => true,
                'fields'    => array(
                    array(
                        'id'            => 'ef_footer_text',
                        'type'          => 'textarea',
                        'title'         => __('Footer Text', EF_TDM),
                        'subtitle'      => __('Paste your copyrights here. Allowed tags: &#60;a&#62;, &#60;br&#62;, &#60;em&#62;, &#60;strong&#62;', EF_TDM),
                        'validate'      => 'html_custom',
                        'default'       => '&copy; 2014 Brama - agency theme by <a target="_blank" href="http://themeforest.net/user/fireform">Fireform</a>.',
                        'allowed_html'  => array(
                            'a'     => array(
                                'href'      => array(),
                                'title'     => array(),
                                'target'    => array(),
                            ),
                            'br'    => array(),
                            'em'    => array(),
                            'strong'=> array(),
                        ),
                    ),
                    array(
                        'id'        => 'ef_google_analytics',
                        'type'      => 'textarea',
                        'title'     => __('Tracking Code', EF_TDM),
                        'subtitle'      => __('Paste your Google Analytics (or other) tracking code here. This will be added into the footer template.', EF_TDM),
                    ),
                ),
            );

            // Home Settings

            $this->sections[] = array(
                'title'     => __('Home Settings', EF_TDM),
                'icon'      => 'el-icon-home',
                'desc'      => __('These options let you configure your homepage (or page created using "Home" template)', EF_TDM),
                'fields'    => array(
                    // Slides/video
                    array(
                        'id'        => 'ef_home_slideshow',
                        'type'      => 'button_set',
                        'title'     => __('Content', EF_TDM),
                        'multi'     => false,
                        'options'   => array(
                            '1' => __('Hide', EF_TDM),
                            '2' => __('Blog Posts', EF_TDM),
                            '3' => __('Portfolio Posts', EF_TDM),
                            '4' => __('Custom Set', EF_TDM),
                            '5' => __('Video', EF_TDM),
                        ),
                        'default'   => '2',
                    ),
                    array(
                        'id'        => 'section-media-start',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('ef_home_slideshow', "=", 2),
                    ),
                        array(
                            'id'            => 'ef_blog_slideshow',
                            'type'          => 'slider',
                            'title'         => __('Number of posts', EF_TDM),
                            'subtitle'      => __('Specify a number of recent posts to show in the slideshow.', EF_TDM),
                            'desc'          => __('Min: 1, max: 10, default: 5', EF_TDM),
                            'default'       => 5,
                            'min'           => 1,
                            'step'          => 1,
                            'max'           => 10,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'        => 'ef_include_posts',
                            'type'      => 'select',
                            'title'     => __('Posts', EF_TDM),
                            'subtitle'  => __('Select posts you want to include manually ("Number of posts" option will be ignored).', EF_TDM),
                            'data'      => 'posts',
                            'multi'     => true,
                            'args'      => array(
                                'post_type'     => 'post',
                                'posts_per_page' => -1
                            ),
                            'placeholder' => __('Show all posts', EF_TDM),
                        ),
                    array(
                        'id'        => 'section-media-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('ef_home_slideshow', "=", 2),
                    ),
                    array(
                        'id'        => 'section-media-start1',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('ef_home_slideshow', "=", 3),
                    ),
                        array(
                            'id'            => 'ef_portf_slideshow',
                            'type'          => 'slider',
                            'title'         => __('Number of posts', EF_TDM),
                            'title'         => __('Number of posts', EF_TDM),
                            'subtitle'      => __('Specify a number of recent posts to show in the slideshow.', EF_TDM),
                            'desc'          => __('Min: 1, max: 10, default: 5', EF_TDM),
                            'default'       => 5,
                            'min'           => 1,
                            'step'          => 1,
                            'max'           => 10,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'        => 'ef_include_portf',
                            'type'      => 'select',
                            'title'     => __('Posts', EF_TDM),
                            'subtitle'  => __('Select posts you want to include manually ("Number of posts" option will be ignored).', EF_TDM),
                            'data'      => 'posts',
                            'multi'     => true,
                            'args'      => array(
                                'post_type' => 'portfolios',
                                'posts_per_page' => -1
                            ),
                            'placeholder' => __('Show all posts', EF_TDM),
                        ),
                    array(
                        'id'        => 'section-media-end1',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('ef_home_slideshow', "=", 3),
                    ),
                    array(
                        'id'        => 'section-media-start2',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('ef_home_slideshow', "=", 4),
                    ),
                        array(
                            'id'        => 'ef_home_slides',
                            'type'      => 'slides',
                            'title'     => __('Slides', EF_TDM),
                            'subtitle'  => __('Unlimited slides with drag and drop sortings. "Title" field is required in case you want to show description and set a link.', EF_TDM),
                            'placeholder'   => array(
                                'title'         => __('Slide title', EF_TDM),
                                'description'   => __('Slide description', EF_TDM),
                                'url'           => __('Slide link', EF_TDM),
                            ),
                        ),
                    array(
                        'id'        => 'section-media-end2',
                        'type'      => 'section',
                        'indent'    => false,
                        'required'  => array('ef_home_slideshow', "=", 4),
                    ),
                    array(
                        'id'        => 'section-media-start3',
                        'type'      => 'section',
                        'indent'    => true,
                        'required'  => array('ef_home_slideshow', "=", 5),
                    ),
                        array(
                            'id'        => 'ef_mp4vid_upload',
                            'type'      => 'media',
                            'url'       => true,
                            'mode'      => 'video',
                            'title'     => __('Mp4 Video (required)', EF_TDM),
                            'subtitle'      => __('Upload Mp4 video (*.mp4). Mp4 works in Internet Explorer and Webkit browsers like Safari, Chrome or Opera (since v.12).', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_webmvid_upload',
                            'type'      => 'media',
                            'url'       => true,
                            'mode'      => 'video',
                            'title'     => __('Webm Video (required)', EF_TDM),
                            'subtitle'      => __('Upload webm video (*.webm). This is alternate video source for Mozilla Firefox browser.', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_vid_image_upload',
                            'type'      => 'media',
                            'url'       => true,
                            'mode'      => 'image',
                            'title'     => __('Poster Image (required)', EF_TDM),
                            'subtitle'      => __('Upload a jpg/png/gif poster image that will be represented as HTML5 video "poster" attribute.', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_vid_loop',
                            'type'      => 'switch',
                            'title'     => __('Loop', EF_TDM),
                            'subtitle'      => __('Enable/disable "loop" HTML5 video attribute.', EF_TDM),
                            'default'   => true,
                        ),
                        array(
                            'id'        => 'ef_vid_title',
                            'type'      => 'text',
                            'validate'  => 'no_html',
                            'title'     => __('Title', EF_TDM),
                            'subtitle'  => __('Caption title. This is required field in case you want to show description and set a link.', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_vid_desc',
                            'type'      => 'textarea',
                            'validate'  => 'no_html',
                            'title'     => __('Description', EF_TDM),
                            'subtitle'  => __('Caption description.', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_vid_lnk',
                            'type'      => 'text',
                            'validate'  => 'url',
                            'title'     => __('Link', EF_TDM),
                            'subtitle'  => __('Paste a link to any page/post/external website. Leave this field blank to hide link.', EF_TDM),
                        ),
                    array(
                        'id'        => 'section-media-end3',
                        'type'      => 'section',
                        'indent'    => false,
                        'required'  => array('ef_home_slideshow', "=", 5),
                    ),

                    // Slideshow options
                    array(
                        'id'        => 'section-media-start4',
                        'type'      => 'section',
                        'indent'    => true,
                        'required'  => array(
                            array( 'ef_home_slideshow', "!=", 5 ),
                            array( 'ef_home_slideshow', "!=", 1 ),
                        ),
                    ),
                        array(
                            'id'        => 'ef_flex_auto',
                            'type'      => 'switch',
                            'title'     => __('Autoplay', EF_TDM),
                            'subtitle'  => __('Enable/disable autoplay.', EF_TDM),
                            'default'   => false,
                        ),
                        array(
                            'id'            => 'ef_flex_interval',
                            'type'          => 'slider',
                            'title'         => __('Slide Interval', EF_TDM),
                            'subtitle'      => __('Slide interval time (milliseconds).', EF_TDM),
                            'desc'          => __('Min: 300, max: 15000, default: 6000', EF_TDM),
                            'default'       => 6000,
                            'min'           => 3000,
                            'step'          => 500,
                            'max'           => 15000,
                            'display_value' => 'text',
                        ),
                        array(
                            'id'        => 'ef_flex_transition',
                            'type'      => 'radio',
                            'title'     => __('Transition', EF_TDM),
                            'subtitle'  => __('Slideshow transition (animation).', EF_TDM),
                            'options'   => array(
                                'fade'      => 'Fade',
                                'slide'     => 'Slide',
                            ),
                            'default'   => 'fade',
                        ),
                        array(
                            'id'            => 'ef_flex_speed',
                            'type'          => 'slider',
                            'title'         => __('Transition speed', EF_TDM),
                            'subtitle'      => __('Slideshow transition duration (milliseconds).', EF_TDM),
                            'desc'          => __('Min: 500, max: 2000, default: 800', EF_TDM),
                            'default'       => 800,
                            'min'           => 500,
                            'step'          => 100,
                            'max'           => 2000,
                            'display_value' => 'text',
                        ),
                        array(
                            'id'        => 'ef_flex_cover',
                            'type'      => 'switch',
                            'title'     => __('Cover screen', EF_TDM),
                            'subtitle'  => __('Images cover the whole screen by default. Turn it "off" to show full image (fills screen height).', EF_TDM),
                            'default'   => true,
                        ),
                    array(
                        'id'        => 'section-media-end4',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array(
                            array( 'ef_home_slideshow', "!=", 5 ),
                            array( 'ef_home_slideshow', "!=", 1 ),
                        ),
                    ),
                ),
            );

            // Widgets

            $this->sections[] = array(
                'title'     => __('Widgets', EF_TDM),
                'desc'      => __('These options are common for all pages except archives, blog pages/posts and portfolio pages/posts. Create custom widget area (Widget areas > Create New) if you want to display different widgets on multiple pages.', EF_TDM),
                'icon'      => 'el-icon-indent-left',
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Archive Widgets', EF_TDM),
                'desc'      => __('These options are common for all archives. Create custom widget area (Widget areas > Create New) if you want to display different widgets on multiple archive pages.', EF_TDM),
                'subsection'=> true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout_archive',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets_archive',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_archive',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets_archive',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer_archive',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets_archive',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Blog Widgets', EF_TDM),
                'desc'      => __('These options are common for all pages created using "Blog" template. Create custom widget area (Widget areas > Create New) if you want to display different widgets on multiple blog pages.', EF_TDM),
                'subsection'=> true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout_blog',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets_blog',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_blog',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets_blog',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer_blog',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets_blog',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Blog Post Widgets', EF_TDM),
                'desc'      => __('These options are common for blog posts. Create custom widget area (Widget areas > Create New) if you want to assign custom widget areas to your blog posts.', EF_TDM),
                'subsection'=> true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout_blog_single',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets_blog_single',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_blog_single',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets_blog_single',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer_blog_single',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets_blog_single',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Portfolio Widgets', EF_TDM),
                'desc'      => __('These options are common for all pages created using "Portfolio" template. Create custom widget area (Widget areas > Create New) if you want to display different widgets on multiple portfolio pages.', EF_TDM),
                'subsection'=> true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout_portf',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets_portf',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_portf',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets_portf',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer_portf',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets_portf',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Portfolio Post Widgets', EF_TDM),
                'desc'      => __('These options are common for portfolio posts. Create custom widget area (Widget areas > Create New) if you want to assign custom widget areas to your portfolio posts.', EF_TDM),
                'subsection'=> true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout_portf_single',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets_portf_single',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_portf_single',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets_portf_single',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer_portf_single',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets_portf_single',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Portfolio Archive Widgets', EF_TDM),
                'desc'      => __('These options are common for portfolio archives. Create custom widget area (Widget areas > Create New) if you want to display different widgets in portfolio archives.', EF_TDM),
                'subsection'=> true,
                'fields'    => array(
                    array(
                        'id'        => 'ef_menu_layout_archive_portf',
                        'type'      => 'image_select',
                        'title'     => __('Menu', EF_TDM),
                        'subtitle'  => __('Select menu layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_menu_widgets_archive_portf',
                        'type'      => 'select',
                        'title'     => __('Menu widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default menu widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_archive_portf',
                        'type'      => 'image_select',
                        'title'     => __('Sidebar', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb.gif'
                            ),
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'ef_sidebar_widgets_archive_portf',
                        'type'      => 'select',
                        'title'     => __('Sidebar widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default sidebar widget area', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_layout_footer_archive_portf',
                        'type'      => 'image_select',
                        'title'     => __('Footer', EF_TDM),
                        'subtitle'  => __('Select page layout.', EF_TDM),
                        'options'   => array(
                            '1' => array(
                                'alt'   => 'Regular',
                                'title' => 'Regular',
                                'img'   => $template_url . '/assets/img/admin/nosb.gif'
                            ),
                            '2' => array(
                                'alt'   => '+ widgets',
                                'title' => '+ widgets',
                                'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                            ),
                        ),
                        'default'   => '1'
                    ),
                    array(
                        'id'        => 'ef_footer_widgets_archive_portf',
                        'type'      => 'select',
                        'title'     => __('Footer widget area', EF_TDM),
                        'subtitle'  => __('Select custom widget area.', EF_TDM),
                        'data'      => 'posts',
                        'args'      => array(
                            'post_type'     => 'custom-sidebars'
                        ),
                        'placeholder' => __('Default footer widget area', EF_TDM),
                    ),
                ),
            );

            if (current_theme_supports('woocommerce')) {
                $this->sections[] = array(
                    'title'     => __('Single Product Widgets', EF_TDM),
                    'desc'      => __('These options are common for all single product pages. Create custom widget area (Widget areas > Create New) if you want to assign custom widget areas to your single product pages.', EF_TDM),
                    'subsection'=> true,
                    'fields'    => array(
                        array(
                            'id'        => 'ef_menu_layout_product',
                            'type'      => 'image_select',
                            'title'     => __('Menu', EF_TDM),
                            'subtitle'  => __('Select menu layout.', EF_TDM),
                            'options'   => array(
                                '1' => array(
                                    'alt'   => 'Regular',
                                    'title' => 'Regular',
                                    'img'   => $template_url . '/assets/img/admin/nosb.gif'
                                ),
                                '2' => array(
                                    'alt'   => '+ widgets',
                                    'title' => '+ widgets',
                                    'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                                ),
                            ),
                            'default'   => '2'
                        ),
                        array(
                            'id'        => 'ef_menu_widgets_product',
                            'type'      => 'select',
                            'title'     => __('Menu widget area', EF_TDM),
                            'subtitle'  => __('Select custom widget area.', EF_TDM),
                            'data'      => 'posts',
                            'args'      => array(
                                'post_type'     => 'custom-sidebars'
                            ),
                            'placeholder' => __('Default menu widget area', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_layout_product',
                            'type'      => 'image_select',
                            'title'     => __('Sidebar', EF_TDM),
                            'subtitle'  => __('Select page layout.', EF_TDM),
                            'options'   => array(
                                '1' => array(
                                    'alt'   => 'Regular',
                                    'title' => 'Regular',
                                    'img'   => $template_url . '/assets/img/admin/nosb.gif'
                                ),
                                '2' => array(
                                    'alt'   => '+ widgets',
                                    'title' => '+ widgets',
                                    'img'   => $template_url . '/assets/img/admin/sb.gif'
                                ),
                            ),
                            'default'   => '2'
                        ),
                        array(
                            'id'        => 'ef_sidebar_widgets_product',
                            'type'      => 'select',
                            'title'     => __('Sidebar widget area', EF_TDM),
                            'subtitle'  => __('Select custom widget area.', EF_TDM),
                            'data'      => 'posts',
                            'args'      => array(
                                'post_type'     => 'custom-sidebars'
                            ),
                            'placeholder' => __('Default sidebar widget area', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_layout_footer_product',
                            'type'      => 'image_select',
                            'title'     => __('Footer', EF_TDM),
                            'subtitle'  => __('Select page layout.', EF_TDM),
                            'options'   => array(
                                '1' => array(
                                    'alt'   => 'Regular',
                                    'title' => 'Regular',
                                    'img'   => $template_url . '/assets/img/admin/nosb.gif'
                                ),
                                '2' => array(
                                    'alt'   => '+ widgets',
                                    'title' => '+ widgets',
                                    'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                                ),
                            ),
                            'default'   => '1'
                        ),
                        array(
                            'id'        => 'ef_footer_widgets_product',
                            'type'      => 'select',
                            'title'     => __('Footer widget area', EF_TDM),
                            'subtitle'  => __('Select custom widget area.', EF_TDM),
                            'data'      => 'posts',
                            'args'      => array(
                                'post_type'     => 'custom-sidebars'
                            ),
                            'placeholder' => __('Default footer widget area', EF_TDM ),
                        ),
                    ),
                );

                $this->sections[] = array(
                    'title'     => __('Product Archive Widgets', EF_TDM),
                    'desc'      => __('These options are common for all product archives including shop page. Create custom widget area (Widget areas > Create New) if you want to display different widgets in product archives.', EF_TDM),
                    'subsection'=> true,
                    'fields'    => array(
                        array(
                            'id'        => 'ef_menu_layout_archive_product',
                            'type'      => 'image_select',
                            'customizer' => false,
                            'title'     => __('Menu', EF_TDM),
                            'subtitle'  => __('Select menu layout.', EF_TDM),
                            'options'   => array(
                                '1' => array(
                                    'alt'   => 'Regular',
                                    'title' => 'Regular',
                                    'img'   => $template_url . '/assets/img/admin/nosb.gif'
                                ),
                                '2' => array(
                                    'alt'   => '+ widgets',
                                    'title' => '+ widgets',
                                    'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                                ),
                            ),
                            'default'   => '2'
                        ),
                        array(
                            'id'        => 'ef_menu_widgets_archive_product',
                            'type'      => 'select',
                            'title'     => __('Menu widget area', EF_TDM),
                            'subtitle'  => __('Select custom widget area.', EF_TDM),
                            'data'      => 'posts',
                            'args'      => array(
                                'post_type'     => 'custom-sidebars'
                            ),
                            'placeholder' => __('Default menu widget area', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_layout_archive_product',
                            'type'      => 'image_select',
                            'title'     => __('Sidebar', EF_TDM),
                            'subtitle'  => __('Select page layout.', EF_TDM),
                            'options'   => array(
                                '1' => array(
                                    'alt'   => 'Regular',
                                    'title' => 'Regular',
                                    'img'   => $template_url . '/assets/img/admin/nosb.gif'
                                ),
                                '2' => array(
                                    'alt'   => '+ widgets',
                                    'title' => '+ widgets',
                                    'img'   => $template_url . '/assets/img/admin/sb.gif'
                                ),
                            ),
                            'default'   => '2'
                        ),
                        array(
                            'id'        => 'ef_sidebar_widgets_archive_product',
                            'type'      => 'select',
                            'title'     => __('Sidebar widget area', EF_TDM),
                            'subtitle'  => __('Select custom widget area.', EF_TDM),
                            'data'      => 'posts',
                            'args'      => array(
                                'post_type'     => 'custom-sidebars'
                            ),
                            'placeholder' => __('Default sidebar widget area', EF_TDM),
                        ),
                        array(
                            'id'        => 'ef_layout_footer_archive_product',
                            'type'      => 'image_select',
                            'title'     => __('Footer', EF_TDM),
                            'subtitle'  => __('Select page layout.', EF_TDM),
                            'options'   => array(
                                '1' => array(
                                    'alt'   => 'Regular',
                                    'title' => 'Regular',
                                    'img'   => $template_url . '/assets/img/admin/nosb.gif'
                                ),
                                '2' => array(
                                    'alt'   => '+ widgets',
                                    'title' => '+ widgets',
                                    'img'   => $template_url . '/assets/img/admin/sb-menu.gif'
                                ),
                            ),
                            'default'   => '1'
                        ),
                        array(
                            'id'        => 'ef_footer_widgets_archive_product',
                            'type'      => 'select',
                            'title'     => __('Footer widget area', EF_TDM),
                            'subtitle'  => __('Select custom widget area.', EF_TDM),
                            'data'      => 'posts',
                            'args'      => array(
                                'post_type'     => 'custom-sidebars'
                            ),
                            'placeholder' => __('Default footer widget area', EF_TDM ),
                        ),
                    ),
                );
            }

            // Portfolio post

            $this->sections[] = array(
                'title'     => __('Portfolio Post', EF_TDM),
                'icon'      => 'el-icon-picture',
                'fields'    => array(
                    array(
                        'id'        => 'ef_portf_comments',
                        'type'      => 'switch',
                        'title'     => __('Comments', EF_TDM),
                        'subtitle'      => __('Enable/disable comments in portfolio posts.', EF_TDM),
                        'default'   => false,
                    ),
                ),
            );

            // Navigation

            $this->sections[] = array(
                'title'     => __('Navigation', EF_TDM),
                'icon'      => 'el-icon-flag',
                'fields'    => array(
                    array(
                        'id'        => 'ef_post_nav',
                        'type'      => 'checkbox',
                        'title'     => __('Single Post Navigation', EF_TDM),
                        'subtitle'  => __('Adds navigation links on a single post to the previous or next post.', EF_TDM),
                        'options'   => array(
                            '1' => 'Portfolio Post',
                            '2' => 'Blog Post',
                            '3' => 'Extra Post',
                        ),
                        'default'   => array(
                            '1' => '1',
                            '2' => '1',
                            '3' => '0',
                        )
                    ),
                    array(
                        'id'        => 'ef_nav_notice',
                        'type'      => 'info',
                        'notice'    => true,
                        'style'     => 'info',
                        'title'     => __('Parent pages', EF_TDM),
                        'desc'      => __('You can assign parent pages to your blog/portfolio/extra posts and archives. This will keep parent menu item highlighted in the navigation menu also adds \'X\' button to posts and archives (top right corner of the page) which follows to the parent page. Portfolio posts parent should be specified for page created using "Home" template too if you would like to use "Minimal" page style. Note that highlighting works with <a target="_blank" href="http://codex.wordpress.org/Appearance_Menus_Screen">WordPress Menus</a> only therefore you need to create your custom menu and assign it to "Header Navigation" location.', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_portf_parent',
                        'type'      => 'select',
                        'title'     => __('Portfolio Post/archives Parent', EF_TDM),
                        'subtitle'  => __('Select a parent page for your posts.', EF_TDM),
                        'data'      => 'page',
                        'multi'     => false,
                        'args'      => array(
                            'meta_key'      => '_wp_page_template',
                            'meta_value'    => 'templates/portfolio-template.php',
                        ),
                        'placeholder' => __('No parent page', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_blog_parent',
                        'type'      => 'select',
                        'title'     => __('Blog Post/archives Parent', EF_TDM),
                        'subtitle'  => __('Select a parent page for your posts.', EF_TDM),
                        'data'      => 'page',
                        'multi'     => false,
                        'args'      => array(
                            'meta_key'      => '_wp_page_template',
                            'meta_value'    => 'templates/blog-template.php',
                        ),
                        'placeholder' => __('No parent page', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_extra_parent',
                        'type'      => 'select',
                        'title'     => __('Extra Post Parent', EF_TDM),
                        'subtitle'  => __('Select a parent page for your posts.', EF_TDM),
                        'data'      => 'page',
                        'multi'     => false,
                        'args'      => array(
                            'meta_key'      => '_wp_page_template',
                            'meta_value'    => 'templates/regular-template.php',
                        ),
                        'placeholder' => __('No parent page', EF_TDM),
                    ),
                    array(
                        'id'        => 'ef_team_parent',
                        'type'      => 'select',
                        'title'     => __('Team Post Parent', EF_TDM),
                        'subtitle'  => __('Select a parent page for your posts.', EF_TDM),
                        'data'      => 'page',
                        'multi'     => false,
                        'args'      => array(
                            'meta_key'      => '_wp_page_template',
                            'meta_value'    => 'templates/regular-template.php',
                        ),
                        'placeholder' => __('No parent page', EF_TDM),
                    ),
                ),
            );

            if (current_theme_supports('woocommerce') && get_option('woocommerce_shop_page_id')) {
                array_push( $this->sections[count($this->sections)-1]['fields'], array(
                        'id'        => 'ef_shop_parent',
                        'type'      => 'select',
                        'title'     => __('Shop Product/archives Parent', EF_TDM),
                        'subtitle'  => __('Select a parent page for your single product pages. Depends on what page is assigned in WooCommerce settings.', EF_TDM),
                        'options'   => array(
                            get_option('woocommerce_shop_page_id') => get_the_title(get_option('woocommerce_shop_page_id')),
                        ),
                        'placeholder' => __('No parent page', EF_TDM),
                    )
                );
            }

            // Styling options

            $this->sections[] = array(
                'title'     => __('Styling Options', EF_TDM),
                'icon'      => 'el-icon-tint',
                'fields'    => array(
                    array(
                        'id'        => 'ef_theme_color',
                        'type'      => 'color',
                        'compiler'  => true,
                        'title'     => __('Primary color', EF_TDM),
                        'subtitle'      => __('Pick a primary color for the theme.', EF_TDM),
                        'default'   => '#dbb672',
                        'validate'  => 'color',
                        'transparent' => false
                    ),


                    array(
                        'id'          => 'ef_heading_font',
                        'type'        => 'typography',
                        'title'       => __( 'Heading font', EF_TDM ),
                        'google'      => true,
                        'font-backup' => false,
                        'text-align'    => false,
                        'font-size'     => false,
                        'font-style'    => true,
                        'line-height'   => false,
                        'letter-spacing'=> true,
                        'color'         => false,
                        'all_styles'  => true,
                        'compiler'    => true,
                        'units'       => 'em',
                        'subtitle'    => __( 'Specify heading font properties.', EF_TDM ),
                        'default'     => array(
                            'font-weight'  => '700',
                            'font-style'  => 'normal',
                            'font-family' => 'Montserrat',
                            'google'      => true,
                            'letter-spacing' => '-0.06'
                        ),
                    ),
                    array(
                        'id'          => 'ef_content_font',
                        'type'        => 'typography',
                        'title'       => __( 'Content font', EF_TDM ),
                        'google'      => true,
                        'font-backup' => false,
                        'text-align'    => false,
                        'font-size'     => false,
                        'font-style'    => false,
                        'font-weight'   => false,
                        'line-height'   => false,
                        'letter-spacing'=> false,
                        'color'         => false,
                        'all_styles'  => true,
                        'compiler'    => true,
                        'subtitle'    => __( 'Specify content font.', EF_TDM ),
                        'default'     => array(
                            'font-family' => 'Muli',
                            'google'      => true,
                        ),
                    ),
                    array(
                        'id'          => 'ef_deco_font',
                        'type'        => 'typography',
                        'title'       => __( 'Decorative font', EF_TDM ),
                        'google'      => true,
                        'font-backup' => false,
                        'text-align'    => false,
                        'font-size'     => false,
                        'font-style'    => false,
                        'font-weight'   => false,
                        'line-height'   => false,
                        'letter-spacing'=> false,
                        'color'         => false,
                        'all_styles'  => true,
                        'compiler'    => true,
                        'subtitle'    => __( 'Specify decorative font. This font family will be applied to "italic" style of the page description and "blockquote" blocks.', EF_TDM ),
                        'default'     => array(
                            'font-family' => 'Libre Baskerville',
                            'google'      => true,
                        ),
                    ),
                    array(
                        'id'       => 'ef_custom_css',
                        'type'     => 'ace_editor',
                        'compiler'  => true,
                        'title'    => __( 'Custom CSS', EF_TDM ),
                        'subtitle' => __( 'Quickly add some CSS to your theme by adding it to this block. I recommend to use this feature for any CSS customizations.', EF_TDM ),
                        'mode'     => 'css',
                        'theme'    => 'github',
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Performance', EF_TDM),
                'desc'      => __('These options allow you to optimize you website providing some predefined tweaks.', EF_TDM),
                'icon'      => 'el-icon-dashboard',
                'fields'    => array(
                    array(
                        'id'       => 'ef_parallax',
                        'type'     => 'switch',
                        'title'    => __( 'Parallax', EF_TDM ),
                        'subtitle' => __( 'Enable or disable parallax effect.', EF_TDM ),
                        'default'  => true,
                    ),
                    array(
                        'id'       => 'ef_minifier',
                        'type'     => 'switch',
                        'title'    => __( 'Optimize CSS/JS', EF_TDM ),
                        'subtitle' => __( 'This option let you enable combined and minified versions of all CSS (theme.min.css) and JS (theme.min.js) files. Note that if you want to modify theme stylesheets directly you should turn this option off to see your changes.', EF_TDM ),
                        'default'  => false,
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Import / Export', EF_TDM),
                'desc'      => __('Import and Export your settings from file, text or URL.', EF_TDM),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => __('Save and restore your options configuration.', EF_TDM),
                        'full_width'    => false,
                    ),
                ),
            );

            $this->sections[] = array(
                'type' => 'divide',
            );
        }


        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'efto_data',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'submenu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Theme Options', EF_TDM),
                'page_title'        => __('Theme Options', EF_TDM),

                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => '', // Must be defined to add google fonts to the typography module

                'async_typography'  => true,                    // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => false,                    // Enable basic customizer support
                'open_expanded'     => false,                    // Allow you to start the panel in an expanded way initially.
                'disable_save_warn' => false,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.

                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );
        }
    }

    global $reduxConfig;
    $reduxConfig = new ef_redux_config_class();
}