<?php

defined( 'ABSPATH' ) || exit;

if ( !class_exists( 'EfGetOptions' ) ) {
    class EfGetOptions {

        private static $data_ = array();

        public static function TplConditions() {
            return self::$data_;
        }

        // Menu widgets

        private static function MenuLayout( $layout = null, $cpt, $to ) {
            // Menu layout
            if ( empty( $layout ) ) {
                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( is_product() ) {
                        $layout = !empty( self::$data_['ef_menu_layout_product'] ) ? self::$data_['ef_menu_layout_product'] : '2';
                    } else {
                        $layout = !empty( self::$data_['ef_menu_layout_archive_product'] ) ? self::$data_['ef_menu_layout_product'] : '2';
                    }
                } elseif ( $cpt && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    $layout = $to ? self::$data_['ef_menu_layout_archive_portf'] : '2';
                } elseif ( is_archive() ) {
                    $layout = $to ? self::$data_['ef_menu_layout_archive'] : '2';
                } elseif ( is_single() ) {
                    if ( $to && ( $cpt && is_singular( EF_CPT ) ) ) {
                        $layout = !empty( self::$data_['ef_menu_layout_portf_single'] ) ? self::$data_['ef_menu_layout_portf_single'] : '2';
                    } elseif ( is_singular( 'post' ) ) {
                        $layout = !empty( self::$data_['ef_menu_layout_blog_single'] ) ? self::$data_['ef_menu_layout_blog_single'] : '2';
                    } else {
                        $layout = '2';
                    }
                } elseif ( self::$data_['is_blog'] ) {
                    $layout = !empty( self::$data_['ef_menu_layout_blog'] ) ? self::$data_['ef_menu_layout_blog'] : '2';
                } elseif ( self::$data_['is_portfolio'] ) {
                    $layout = !empty( self::$data_['ef_menu_layout_portf'] ) ? self::$data_['ef_menu_layout_portf'] : '2';
                } elseif ( !empty( self::$data_['ef_menu_layout'] ) ) {
                    $layout = self::$data_['ef_menu_layout'];
                } else {
                    $layout = '2';
                }
            }

            return $layout;
        }

        private static function MenuWidgets( $widgets = null, $cpt, $to ) {
            // Menu widgets
            if ( empty( $widgets ) ) {
                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( is_product() ) {
                        $widgets = !empty( self::$data_['ef_menu_widgets_product'] ) ? self::$data_['ef_menu_widgets_product'] : 'ef-menu-widget-area';
                    } else {
                        $widgets = !empty( self::$data_['ef_menu_widgets_archive_product'] ) ? self::$data_['ef_menu_widgets_archive_product'] : 'ef-menu-widget-area';
                    }
                } elseif ( is_single() ) {
                    if ( ( $cpt && is_singular( EF_CPT ) ) ) {
                        if ( !empty( self::$data_['ef_menu_widgets_portf_single'] ) ) {
                            $widgets = self::$data_['ef_menu_widgets_portf_single'];
                        } else {
                           $widgets = 'ef-menu-widget-area';
                        }
                    } else {
                        if ( !empty( self::$data_['ef_menu_widgets_blog_single'] ) ) {
                           $widgets =  self::$data_['ef_menu_widgets_blog_single'];
                        } elseif ( !empty( self::$data_['ef_menu_widgets'] ) ) {
                           $widgets = self::$data_['ef_menu_widgets'];
                        } else {
                           $widgets = 'ef-menu-widget-area';
                        }
                    }
                } elseif ( $cpt && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    $widgets = !empty( self::$data_['ef_menu_widgets_archive_portf'] ) ? self::$data_['ef_menu_widgets_archive_portf'] : 'ef-menu-widget-area';
                } elseif ( is_archive() ) {
                    $widgets = !empty( self::$data_['ef_menu_widgets_archive'] ) ? self::$data_['ef_menu_widgets_archive'] : 'ef-menu-widget-area';
                } elseif ( self::$data_['is_blog'] ) {
                    $widgets = !empty( self::$data_['ef_menu_widgets_blog'] ) ? self::$data_['ef_menu_widgets_blog'] : 'ef-menu-widget-area';
                } elseif ( self::$data_['is_portfolio'] || is_page_template( 'templates/gallery-template.php' ) ) {
                    $widgets = !empty( self::$data_['ef_menu_widgets_portf'] ) ? self::$data_['ef_menu_widgets_portf'] : 'ef-menu-widget-area';
                } elseif ( !empty( self::$data_['ef_menu_widgets'] ) ) {
                    $widgets = self::$data_['ef_menu_widgets'];
                } else {
                    $widgets = 'ef-menu-widget-area';
                }
            }

            return $widgets;
        }

        // Sidebar widgets

        private static function SidebarLayout( $layout = null, $cpt, $to ) {
            // Sidebar layout
            if ( empty( $layout ) ) {
                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( is_product() ) {
                        $layout = !empty( self::$data_['ef_layout_product'] ) ? self::$data_['ef_layout_product'] : '2';
                    } else {
                        $layout = !empty( self::$data_['ef_layout_archive_product'] ) ? self::$data_['ef_layout_archive_product'] : '2';
                    }
                } elseif ( $cpt && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    $layout = $to ? self::$data_['ef_layout_archive_portf'] : '2';
                } elseif ( is_archive() ) {
                    $layout = $to ? self::$data_['ef_layout_archive'] : '2';
                } elseif ( is_single() ) {
                    if ( $to && ( $cpt && is_singular( EF_CPT ) ) ) {
                        $layout = !empty( self::$data_['ef_layout_portf_single'] ) ? self::$data_['ef_layout_portf_single'] : '1';
                    } elseif ( is_singular( 'post' ) ) {
                        $layout = !empty( self::$data_['ef_layout_blog_single'] ) ? self::$data_['ef_layout_blog_single'] : '2';
                    } else {
                        $layout = '1';
                    }
                } elseif ( self::$data_['is_blog'] ) {
                    $layout = !empty( self::$data_['ef_layout_blog'] ) ? self::$data_['ef_layout_blog'] : '2';
                } elseif ( self::$data_['is_portfolio'] ) {
                    $layout = !empty( self::$data_['ef_layout_portf'] ) ? self::$data_['ef_layout_portf'] : '1';
                } elseif ( !empty( self::$data_['ef_layout'] ) ) {
                    $layout = self::$data_['ef_layout'];
                } else {
                    $layout = '1';
                }
            }

            return $layout;
        }

        private static function SidebarWidgets( $widgets = null, $cpt, $to ) {
            // Sidebar widgets
            if ( empty( $widgets ) ) {
                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( is_product() ) {
                        $widgets = !empty( self::$data_['ef_sidebar_widgets_product'] ) ? self::$data_['ef_sidebar_widgets_product'] : 'ef-sidebar-widget-area';
                    } else {
                        $widgets = !empty( self::$data_['ef_sidebar_widgets_archive_product'] ) ? self::$data_['ef_sidebar_widgets_archive_product'] : 'ef-sidebar-widget-area';
                    }
                } elseif ( is_single() ) {
                    if ( ( $cpt && is_singular( EF_CPT ) ) ) {
                        if ( !empty( self::$data_['ef_sidebar_widgets_portf_single'] ) ) {
                            $widgets = self::$data_['ef_sidebar_widgets_portf_single'];
                        } else {
                           $widgets = 'ef-sidebar-widget-area';
                        }
                    } else {
                        if ( !empty( self::$data_['ef_sidebar_widgets_blog_single'] ) ) {
                           $widgets =  self::$data_['ef_sidebar_widgets_blog_single'];
                        } elseif ( !empty( self::$data_['ef_sidebar_widgets'] ) ) {
                           $widgets = self::$data_['ef_sidebar_widgets'];
                        } else {
                           $widgets = 'ef-sidebar-widget-area';
                        }
                    }
                } elseif ( $cpt && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    $widgets = !empty( self::$data_['ef_sidebar_widgets_archive_portf'] ) ? self::$data_['ef_sidebar_widgets_archive_portf'] : 'ef-sidebar-widget-area';
                } elseif ( is_archive() ) {
                    $widgets = !empty( self::$data_['ef_sidebar_widgets_archive'] ) ? self::$data_['ef_sidebar_widgets_archive'] : 'ef-sidebar-widget-area';
                } elseif ( self::$data_['is_blog'] ) {
                    $widgets = !empty( self::$data_['ef_sidebar_widgets_blog'] ) ? self::$data_['ef_sidebar_widgets_blog'] : 'ef-sidebar-widget-area';
                } elseif ( self::$data_['is_portfolio'] || is_page_template( 'templates/gallery-template.php' ) ) {
                    $widgets = !empty( self::$data_['ef_sidebar_widgets_portf'] ) ? self::$data_['ef_sidebar_widgets_portf'] : 'ef-sidebar-widget-area';
                } elseif ( !empty( self::$data_['ef_sidebar_widgets'] ) ) {
                    $widgets = self::$data_['ef_sidebar_widgets'];
                } else {
                    $widgets = 'ef-sidebar-widget-area';
                }
            }

            return $widgets;
        }

        // Footer widgets

        private static function FooterLayout( $layout = null, $cpt, $to ) {
            // Footer layout
            if ( empty( $layout ) ) {
                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( is_product() ) {
                        $layout = !empty( self::$data_['ef_layout_footer_product'] ) ? self::$data_['ef_layout_footer_product'] : '1';
                    } else {
                        $layout = !empty( self::$data_['ef_layout_footer_archive_product'] ) ? self::$data_['ef_layout_footer_archive_product'] : '1';
                    }
                } elseif ( $cpt && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    $layout = $to ? self::$data_['ef_layout_footer_archive_portf'] : '1';
                } elseif ( is_archive() ) {
                    $layout = $to ? self::$data_['ef_layout_footer_archive'] : '1';
                } elseif ( is_single() ) {
                    if ( $to && ( $cpt && is_singular( EF_CPT ) ) ) {
                        $layout = !empty( self::$data_['ef_layout_footer_portf_single'] ) ? self::$data_['ef_layout_footer_portf_single'] : '1';
                    } elseif ( is_singular( 'post' ) ) {
                        $layout = !empty( self::$data_['ef_layout_footer_blog_single'] ) ? self::$data_['ef_layout_footer_blog_single'] : '2';
                    } else {
                        $layout = '1';
                    }
                } elseif ( self::$data_['is_blog'] ) {
                    $layout = !empty( self::$data_['ef_layout_footer_blog'] ) ? self::$data_['ef_layout_footer_blog'] : '2';
                } elseif ( self::$data_['is_portfolio'] ) {
                    $layout = !empty( self::$data_['ef_layout_footer_portf'] ) ? self::$data_['ef_layout_footer_portf'] : '1';
                } elseif ( !empty( self::$data_['ef_layout_footer'] ) ) {
                    $layout = self::$data_['ef_layout_footer'];
                } else {
                    $layout = '1';
                }
            }

            return $layout;
        }

        private static function FooterWidgets( $widgets = null, $cpt, $to ) {
            // Footer widgets
            if ( empty( $widgets ) ) {
                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( is_product() ) {
                        $widgets = !empty( self::$data_['ef_footer_widgets_product'] ) ? self::$data_['ef_footer_widgets_product'] : 'ef-footer-widget-area';
                    } else {
                        $widgets = !empty( self::$data_['ef_footer_widgets_archive_product'] ) ? self::$data_['ef_footer_widgets_archive_product'] : 'ef-footer-widget-area';
                    }
                } elseif ( is_single() ) {
                    if ( ( $cpt && is_singular( EF_CPT ) ) ) {
                        if ( !empty( self::$data_['ef_footer_widgets_portf_single'] ) ) {
                            $widgets = self::$data_['ef_footer_widgets_portf_single'];
                        } else {
                           $widgets = 'ef-footer-widget-area';
                        }
                    } else {
                        if ( !empty( self::$data_['ef_footer_widgets_blog_single'] ) ) {
                           $widgets =  self::$data_['ef_footer_widgets_blog_single'];
                        } elseif ( !empty( self::$data_['ef_footer_widgets'] ) ) {
                           $widgets = self::$data_['ef_footer_widgets'];
                        } else {
                           $widgets = 'ef-footer-widget-area';
                        }
                    }
                } elseif ( $cpt && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    $widgets = !empty( self::$data_['ef_footer_widgets_archive_portf'] ) ? self::$data_['ef_footer_widgets_archive_portf'] : 'ef-footer-widget-area';
                } elseif ( is_archive() ) {
                    $widgets = !empty( self::$data_['ef_footer_widgets_archive'] ) ? self::$data_['ef_footer_widgets_archive'] : 'ef-footer-widget-area';
                } elseif ( self::$data_['is_blog'] ) {
                    $widgets = !empty( self::$data_['ef_footer_widgets_blog'] ) ? self::$data_['ef_footer_widgets_blog'] : 'ef-footer-widget-area';
                } elseif ( self::$data_['is_portfolio'] || is_page_template( 'templates/gallery-template.php' ) ) {
                    $widgets = !empty( self::$data_['ef_footer_widgets_portf'] ) ? self::$data_['ef_footer_widgets_portf'] : 'ef-footer-widget-area';
                } elseif ( !empty( self::$data_['ef_footer_widgets'] ) ) {
                    $widgets = self::$data_['ef_footer_widgets'];
                } else {
                    $widgets = 'ef-footer-widget-area';
                }
            }

            return $widgets;
        }

        public static function init() {

            // Retriving Theme Options data
            global $efto_data;

            // Pushing to the common array
            self::$data_ = $efto_data;

            if ( current_theme_supports( 'woocommerce' ) && is_shop() && get_option( 'woocommerce_shop_page_id' ) ) {
                self::$data_['post_id'] = get_option( 'woocommerce_shop_page_id' );
            } else {
                global $post;
                self::$data_['post_id'] = null !== $post ? $post->ID : null;
            }

            // Features support
            self::$data_['mb_support']  = $mb = current_theme_supports( 'ef-meta-boxes' );
            self::$data_['cpt_support'] = $cpt = current_theme_supports( 'ef-custom-post-types' );
            self::$data_['to_support']  = $to = current_theme_supports( 'ef-theme-options' );

            self::$data_['is_template'] = is_page_template();
            self::$data_['is_portfolio'] = is_page_template( 'templates/portfolio-template.php' );
            self::$data_['is_blog'] = is_page_template( 'templates/blog-template.php' );

            $js_vars = array();
            self::$data_['project_details'] = array();

            $js_vars['demo_mode'] = EF_DEMO_CHANGER ? true : false;

            // Get post meta

            if ( $mb ) {
                self::$data_['posts_per'] = efmb_meta( 'ef_range', 'type=slider' );
                $sl_type = efmb_meta( 'ef_home_slider', 'type=select' );
                $layout = efmb_meta( 'ef_menu_layout', 'type=layout', self::$data_['post_id'] );
                $widgets = efmb_meta( 'ef_menu_widgets', 'type=select', self::$data_['post_id'] );

                $layout1 = efmb_meta( 'ef_sidebar_layout', 'type=layout', self::$data_['post_id'] );
                $widgets1 = efmb_meta( 'ef_sidebar_widgets', 'type=select', self::$data_['post_id'] );

                $layout2 = efmb_meta( 'ef_footer_layout', 'type=layout', self::$data_['post_id'] );
                $widgets2 = efmb_meta( 'ef_footer_widgets', 'type=select', self::$data_['post_id'] );

                $img_array = efmb_meta( 'ef_upload_img', 'type=image_advanced', self::$data_['post_id'] );

                self::$data_['show_content'] = efmb_meta( 'ef_extr_content', 'type=radio' );

                $client = efmb_meta( 'ef_client', 'type=text' );
                $services = efmb_meta( 'ef_services', 'type=textarea' );
                $website = efmb_meta( 'ef_website', 'type=text' );

                self::$data_['page_desc'] = efmb_meta( 'ef_text', 'type=wysiwyg', self::$data_['post_id'] );

                if ( !empty( $client ) ) {
                    self::$data_['project_details']['client'] = $client;
                }
                if ( !empty( $services ) ) {
                    self::$data_['project_details']['services'] = $services;
                }
                if ( !empty( $website ) ) {
                    self::$data_['project_details']['website'] = $website;
                }

                if ( $cpt ) {
                    if ( self::$data_['is_portfolio'] ) {
                        self::$data_['portf_style'] = efmb_meta( 'ef_portf_style', 'type=radio' );
                        $terms = efmb_meta( 'ef_cat', 'type=taxonomy&taxonomy='.EF_CPT_TAX_CAT.'' );
                    } elseif ( is_page_template( 'templates/regular-template.php' ) ) {
                        self::$data_['team_extr'] = efmb_meta( 'ef_team_extr', 'type=checkbox' );
                        if ( self::$data_['team_extr'] ) {
                            $terms = efmb_meta( 'ef_cat_extras', 'type=taxonomy&taxonomy='.EF_CPT_EXTR_TAX.'' );
                        } else {
                            $terms = efmb_meta( 'ef_cat_team', 'type=taxonomy&taxonomy='.EF_CPT_TEAM_TAX.'' );
                            self::$data_['show_content'] = efmb_meta( 'ef_team_content', 'type=radio' );
                        }
                    }
                }

                self::$data_['paging_type'] = efmb_meta( 'ef_portf_type', 'type=radio' );

                if ( is_page_template( 'templates/home-template.php' ) ) {
                    self::$data_['home_style'] = efmb_meta( 'ef_home_style', 'type=radio' );
                } elseif ( self::$data_['is_blog'] ) {
                    $terms = efmb_meta( 'ef_cat', 'type=taxonomy&taxonomy=category' );
                }

                self::$data_['blog_style'] = efmb_meta( 'ef_feed_layout', 'type=layout', self::$data_['post_id'] );

                if ( is_page_template( 'templates/contact-template.php' ) ) {

                    $map_lat = efmb_meta( 'ef_map_lat', 'type=text' );
                    $map_long = efmb_meta( 'ef_map_long', 'type=text' );
                    $has_map = !empty( $map_lat ) && !empty( $map_long ) ? true : false;

                    if ( $has_map ) {
                        $markers = array();
                        $markers['lat'] = $map_lat;
                        $markers['lon'] = $map_long;

                        if ( !empty( $markers ) ) {
                            $js_vars['map_zoom'] = efmb_meta( 'ef_map_zoom', 'type=slider' );
                            $js_vars['map_marker'] = $markers;
                            $marker = efmb_meta( 'ef_upload_marker', 'type=image_advanced' );
                            $js_vars['marker_img'] = !empty( $marker ) ? $marker[key($marker)]['thumb'] : get_template_directory_uri() . '/assets/img/map_marker.png';
                        }
                    }
                }
            } else {
                self::$data_['blog_style'] = 'ef-grid-blog';
            }

            // If supports Theme Options

            if ( $to ) {

                // Logo

                if ( !empty( self::$data_['ef_custom_logo_svg']['url'] ) ) {
                    self::$data_['custom_logo'] = self::$data_['ef_custom_logo_svg']['url'];
                } elseif ( !empty( self::$data_['ef_custom_logo']['url'] ) ) {
                    self::$data_['custom_logo'] = self::$data_['ef_custom_logo']['url'];
                }

                if ( !empty( self::$data_['ef_custom_logo_svg_light']['url'] ) ) {
                    self::$data_['custom_logo_light'] = self::$data_['ef_custom_logo_svg_light']['url'];
                } elseif ( !empty( self::$data_['ef_custom_logo_light']['url'] ) ) {
                    self::$data_['custom_logo_light'] = self::$data_['ef_custom_logo_light']['url'];
                }

                if ( isset( self::$data_['custom_logo'] ) ) {
                    self::$data_['custom_logo_light'] = isset( self::$data_['custom_logo_light'] ) ? self::$data_['custom_logo_light'] : self::$data_['custom_logo'];
                }

                // Slideshow
                $js_vars['slider_options'] = array(
                    'auto'              => !empty( self::$data_['ef_flex_auto'] ),
                    'transition'        => !empty( self::$data_['ef_flex_transition'] ) ? self::$data_['ef_flex_transition'] : 'fade',
                    'cover'             => !empty( self::$data_['ef_flex_cover'] ),
                    'transition_speed'  => !empty( self::$data_['ef_flex_speed'] ) ? self::$data_['ef_flex_speed'] : 800,
                    'slide_interval'    => !empty( self::$data_['ef_flex_interval'] ) ? self::$data_['ef_flex_interval'] : 6000,
                );
                if ( $mb ) {
                    $auto = efmb_meta( 'ef_slider_autoplay', 'type=radio' );
                    switch ( $auto ) {
                        case 1:
                            $js_vars['slider_options']['auto'] = true;
                            break;
                        case 2:
                            $js_vars['slider_options']['auto'] = false;
                            break;
                    }
                }

                // Post parents
                self::$data_['parent_link'] = array();

                if ( $to && current_theme_supports( 'woocommerce' ) && is_woocommerce() ) {
                    if ( !is_shop() && get_option( 'woocommerce_shop_page_id' ) && !empty( self::$data_['ef_shop_parent'] ) ) {
                        self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_shop_parent'] );
                        self::$data_['parent_link']['post_id'] = self::$data_['ef_shop_parent'];
                    } else {
                        self::$data_['parent_link'] = false;
                    }
                } elseif ( is_page_template( 'templates/home-template.php' ) ) {
                    if ( !( isset( self::$data_['home_style'] ) && self::$data_['home_style'] == 'ef-home-default' ) ) {
                        if ( $to && !empty( self::$data_['ef_portf_parent'] ) && ( $mb && $sl_type === '3' || ( empty( $sl_type ) && self::$data_['ef_home_slideshow'] === '3' ) ) ) {
                            self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_portf_parent'] );
                            self::$data_['parent_link']['post_id'] = false;
                        } elseif ( $to && !empty( self::$data_['ef_blog_parent'] ) && ( $mb && $sl_type === '2' || ( empty( $sl_type ) && self::$data_['ef_home_slideshow'] === '2' ) ) ) {
                            self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_blog_parent'] );
                            self::$data_['parent_link']['post_id'] = false;
                        }
                    } else {
                        self::$data_['parent_link'] = false;
                    }
                } elseif ( $cpt && !empty( self::$data_['ef_portf_parent'] ) && ( is_singular( EF_CPT ) || is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) {
                    self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_portf_parent'] );
                    self::$data_['parent_link']['post_id'] = self::$data_['ef_portf_parent'];
                } elseif ( !empty( self::$data_['ef_blog_parent'] ) && ( is_archive( 'post' ) || is_singular( 'post' ) ) ) {
                    self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_blog_parent'] );
                    self::$data_['parent_link']['post_id'] = self::$data_['ef_blog_parent'];
                } elseif ( $cpt ) {
                    if ( !empty( self::$data_['ef_extra_parent'] ) && is_singular( EF_CPT_EXTR ) ) {
                        self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_extra_parent'] );
                        self::$data_['parent_link']['post_id'] = self::$data_['ef_extra_parent'];
                    } elseif ( !empty( self::$data_['ef_team_parent'] ) && is_singular( EF_CPT_TEAM ) ) {
                        self::$data_['parent_link']['url'] = get_permalink( self::$data_['ef_team_parent'] );
                        self::$data_['parent_link']['post_id'] = self::$data_['ef_team_parent'];
                    } else {
                        self::$data_['parent_link'] = false;
                    }
                } else {
                    self::$data_['parent_link'] = false;
                }

            } else {
                $js_vars['slider_options']['cover'] = true;
                self::$data_['parent_link'] = false;
            }

            if ( $cpt && !empty( $img_array ) ) {
                $js_vars['slider_options'] = array(
                    'auto'              => efmb_meta( 'ef_slider_autoplay', 'type=checkbox', self::$data_['post_id'] ),
                    'transition'        => efmb_meta( 'ef_slider_fx', 'type=radio', self::$data_['post_id'] ),
                    'cover'             => efmb_meta( 'ef_slider_cover', 'type=checkbox', self::$data_['post_id'] ),
                    'transition_speed'  => efmb_meta( 'ef_transition_speed', 'type=slider', self::$data_['post_id'] ),
                    'slide_interval'    => efmb_meta( 'ef_slider_duration', 'type=slider', self::$data_['post_id'] ),
                );
            }

            self::$data_['term_slugs'] = !empty( $terms ) ? wp_list_pluck( $terms, 'slug' ) : false;
            self::$data_['term_ids'] = !empty( $terms ) ? wp_list_pluck( $terms, 'term_id' ) : array();

            // Ajax

            if ( !empty( self::$data_['paging_type'] ) && self::$data_['paging_type'] !== 'regular' && is_page_template() ) {
                if ( $cpt && self::$data_['is_portfolio'] ) {
                    $terms = self::$data_['term_slugs'];
                    $pt = EF_CPT;
                    $tax_query = $terms ? array(
                        array(
                            'taxonomy'  => EF_CPT_TAX_CAT,
                            'terms'     => $terms,
                            'field'     => 'slug',
                        )
                    ) : array();
                    $args = array(
                        'post_type'         => $pt,
                        'tax_query'         => $tax_query,
                        'post_status'       => 'publish',
                        'posts_per_page'    => -1,
                        'meta_key'          => '_thumbnail_id'
                    );
                } elseif ( self::$data_['is_blog'] ) {
                    $terms = self::$data_['term_ids'];
                    $pt = 'post';
                    $args = array(
                        'post_type'         => $pt,
                        'category__in'      => $terms,
                        'post_status'       => 'publish',
                        'posts_per_page'    => -1
                    );
                }

                $postslist = new WP_Query( $args );
                $postscount = count( $postslist->posts );

                $js_vars['ef_ajax'] = array(
                    'post_type'         => $pt,
                    'show_content'      => isset( self::$data_['show_content'] ) ? self::$data_['show_content'] : '',
                    'postCommentNonce'  => wp_create_nonce( 'ef_ajax-post-comment-nonce' ),
                    'ajaxurl'           => admin_url( 'admin-ajax.php' ),
                    'offset'            => !empty( self::$data_['posts_per'] ) ? self::$data_['posts_per'] : get_option( 'posts_per_page' ),
                    'postscount'        => $postscount,
                    'terms'             => $terms,
                    'no_more_text'      => __( 'No more entries', EF_TDM ),
                    'blog_style'        => self::$data_['blog_style'],
                    'show_gallery'      => efmb_meta( 'ef_show_gallery', 'type=checkbox' )
                );
            }

            self::$data_['img_array']   = !empty( $img_array ) ? $img_array : false;


            if ( $to && !empty( $sl_type ) ) {
                self::$data_['home_slideshow'] = $sl_type;
            } else {
                self::$data_['home_slideshow'] = $to ? self::$data_['ef_home_slideshow'] : 0;
            }

            if ( self::$data_['home_slideshow'] === '5' && is_page_template( 'templates/home-template.php' ) ) {
                $js_vars['video'] = true;
            }

            $js_vars['map_icon'] = get_template_directory_uri() . '/assets/map_marker.png';
            self::$data_['has_map'] = isset( $has_map ) ? $has_map : false;

            $js_vars['woolightbox'] = current_theme_supports( 'woocommerce' ) && get_option( 'woocommerce_enable_lightbox' ) == 'yes';

            if ( self::$data_['to_support'] ) {
                $js_vars['parallax'] = !empty( self::$data_['ef_parallax'] ) ? true : false;
            } else {
                $js_vars['parallax'] = true;
            }

            /////// JS variables array //////

            self::$data_['js_vars'] = $js_vars;

            ////////////////////////////////

            // Social icons

            self::$data_['socialize_team'] = array(
                'icon-ef-twitter'       => 'Twitter',
                'icon-ef-facebook'      => 'Facebook',
                'icon-ef-linkedin'      => 'LinkedIn',
                'icon-ef-gplus'         => 'Google+',
                'icon-ef-mail'          => __( 'Send Mail', EF_TDM ),
                'icon-ef-instagramm'    => 'Instagram',
                'icon-ef-behance'       => 'Behance',
                'icon-ef-stumbleupon'   => 'StumbleUpon',
                'icon-ef-skype'         => 'Skype',
                'icon-ef-pinterest'     => 'Pinterest',
                'icon-ef-youtube'       => 'Youtube',
                'icon-ef-vimeo'         => 'Vimeo',
                'icon-ef-soundcloud'    => 'Soundcloud',
                'icon-ef-tumblr'        => 'Tumblr',
                'icon-ef-vkontakte'     => 'Vkontakte',
                'icon-ef-lastfm'        => 'Last.fm',
                'icon-ef-dribbble'      => 'Dribbble',
                'icon-ef-flickr'        => 'Flickr',
                'icon-ef-digg'          => 'Digg',
                'icon-ef-rss'           => 'RSS',
                'icon-ef-fivehundredpx' => '500px',
            );

            if ( $to ) {
                self::$data_['socialize'] = array();

                foreach ( self::$data_['socialize_team'] as $field_id => $title ) {
                    if ( !empty( self::$data_['ef_'.$field_id] ) ) {
                        self::$data_['socialize'][$title]['url'] = self::$data_['ef_'.$field_id];
                        self::$data_['socialize'][$title]['class'] = $field_id;
                    }
                }
            }

            // widgets
            self::$data_['menu'] = self::$data_['sidebar'] = self::$data_['footer'] = array();

            if ( $mb ) {
                self::$data_['menu']['layout'] = self::MenuLayout( $layout, $cpt, $to );
                self::$data_['sidebar']['layout'] = self::SidebarLayout( $layout1, $cpt, $to );
                self::$data_['footer']['layout'] = self::FooterLayout( $layout2, $cpt, $to );
            } else {
                self::$data_['menu']['layout'] = '2';
                self::$data_['sidebar']['layout'] = self::$data_['footer']['layout'] = '1';
            }

            if ( $mb && $cpt ) {
                self::$data_['menu']['widgets'] = self::MenuWidgets( $widgets, $cpt, $to );
                self::$data_['sidebar']['widgets'] = self::SidebarWidgets( $widgets1, $cpt, $to );
                self::$data_['footer']['widgets'] = self::FooterWidgets( $widgets2, $cpt, $to );
            } else {
                self::$data_['menu']['widgets'] = 'ef-menu-widget-area';
                self::$data_['sidebar']['widgets'] = 'ef-sidebar-widget-area';
                self::$data_['footer']['widgets'] = 'ef-footer-widget-area';
            }
        }
    }
}