<?php

//Textdomain
define( 'EF_TDM', 'Brama' );

// Init
require_once 'admin/init.php';

// Demo importer
require_once ( 'inc/importer/init.php' );

// Ajax
require_once 'inc/ajax-posts.php';