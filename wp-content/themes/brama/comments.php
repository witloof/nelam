<?php
/**
 * The template for displaying Comments.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>
		<?php if ( have_comments() ) { ?>
			<?php if ( post_password_required() ) return; ?>

			<div id="ef-comments-header" class="row">
				<div class="col-md-8 col-md-offset-2">
					<h5 class="ef-comments-title"><?php printf( _n( '<span>1 Comment to</span><br>%2$s', '<span>%1$s Comments to</span><br>%2$s', get_comments_number(), EF_TDM ), number_format_i18n( get_comments_number() ), '<span>"' . get_the_title() . '"</span>' );?></h5>
				</div>
			</div>

			<?php wp_list_comments( array (
				'walker' => new efCommentsWalker,
				'style' => 'ul',
		        'avatar_size' => 160
			) ); ?>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) { ?>
				<p class="ef-post-comments-nav text-center">
					<?php previous_comments_link( __( '&laquo; Older Comments', EF_TDM ) ); ?>

					<?php next_comments_link( __( 'Newer Comments &raquo;', EF_TDM ) ); ?>
				</p><!-- .ef-post-single-nav -->
			<?php } ?>
		<?php } // end have_comments() ?>

		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<?php comment_form( array( 'label_submit' => '', 'title_reply' => __( 'Your thoughts, please', EF_TDM ), 'comment_notes_after' => ' ' ) ); ?>
			</div>
		</div>