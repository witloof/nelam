<?php
/**
 * Template Name: Contact
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

	<?php get_template_part( 'loop', 'page' ); ?>

<?php get_footer(); ?>
