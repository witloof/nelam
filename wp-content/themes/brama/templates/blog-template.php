<?php
/**
 * Template Name: Blog
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

	<?php if ( post_password_required() ) { ?>
		<?php echo get_the_password_form(); ?>
	<?php } else { ?>
		<?php get_template_part( 'loop', 'page' ); ?>

		<?php global $paged; ?>
		<?php $ef_data = EfGetOptions::TplConditions(); ?>
		<?php $wp_query_tmp = $wp_query; ?>

		<?php $args = array(
			'post_type'				=> 'post',
			'category__in'			=> $ef_data['term_ids'],
			'posts_per_page'		=> isset( $ef_data['posts_per'] ) ? $ef_data['posts_per'] : '',
			'post_status'			=> 'publish',
			'paged'					=> $paged,
		); ?>

		<?php $wp_query = NULL; $wp_query = new WP_Query( $args ); ?>

		<?php get_template_part( 'loop', 'blog' ); ?>

		<?php /* Pagination */ ?>
		<?php ef_theme_pagination( $wp_query->max_num_pages, $ef_data ); ?>

		<?php $wp_query = null; $wp_query = $wp_query_tmp; ?>
	<?php } ?>

<?php get_footer(); ?>
