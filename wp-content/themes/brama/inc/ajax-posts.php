<?php

/* Ajax Post */

add_action( 'wp_ajax_ef_ajax_posts', 'ef_ajax_posts' );
add_action( 'wp_ajax_nopriv_ef_ajax_posts', 'ef_ajax_posts' );

function ef_ajax_posts() {
	$nonce = $_POST['postCommentNonce'];
	if ( !wp_verify_nonce( $nonce, 'ef_ajax-post-comment-nonce' ) ) {
		die ( 'Busted!' );
	} elseif ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) ) {
		if ( $_POST['post_type'] === EF_CPT ) {
			$tax_query = $_POST['terms'] !== 'false' ? array(
	            array(
	                'taxonomy'  => EF_CPT_TAX_CAT,
	                'terms'     => $_POST['terms'],
	                'field'     => 'slug',
	            )
	        ) : array();

			$args = array(
				'post_type' 		=> EF_CPT,
				'tax_query' 		=> $tax_query,
				'posts_per_page'	=> $_POST['posts_per'],
				'offset'			=> $_POST['offset'],
				'post_status'       => 'publish',
				'meta_key'			=> '_thumbnail_id',
				'ignore_sticky_posts' => true,
			);
		} else {
			$args = array(
				'post_type' 		=> 'post',
				'category__in' 		=> !empty( $_POST['terms'] ) ? $_POST['terms'] : array(),
				'post_status'       => 'publish',
				'posts_per_page'	=> $_POST['posts_per'],
				'offset'			=> $_POST['offset'],
			);
		}

		$new_query = new WP_Query( $args ); ?>

		<?php if ( $_POST['post_type'] === EF_CPT ) { ?>
			<ul>
				<?php while ( $new_query->have_posts() ) : $new_query->the_post(); ?>

					<?php $pass = post_password_required() ? '<span class="ef-pass ts-icon-lock"></span>' : ''; ?>
					<?php $thumb = efmb_meta( 'ef_img_orientation', 'type=radio' ); ?>
					<?php $double_h = $thumb === '1' ? 'cbp-height2x' : ''; ?>
					<?php $thumb = $thumb === '1' ? 'portfolio_portrate' : 'portfolio_landscape'; ?>

					<li id="post-<?php the_ID(); ?>" <?php post_class( $double_h ); ?>>
						<div class="ef-proj-img">
							<?php echo get_the_post_thumbnail( $new_query->post->ID, $thumb ); ?>

							<h1 class="ef-proj-desc entry-title">
								<?php echo $pass; ?>

								<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
                                	<span class="ef-details-holder">
	                                	<?php the_title(); ?>

	                                	<?php $creds = efmb_meta( 'ef_img_credits', 'type=text', $new_query->post->ID ); ?>

	                                	<?php echo !empty( $creds ) ? '<span class="ef-additional-info">' . wp_kses( $creds, array( 'img' => array( 'src' => array(), 'alt' => array() ), 'br' => array(), 'strong' => array() ) ) . '</span>' : ''; ?>
									</span>
                                </a>
							</h1><!-- .ef-proj-desc -->
						</div><!-- .ef-proj-img  -->
					</li><!-- .ef-post  -->
				<?php endwhile; ?>
			</ul>
		<?php } else { ?>
			<div>
				<?php while ( $new_query->have_posts() ) : $new_query->the_post(); ?>
					<?php $if_has_more_tag = strpos( $new_query->post->post_content, '<!--more-->' ); ?>
					<?php $show_content = $_POST['show_content'] == 'ef-content'; ?>
					<?php $pass = post_password_required( $new_query->post->ID ) ? '<span class="ef-pass icon-ef-lock-1"></span>' : ''; ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'ef-post clearfix' ); ?>>
						<div class="ef-proj-img">
							<?php if ( $_POST['blog_style'] == 'ef-min-blog' ) { ?>
								<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-default"></a>
							<?php } else { ?>
								<?php if ( has_post_thumbnail( $new_query->post->ID ) ) { ?>
									<?php if ( get_post_format( $new_query->post->ID ) == 'video' && $new_query->post->post_type == 'post' ) { ?>
										<?php $vid = EfPrintImages::video_meta( $new_query->post->ID ); ?>

										<?php if ( $vid[4] ) { ?>
											<div class="ef-thumb">
												<div class="ef-responsive-iframe">
													<?php echo wp_oembed_get( $vid[4] ); ?>
												</div>
											</div>
										<?php } else { ?>
											<?php EfPrintImages::video( $vid[1], $vid[2], $vid[3], false, false, false ) ?>
										<?php } ?>
									<?php } else { ?>
										<div class="ef-thumb">
											<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
												<?php echo get_the_post_thumbnail( $new_query->post->ID, $_POST['blog_style'] == 'ef-classic-blog' ? 'blog_classix' : 'medium_thumb' ); ?>

												<?php echo $pass; ?>

												<?php if ( !$show_content && !empty( $_POST['show_gallery'] ) ) { ?>
													<?php ef_call_gallery_imgs( $new_query->post ); ?>
												<?php } ?>
											</a>
										</div><!-- .ef-thumb -->
									<?php } ?>
								<?php } elseif ( !$show_content && !empty( $_POST['show_gallery'] ) ) { ?>
									<?php ef_call_gallery_imgs( $new_query->post, 'a' ); ?>
								<?php } ?>
							<?php } ?>

							<?php if ( $_POST['blog_style'] == 'ef-classic-blog' ) { ?>
								<div class="ef-post-side">
							<?php } ?>

							<?php echo ef_theme_posted_on( $new_query->post, false ); ?>

							<h1 class="ef-proj-desc entry-title">
								<a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h1><!-- .ef-proj-desc -->

							<?php if ( $_POST['blog_style'] !== 'ef-min-blog' ) { ?>
								<div class="entry-content">
									<?php if ( $show_content ) { ?>
										<?php global $more; $more = 0; ?>
										<?php if ( $if_has_more_tag ) { ?>
											<?php the_content( '', FALSE, '' ); ?>
										<?php } else { ?>
											<?php $content = apply_filters( 'the_content', strip_shortcodes( $new_query->post->post_content ) ); ?>
											<?php echo $content; ?>
										<?php } ?>
										<?php $more = 1; ?>
									<?php } else { ?>
										<?php the_excerpt(); ?>
									<?php } ?>
								</div><!-- .entry-content -->

								<?php if ( !post_password_required( $new_query->post->ID ) ) { ?>
									<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-default"><?php _e( 'Keep reading', EF_TDM ); ?></a>
								<?php } ?>
							<?php } ?>

							<?php if ( $_POST['blog_style'] == 'ef-classic-blog' ) { ?>
								</div>
							<?php } ?>
						</div><!-- .ef-proj-img  -->
					</article><!-- .ef-post  -->
				<?php endwhile; ?>
			</div>
		<?php } ?>
	<?php } ?>
  <?php exit;
}
