<?php
/**
 * Version 0.0.2
 */

require_once(  dirname( __FILE__ ) .'/importer/radium-importer.php' ); //load admin theme data importer

class Radium_Theme_Demo_Data_Importer extends Radium_Theme_Importer {

    /**
     * Holds a copy of the object for easy reference.
     *
     * @since 0.0.1
     *
     * @var object
     */
    private static $instance;

    /**
     * Set the key to be used to store theme options
     *
     * @since 0.0.2
     *
     * @var object
     */

    public $theme_option_name = 'efto_data';

	public $theme_options_file_name = 'theme_options.json';

	public $widgets_file_name 		= 'widgets.json';

	public $content_demo_file_name  = 'content.xml';

	/**
	 * Holds a copy of the widget settings
	 *
	 * @since 0.0.2
	 *
	 * @var object
	 */
	public $widget_import_results;

    /**
     * Constructor. Hooks all interactions to initialize the class.
     *
     * @since 0.0.1
     */
    public function __construct() {

		$this->demo_files_path = dirname(__FILE__) . '/demo-files/';

        self::$instance = $this;
		parent::__construct();

    }

	/**
	 * Add menus
	 *
	 * @since 0.0.1
	 */
	public function set_demo_menus(){

		// Menus to Import and assign - you can remove or add as many as you want
		$main_menu = get_term_by('name', 'Menu 1', 'nav_menu');

		set_theme_mod( 'nav_menu_locations', array(
                'primary' => $main_menu->term_id,
            )
        );

	}

	/**
	 * Update WooCommerce settings
	 *
	 * @since 0.0.1
	 */
	public function set_woo_settings(){

		// Set image dimensions
		ef_woo_image_dimensions();

		// Update pages
		update_option( 'woocommerce_shop_page_id', 427 );
		update_option( 'woocommerce_cart_page_id', 428 );
		update_option( 'woocommerce_checkout_page_id', 429 );
		update_option( 'woocommerce_myaccount_page_id', 430 );
	}

	/**
	 * Update Front page displays option
	 *
	 * @since 0.0.1
	 */
	public function set_front_page(){

		// Update frontpage option
		update_option('show_on_front', 'page'); // show on front a static page
		update_option('page_on_front', 16);
	}

}

new Radium_Theme_Demo_Data_Importer;