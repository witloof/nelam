<?php
/**
 * The template for displaying all pages.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

	<?php if ( post_password_required() ) { ?>
		<?php echo get_the_password_form(); ?>
	<?php } else { ?>
		<?php get_template_part( 'loop', 'page' ); ?>
	<?php } ?>

<?php get_footer(); ?>