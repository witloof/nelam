<?php

/**
 * The sidebar template.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php if ( $ef_data['sidebar']['layout'] === '2' ) { ?>

	<aside id="ef-sidebar">
    	<div id="ef-sidebar-inner">
			<?php if ( is_numeric( $ef_data['sidebar']['widgets'] ) ) { ?>
				<?php $sidebar = 'ef-sidebar-' . $ef_data['sidebar']['widgets']; ?>
				<?php $sidebar_title = get_the_title( $ef_data['sidebar']['widgets'] ); ?>
			<?php } else { ?>
				<?php $sidebar = $ef_data['sidebar']['widgets']; ?>
			<?php } ?>

			<?php if ( is_active_sidebar( $sidebar ) ) { ?>

				<?php dynamic_sidebar( $sidebar ); ?>

			<?php } elseif ( is_user_logged_in() ) { ?>

				<div class="alert alert-info">
					<?php $link = sprintf( __( ' Ready to add widgets? <a href="%s"><abbr title="Add new">Get started here</abbr></a>', EF_TDM ), admin_url( 'widgets.php' ) ); ?>
					<p><?php printf( __( 'Please, add widgets to "%s". %s', EF_TDM ), is_numeric( $ef_data['sidebar']['widgets'] ) ? $sidebar_title : __( 'Sidebar Widget Area', EF_TDM ), $link ); ?></p>
				</div><!-- .alert.alert-info -->

			<?php } ?>
		</div><!-- #ef-sidebar-inner -->
     </aside><!-- #ef-sidebar -->

<?php } ?>