<?php
/**
 * The template for displaying the footer.
 *
 * @package Brama
 * @since Brama 1.0
 */
?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

		<?php if ( !is_page_template( 'templates/home-template.php' ) ) { ?>
	    		</main>

	            <footer id="ef-footer" class="clearfix">
					<?php if ( !is_page_template( 'templates/home-template.php' ) ) { ?>
						<?php get_sidebar( 'footer' ); ?>
					<?php } ?>

	                <div id="ef-footer-inner" class="text-center">
	                    <div class="row">
	                        <div class="col-md-6 col-md-offset-3">

	                        	<?php ef_add_social_profiles( $ef_data ); ?>

								<?php if ( $ef_data['to_support'] ) { ?>
		                            <?php if ( !empty( $ef_data['ef_footer_text'] ) ) { ?>
		                            	<?php $copyrights = wp_kses( $ef_data['ef_footer_text'], array( 'a' => array( 'href' => array(), 'title' => array(), 'target' => array() ), 'br' => array(), 'em' => array(), 'strong' => array() ) ); ?>
										<?php if ( function_exists( 'icl_register_string' ) ) { ?>
											<?php icl_register_string( __( 'Footer Text', EF_TDM ), '', $copyrights ); ?>
										<?php } ?>

										<?php _e( function_exists( 'icl_t' ) ? '<p>' . icl_t( __( 'Footer Text', EF_TDM ), '', $copyrights ) . '</p>' : '<p>' . $copyrights ) . '</p>'; ?>
									<?php } ?>
								<?php } else { ?>
									&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a> - <?php bloginfo( 'description' ); ?>
								<?php } ?>
	                        </div><!-- .col-md-6.col-md-offset-3 -->
	                    </div><!-- .row -->
	                </div><!-- #ef-footer-inner -->
	            </footer><!-- #ef-footer -->
	        </section><!-- #ef-content -->
        <?php } ?>

        <?php get_sidebar(); ?>
	</div><!-- #ef-tpl-wrapper -->

<?php if ( !empty( $ef_data['ef_google_analytics'] ) ) { ?>
	<div class="ef-h-script">
		<?php echo stripslashes( $ef_data['ef_google_analytics'] ); ?>
	</div>
<?php } ?>
<?php wp_footer(); ?>
</body>
</html>