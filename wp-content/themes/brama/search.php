<?php

/**
 * The template for displaying Search Results pages.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>
<?php $ef_data = EfGetOptions::TplConditions(); ?>
	<?php if ( have_posts() ) { ?>
		<?php get_template_part( 'loop', 'search' ); ?>

		<?php /* Pagination */ ?>

		<?php ef_theme_pagination( $wp_query->max_num_pages, $ef_data ); ?>
	<?php } else { ?>
		<div id="post-0" class="post no-results not-found text-center">
			<h2 class="entry-title"><?php _e( 'Nothing Found', EF_TDM ); ?></h2>

			<div class="entry-content">
				<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', EF_TDM ); ?></p>

				<?php get_search_form(); ?>
			</div><!-- .ef-entry-content -->
		</div><!-- #post-0 -->
	<?php } ?>
<?php get_footer(); ?>
