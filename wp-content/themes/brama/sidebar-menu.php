<?php

/**
 * The sidebar template.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php if ( $ef_data['menu']['layout'] === '2' ) { ?>
	<aside id="ef-menu-widgets">
		<div class="row">
			<?php if ( is_numeric( $ef_data['menu']['widgets'] ) ) { ?>
				<?php $sidebar = 'ef-sidebar-' . $ef_data['menu']['widgets']; ?>
				<?php $sidebar_title = get_the_title( $ef_data['menu']['widgets'] ); ?>
			<?php } else { ?>
				<?php $sidebar = $ef_data['menu']['widgets']; ?>
			<?php } ?>

			<?php if ( is_active_sidebar( $sidebar ) ) { ?>

				<?php dynamic_sidebar( $sidebar ); ?>

			<?php } elseif ( is_user_logged_in() ) { ?>
				<div class="col-md-12">
					<div class="alert alert-info">
						<?php $link = sprintf( __( ' Ready to add widgets? <a href="%s"><abbr title="Add new">Get started here</abbr></a>', EF_TDM ), admin_url( 'widgets.php' ) ); ?>

						<p><?php printf( __( 'Please, add widgets to "%s". %s', EF_TDM ), is_numeric( $ef_data['menu']['widgets'] ) ? $sidebar_title : __( 'Menu Widget Area', EF_TDM ), $link ); ?></p>
					</div><!-- .alert-box.radius -->
				</div><!-- .col-md-12 -->
			<?php } ?>
		</div><!-- .row -->
	</aside><!-- #ef-menu-widgets -->
<?php } ?>